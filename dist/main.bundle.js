webpackJsonp(["main"],{

/***/ "./C:/posreaderlib/build/Release/posappnode.node":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, module) {try {global.process.dlopen(module, "C:\\posreaderlib\\build\\Release\\posappnode.node"); } catch(e) {throw new Error('Cannot open ' + "C:\\posreaderlib\\build\\Release\\posappnode.node" + ': ' + e);}
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/webpack/buildin/global.js"), __webpack_require__("./node_modules/webpack/buildin/module.js")(module)))

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"app/dashboard/dashboard.module": [
		"./src/app/dashboard/dashboard.module.ts",
		"dashboard.module"
	],
	"app/dashboard/passes/passes.module": [
		"./src/app/dashboard/passes/passes.module.ts",
		"passes.module"
	],
	"app/dashboard/settlement/settlement.module": [
		"./src/app/dashboard/settlement/settlement.module.ts",
		"settlement.module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet ></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html")
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login_component__ = __webpack_require__("./src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_inputMobileNumber_component__ = __webpack_require__("./src/app/login/inputMobileNumber.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__login_inputNewPassword_component__ = __webpack_require__("./src/app/login/inputNewPassword.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_inputOtp_component__ = __webpack_require__("./src/app/login/inputOtp.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__routes_app_routing_module__ = __webpack_require__("./src/app/routes/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__directives_directive_module__ = __webpack_require__("./src/app/directives/directive.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_token_interceptor__ = __webpack_require__("./src/app/services/token.interceptor.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_base64_service__ = __webpack_require__("./src/app/services/base64.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_dal_service__ = __webpack_require__("./src/app/services/dal.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__common_common_module__ = __webpack_require__("./src/app/common/common.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











//import { PipesModule } from './pipes/pipes.module';






var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_9__routes_app_routing_module__["a" /* AppRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_10__directives_directive_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_16__common_common_module__["a" /* CommonBaseModule */],
                // PipesModule,
                __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["c" /* NgbModule */].forRoot()
            ],
            providers: [
                {
                    provide: __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HTTP_INTERCEPTORS */],
                    useClass: __WEBPACK_IMPORTED_MODULE_12__services_token_interceptor__["a" /* TokenInterceptor */],
                    multi: true
                },
                __WEBPACK_IMPORTED_MODULE_13__services_common_service__["a" /* CommonBaseService */],
                __WEBPACK_IMPORTED_MODULE_14__services_base64_service__["a" /* Base64Service */],
                __WEBPACK_IMPORTED_MODULE_15__services_dal_service__["a" /* DalService */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_5__login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_6__login_inputMobileNumber_component__["a" /* InputMobileNo */],
                __WEBPACK_IMPORTED_MODULE_7__login_inputNewPassword_component__["a" /* InputNewPassword */],
                __WEBPACK_IMPORTED_MODULE_8__login_inputOtp_component__["a" /* InputOtp */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/common/common.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommonBaseModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__directives_directive_module__ = __webpack_require__("./src/app/directives/directive.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pipes_pipes_module__ = __webpack_require__("./src/app/pipes/pipes.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__popup_popup_component__ = __webpack_require__("./src/app/common/popup/popup.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var CommonBaseModule = (function () {
    function CommonBaseModule() {
    }
    CommonBaseModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_3__directives_directive_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_5__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__["c" /* NgbModule */].forRoot()
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__popup_popup_component__["a" /* PopupComponent */]
            ],
            providers: [],
            exports: [__WEBPACK_IMPORTED_MODULE_6__popup_popup_component__["a" /* PopupComponent */]]
        })
    ], CommonBaseModule);
    return CommonBaseModule;
}());



/***/ }),

/***/ "./src/app/common/popup/popup.component.html":
/***/ (function(module, exports) {

module.exports = "<!--modal for insufficent amount -->\r\n<div class=\"modal_popup\" role=\"dialog\" data-backdrop=\"false\" role=\"dialog\" *ngIf=\"inSufficentBalance\">\r\n  <div class=\"modal__wrapper\">\r\n    <div class=\"img-warning\">\r\n      <img src=\"../src/images/group-28.png\" alt=\"\" />\r\n    </div>\r\n    <div class=\"warning-message\">\r\n      <span>\r\n        YOU HAVE INSUFFICIENT BALANCE\r\n      </span><br>\r\n      <span>\r\n        PLEASE RECHARGE\r\n      </span>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- modal for insufficent amount end-->\r\n\r\n<!-- modal for successful deduction  -->\r\n<div class=\"modal_popup\" role=\"dialog\" data-backdrop=\"false\" role=\"dialog\" *ngIf=\"successPopup\">\r\n  <div class=\"modal__wrapper\">\r\n    <div class=\"img-warning\">\r\n      <img src=\"../src/images/fingerprint-success.png\" alt=\"\" />\r\n    </div>\r\n    <div class=\"warning-message\">\r\n      <span>\r\n        FINE DEDUCTED SUCCESSFULL\r\n      </span>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- modal for successful deduction ends -->\r\n<!-- modal for successful transaction  -->\r\n<div class=\"modal_popup\" role=\"dialog\" data-backdrop=\"false\" role=\"dialog\" *ngIf=\"donePopupFlag\">\r\n  <div class=\"modal__wrapper\">\r\n    <div class=\"img-warning\">\r\n      <img src=\"../src/images/fingerprint-success.png\" alt=\"\" />\r\n    </div>\r\n    <div class=\"warning-message\">\r\n      <span>\r\n        DONE\r\n      </span>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- modal for successful deduction ends -->\r\n\r\n<!-- modal for activate card popup  -->\r\n<div class=\"modal_popup\" role=\"dialog\" data-backdrop=\"false\" role=\"dialog\" *ngIf=\"warningPopupDashboard\">\r\n  <div class=\"modal__wrapper wrapper_class_activateCard\">\r\n    <div class=\"warning-message__before_cancel\">\r\n      <span>\r\n        {{message}}\r\n      </span>\r\n    </div>\r\n    <div class=\"yes_no\">\r\n      <button class=\"button-yes\" appAutofocus (click)=\"cancelPopupForActivateCard()\">OK</button>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- modal for activate card popup ends -->\r\n<!-- modal for TAP AGAIN  -->\r\n<div class=\"modal_popup\" role=\"dialog\" data-backdrop=\"false\" role=\"dialog\" *ngIf=\"tap_again_warningPopup\">\r\n  <div class=\"modal__wrapper\">\r\n    <div class=\"img-warning\">\r\n      <img src=\"../src/images/noun-wrong-1890903.png\" alt=\"\" />\r\n    </div>\r\n    <div class=\"warning-message\">\r\n      <span>\r\n        {{tap_again_message}}\r\n      </span>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- modal for modal for TAP AGAIN ends -->\r\n\r\n<!-- modal for tab the card  -->\r\n<div class=\"modal_popup\" role=\"dialog\" data-backdrop=\"false\" role=\"dialog\" *ngIf=\"tapCardPopup\">\r\n  <div class=\"modal__wrapper\">\r\n    <div class=\"tab_card\">\r\n      <span>PLEASE TAP THE CARD</span>\r\n    </div>\r\n    <div class=\"tap_image\">\r\n      <img src=\"../src/images/sync-card-icon.png\" alt=\"\" />\r\n    </div>\r\n    <div class=\"authorised-text\">\r\n      <span>*AUTHORISED PERSOND ONLY</span>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- modal for modal for tab the card  ends -->\r\n\r\n<!-- Modal for otp verification and fingerprint verification -->\r\n<div class=\"modal_popup \" role=\"dialog\" data-backdrop=\"false\" role=\"dialog\" *ngIf=\"otpFingerPopup\">\r\n  <div class=\"total-content\">\r\n    <div class=\"close-modal\">\r\n      <button>\r\n        <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n      </button>\r\n    </div>\r\n    <div class=\"otp-finger-content\">\r\n      <div class=\"otp-verification\">\r\n        <div class=\"dont-have-text\">\r\n          <p>Have a Fingerprint Enabled Device?</p>\r\n        </div>\r\n        <div class=\"opt-verification-img\">\r\n          <button>OTP VERIFICATION</button>\r\n        </div>\r\n      </div>\r\n      <div class=\"border-div\">\r\n      </div>\r\n      <div class=\"finger-scanner\">\r\n        <div class=\"finger-on-text\">\r\n          <p>Place your Finger on the Device Now.</p>\r\n        </div>\r\n        <div class=\"fingerprint-img\">\r\n          <img src=\"../src/images/fingerprint.png\" alt=\"\" />\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"fingerprint-modal-footer\">\r\n      <div>\r\n        <img src=\"../src/images/alert.png\" alt=\"\" />\r\n      </div>\r\n      <div class=\"footer-text\">\r\n        <p>Loreum Ipsum is simply Dummytext</p>\r\n      </div>\r\n      <button class=\"skip-button\" (click)=\"otpFingerConfirmed()\">SKIP</button>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Modal for otp verification and fingerprint verification ends-->\r\n\r\n<!-- Modal for fingerprint verified-->\r\n\r\n<div class=\"modal_popup \" role=\"dialog\" data-backdrop=\"false\" role=\"dialog\" *ngIf=\"otpFingerconfirmedPopup\">\r\n  <div class=\"total-content\">\r\n    <div class=\"close-modal \">\r\n      <button>\r\n        <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n      </button>\r\n    </div>\r\n    <div class=\"otp-finger-content\">\r\n      <div class=\"otp-verification\">\r\n        <div class=\"dont-have-text\">\r\n          <p>Have a Fingerprint Enabled Device?</p>\r\n        </div>\r\n        <div class=\"opt-verification-img\">\r\n          <button>OTP VERIFICATION</button>\r\n        </div>\r\n      </div>\r\n      <div class=\"border-div\">\r\n      </div>\r\n      <div class=\"finger-scanner-verified\">\r\n        <div class=\"tick-img\">\r\n          <img src=\"../src/images/tick.png\">\r\n        </div>\r\n        <div class=\"fingerprint-recognized\">\r\n          <p>FINGERPRINT RECOGNIZED</p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"fingerprint-modal-footer\">\r\n      <div>\r\n        <img src=\"../src/images/alert.png\" alt=\"\" />\r\n      </div>\r\n      <div class=\"footer-text\">\r\n        <p>Loreum Ipsum is seems to</p>\r\n      </div>\r\n      <button class=\"skip-button\" (click)=\"otpFingerNotRecognized()\">SKIP</button>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Modal for fingerprint verified ends-->\r\n\r\n  <!-- Modal for fingerprint not verified-->\r\n<div class=\"modal_popup \" role=\"dialog\" data-backdrop=\"false\" role=\"dialog\" *ngIf=\"otpFingernotRecognizedPopup\">\r\n  <div class=\"total-content\">\r\n    <div class=\"close-modal \">\r\n      <button>\r\n        <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n      </button>\r\n    </div>\r\n    <div class=\"otp-finger-content\">\r\n      <div class=\"otp-verification\">\r\n        <div class=\"dont-have-text\">\r\n          <p>Have a Fingerprint Enabled Device?</p>\r\n        </div>\r\n        <div class=\"opt-verification-img\">\r\n          <button>OTP VERIFICATION</button>\r\n        </div>\r\n      </div>\r\n      <div class=\"border-div\">\r\n      </div>\r\n      <div class=\"finger-scanner-notverified\">\r\n        <div class=\"notsuccess-img\">\r\n          <img src=\"../src/images/notsuccess.png\">\r\n        </div>\r\n        <div class=\"notrecognized-text\">\r\n          <p>Fingerprint not recognized.</p>\r\n        </div>\r\n        <div class=\"try-again\">\r\n          <button>TRY AGAIN</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"fingerprint-modal-footer\">\r\n      <div>\r\n        <img src=\"../src/images/alert.png\" alt=\"\" />\r\n      </div>\r\n      <div class=\"footer-text\">\r\n        <p>Loreum Ipsum is heems to</p>\r\n      </div>\r\n      <button class=\"skip-button\" (click)=\"otpConfirmedFinger()\">SKIP</button>\r\n    </div>\r\n  </div>\r\n</div>\r\n  <!-- Modal for fingerprint not verified ends-->\r\n\r\n<!-- Modal for otp verified success-->\r\n<div class=\"modal_popup \" role=\"dialog\" data-backdrop=\"false\" role=\"dialog\" *ngIf=\"otpconfirmedFingerPopup\">\r\n<div class=\"total-content\">\r\n    <div class=\"close-modal \">\r\n      <button>\r\n        <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n      </button>\r\n    </div>\r\n    <div class=\"otp-finger-content\">\r\n      <div class=\"finger-scanner-verified\">\r\n        <div class=\"tick-img\">\r\n          <img src=\"../src/images/tick.png\">\r\n        </div>\r\n        <div class=\"fingerprint-recognized\">\r\n          <p>OTP Verified</p>\r\n        </div>\r\n      </div>\r\n      <div class=\"border-div\">\r\n      </div>\r\n      <div class=\"finger-scanner-notverified\">\r\n        <div class=\"start-fingerprint-text\">\r\n          <p>Start Using Your Fingerprint</p>\r\n        </div>\r\n        <div class=\"show-fingerprint-img\">\r\n          <img src=\"../src/images/fingerprint.png\" alt=\"\" />\r\n        </div>\r\n        <div class=\"use-fingerprint\">\r\n          <button>USE FINGERPRINT</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"fingerprint-modal-footer\">\r\n      <div>\r\n        <img src=\"../src/images/alert.png\" alt=\"\" />\r\n      </div>\r\n      <div class=\"footer-text\">\r\n        <p>Loreum Ipsum is heems to</p>\r\n      </div>\r\n      <button class=\"skip-button\" (click)=\"optInputFinger()\">SKIP</button>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Modal for otp verified success ends-->\r\n\r\n  <!-- Modal to give otp code-->\r\n<div class=\"modal_popup \" role=\"dialog\" data-backdrop=\"false\" role=\"dialog\" *ngIf=\"optinputFingerPopup\">\r\n  <div class=\"total-content\">\r\n    <div class=\"close-modal \">\r\n      <button>\r\n        <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n      </button>\r\n    </div>\r\n    <div class=\"otp-finger-content\">\r\n      <div class=\"input-otp-verify\">\r\n        <div class=\"otp-text\">\r\n          <span>OTP</span>\r\n        </div>\r\n        <div class=\"otp-sent-text\">\r\n          <p>we have sent OTP to the Mobile number for which aadhar card sync</p>\r\n        </div>\r\n        <div class=\"enter-otp\">\r\n          <p>Enter OTP Here</p>\r\n        </div>\r\n        <div class=\"otp-feild\">\r\n          <input class=\"otp-value\" type=\"password\" name=\"\" maxlength=\"1\">\r\n          <input class=\"otp-value\" type=\"password\" name=\"\" maxlength=\"1\">\r\n          <input class=\"otp-value\" type=\"password\" name=\"\" maxlength=\"1\">\r\n          <input class=\"otp-value\" type=\"password\" name=\"\" maxlength=\"1\">\r\n          <input class=\"otp-value\" type=\"password\" name=\"\" maxlength=\"1\">\r\n          <input class=\"otp-value\" type=\"password\" name=\"\" maxlength=\"1\">\r\n        </div>\r\n        <div class=\"resend-text\">\r\n          <span>Resend</span>\r\n        </div>\r\n        <div class=\"otp-submit-button\">\r\n          <button>VERIFY</button>\r\n        </div>\r\n      </div>\r\n      <div class=\"border-div\">\r\n      </div>\r\n      <div class=\"finger-scanner-notverified\">\r\n        <div class=\"start-fingerprint-text\">\r\n          <p>Start Using Your Fingerprint</p>\r\n        </div>\r\n        <div class=\"show-fingerprint-img\">\r\n          <img src=\"../src/images/fingerprint.png\" alt=\"\" />\r\n        </div>\r\n        <div class=\"use-fingerprint\">\r\n          <button>USE FINGERPRINT</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"fingerprint-modal-footer\">\r\n      <div>\r\n        <img src=\"../src/images/alert.png\" alt=\"\" />\r\n      </div>\r\n      <div class=\"footer-text\">\r\n        <p>Loreum Ipsum is heems to</p>\r\n      </div>\r\n      <button class=\"skip-button\" (click)=\"otpOrFinger()\">SKIP</button>\r\n    </div>\r\n  </div>\r\n</div>\r\n  <!-- Modal to give otp code ends-->\r\n\r\n <!-- Modal to SELECT OTP OR FINGERPRINT-->\r\n<div class=\"modal_popup \" role=\"dialog\" data-backdrop=\"false\" role=\"dialog\" *ngIf=\"otporFingerPopup\">\r\n<div class=\"total-content\">\r\n    <div class=\"close-modal \">\r\n      <button>\r\n        <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n      </button>\r\n    </div>\r\n    <div class=\"otp-finger-content\">\r\n      <div class=\"otp-verification\">\r\n        <div class=\"otp-text\">\r\n          <span>OTP</span>\r\n        </div>\r\n        <div class=\"otp-send-img\">\r\n          <img src=\"../src/images/send-otp.png\" alt=\"\" />\r\n        </div>\r\n        <div class=\"otp-submit-button\">\r\n          <button>SEND OTP</button>\r\n        </div>\r\n      </div>\r\n      <div class=\"border-div\">\r\n      </div>\r\n      <div class=\"finger-scanner\">\r\n        <div class=\"finger-on-text\">\r\n          <p>Start Using Your Fingerprint</p>\r\n        </div>\r\n        <div class=\"fingerprint-img\">\r\n          <img src=\"../src/images/fingerprint.png\" alt=\"\" />\r\n        </div>\r\n        <div class=\"use-fingerprint\">\r\n          <button>USE FINGERPRINT</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"fingerprint-modal-footer\">\r\n      <div>\r\n        <img src=\"../src/images/alert.png\" alt=\"\" />\r\n      </div>\r\n      <div class=\"footer-text\">\r\n        <p>Loreum Ipsum is simply Dummytext</p>\r\n      </div>\r\n      <button class=\"skip-button\" (click)=\"otpFingerNotRecognized()\">SKIP</button>\r\n    </div>\r\n  </div>\r\n</div>\r\n <!-- Modal to SELECT OTP OR FINGERPRINT ends-->\r\n\r\n <!-- Modal for card action popup-->\r\n<!-- <div class=\"modal_popup \" role=\"dialog\" data-backdrop=\"false\" role=\"dialog\" *ngIf=\"warningPopupDashboard\">\r\n  <div class=\"modal-content-card\">\r\n    <div class=\"close-modal \">\r\n      <button>\r\n        <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n      </button>\r\n    </div>\r\n    <div class=\"message-button\">\r\n      <span>Successful</span>\r\n      <button  (click)=\"c('Close click')\" autofocus>Done</button>\r\n    </div>\r\n  </div>\r\n</div> -->\r\n\r\n<!-- Modal for card action popup ends-->"

/***/ }),

/***/ "./src/app/common/popup/popup.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_dal_service__ = __webpack_require__("./src/app/services/dal.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_base64_service__ = __webpack_require__("./src/app/services/base64.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PopupComponent = (function () {
    //aadhar card popupends
    function PopupComponent(commonserve, activateroute, router, base64, dal, ref) {
        this.commonserve = commonserve;
        this.activateroute = activateroute;
        this.router = router;
        this.base64 = base64;
        this.dal = dal;
        this.ref = ref;
        this.setDefault = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.cardInfo = {
            tollLogs: []
        };
        this.otpFingerPopup = false;
        this.otpFingerconfirmedPopup = false;
        this.otpFingernotRecognizedPopup = false;
        this.otpconfirmedFingerPopup = false;
        this.optinputFingerPopup = false;
        this.otporFingerPopup = false;
    }
    PopupComponent.prototype.showPopup = function (type) {
        switch (type) {
            case "INSUFFICIENTBAL":
                this.finePopupFlag = false;
                this.inSufficentBalance = true;
                this.tapCardPopup = false;
                this.successPopup = false;
                this.cancelFinePopup = false;
                this.donePopupFlag = false;
                this.warningPopupDashboard = false;
                this.tap_again_warningPopup = false;
                break;
            case "TAPCARD":
                this.finePopupFlag = false;
                this.inSufficentBalance = false;
                this.tapCardPopup = true;
                this.successPopup = false;
                this.cancelFinePopup = false;
                this.donePopupFlag = false;
                this.warningPopupDashboard = false;
                this.tap_again_warningPopup = false;
                break;
            case 'CANCEL':
                this.finePopupFlag = false;
                this.inSufficentBalance = false;
                this.tapCardPopup = false;
                this.successPopup = false;
                this.cancelFinePopup = true;
                this.donePopupFlag = false;
                this.warningPopupDashboard = false;
                this.tap_again_warningPopup = false;
                break;
            case "SUCCESS":
                this.finePopupFlag = false;
                this.inSufficentBalance = false;
                this.tapCardPopup = false;
                this.successPopup = true;
                this.cancelFinePopup = false;
                this.donePopupFlag = false;
                this.warningPopupDashboard = false;
                this.tap_again_warningPopup = false;
                break;
            case "DONE":
                this.finePopupFlag = false;
                this.inSufficentBalance = false;
                this.tapCardPopup = false;
                this.successPopup = false;
                this.cancelFinePopup = false;
                this.donePopupFlag = true;
                this.warningPopupDashboard = false;
                this.tap_again_warningPopup = false;
                break;
            case "ACTIVATECARD":
                this.finePopupFlag = false;
                this.inSufficentBalance = false;
                this.tapCardPopup = false;
                this.successPopup = false;
                this.cancelFinePopup = false;
                this.donePopupFlag = false;
                this.warningPopupDashboard = false;
                this.tap_again_warningPopup = false;
                break;
            case "WARNINGPOPUP":
                this.finePopupFlag = false;
                this.inSufficentBalance = false;
                this.tapCardPopup = false;
                this.successPopup = false;
                this.cancelFinePopup = false;
                this.donePopupFlag = false;
                this.warningPopupDashboard = true;
                this.tap_again_warningPopup = false;
                break;
            case "TAPAGAIN":
                this.finePopupFlag = false;
                this.inSufficentBalance = false;
                this.tapCardPopup = false;
                this.successPopup = false;
                this.cancelFinePopup = false;
                this.donePopupFlag = false;
                this.warningPopupDashboard = false;
                this.tap_again_warningPopup = true;
                break;
        }
        this.ref.detectChanges();
    };
    PopupComponent.prototype.closePopup = function () {
        this.finePopupFlag = false;
        this.inSufficentBalance = false;
        this.tapCardPopup = false;
        this.successPopup = false;
        this.cancelFinePopup = false;
        this.donePopupFlag = false;
        this.warningPopupDashboard = false;
        this.tap_again_warningPopup = false;
        this.ref.detectChanges();
    };
    PopupComponent.prototype.cancelPopupYesBtn = function () {
        this.setDefault.emit(null);
        this.closePopup();
    };
    PopupComponent.prototype.cancelPopupForActivateCard = function () {
        this.closePopup();
    };
    PopupComponent.prototype.otpFingerscan = function () {
        this.otpFingerPopup = true;
        this.otpFingerconfirmedPopup = false;
        this.otpFingernotRecognizedPopup = false;
        this.otpconfirmedFingerPopup = false;
        this.optinputFingerPopup = false;
        this.otporFingerPopup = false;
        this.otpconfirmedFingerPopup = false;
        this.ref.detectChanges();
    };
    PopupComponent.prototype.otpFingerConfirmed = function () {
        this.otpFingerconfirmedPopup = false;
        this.otpFingerPopup = false;
        this.otpFingernotRecognizedPopup = false;
        this.otpconfirmedFingerPopup = false;
        this.optinputFingerPopup = false;
        this.otporFingerPopup = true;
        this.ref.detectChanges();
    };
    PopupComponent.prototype.otpFingerNotRecognized = function () {
        this.otpFingernotRecognizedPopup = false;
        this.otpFingerPopup = false;
        this.otpFingerconfirmedPopup = false;
        this.otpconfirmedFingerPopup = false;
        this.optinputFingerPopup = false;
        this.otporFingerPopup = false;
        this.ref.detectChanges();
    };
    PopupComponent.prototype.otpConfirmedFinger = function () {
        this.otpconfirmedFingerPopup = true;
        this.otpFingerPopup = false;
        this.otpFingerconfirmedPopup = false;
        this.otpFingernotRecognizedPopup = false;
        this.optinputFingerPopup = false;
        this.otporFingerPopup = false;
        this.ref.detectChanges();
    };
    PopupComponent.prototype.optInputFinger = function () {
        this.optinputFingerPopup = true;
        this.otpconfirmedFingerPopup = false;
        this.otpFingerPopup = false;
        this.otpFingerconfirmedPopup = false;
        this.otpFingernotRecognizedPopup = false;
        this.optinputFingerPopup = false;
        this.otporFingerPopup = false;
        this.ref.detectChanges();
    };
    PopupComponent.prototype.otpOrFinger = function () {
        this.otporFingerPopup = true;
        this.otpconfirmedFingerPopup = false;
        this.otpFingerPopup = false;
        this.otpFingerconfirmedPopup = false;
        this.otpFingernotRecognizedPopup = false;
        this.optinputFingerPopup = false;
        this.otporFingerPopup = false;
        this.ref.detectChanges();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], PopupComponent.prototype, "setDefault", void 0);
    PopupComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/common/popup/popup.component.html"),
            selector: 'popup'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_common_service__["a" /* CommonBaseService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_4__services_base64_service__["a" /* Base64Service */], __WEBPACK_IMPORTED_MODULE_3__services_dal_service__["a" /* DalService */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */]])
    ], PopupComponent);
    return PopupComponent;
}());



/***/ }),

/***/ "./src/app/directives/autoFocus.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutofocusDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AutofocusDirective = (function () {
    function AutofocusDirective(el) {
        this.el = el;
    }
    AutofocusDirective.prototype.ngAfterViewInit = function () {
        this.el.nativeElement.focus();
    };
    AutofocusDirective.prototype.abc = function () {
        alert(1);
    };
    AutofocusDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Directive */])({
            selector: '[appAutofocus]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */]])
    ], AutofocusDirective);
    return AutofocusDirective;
}());



/***/ }),

/***/ "./src/app/directives/cursor-position.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CursorPosition; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CursorPosition = (function () {
    function CursorPosition(el, ref) {
        this.el = el;
        this.ref = ref;
    }
    CursorPosition.prototype.onClick = function () {
        this.onClickFunction(Event);
    };
    CursorPosition.prototype.onClickFunction = function (e) {
        var txt = document.getElementById("rate-card--recharge__value");
        this.caretPos = txt['selectionStart'];
        var textAreaTxt = this.recharge_amount.toString();
        var len = parseInt(this.caretPos);
        this.recharge_amount = textAreaTxt.substring(0, len - 1) + textAreaTxt.substring(len);
        var elemntHtml = document.getElementById("rate-card--recharge__value");
        console.log(elemntHtml);
        elemntHtml['value'] = this.recharge_amount;
        elemntHtml['selectionStart'] = this.recharge_amount.length;
        var event = new Event('input');
        elemntHtml.dispatchEvent(event);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])('amount'),
        __metadata("design:type", Object)
    ], CursorPosition.prototype, "recharge_amount", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* HostListener */])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], CursorPosition.prototype, "onClick", null);
    CursorPosition = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Directive */])({
            selector: '[cursor]',
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */]])
    ], CursorPosition);
    return CursorPosition;
}());



/***/ }),

/***/ "./src/app/directives/directive.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DirectivesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cursor_position__ = __webpack_require__("./src/app/directives/cursor-position.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__toUpperCase__ = __webpack_require__("./src/app/directives/toUpperCase.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__autoFocus__ = __webpack_require__("./src/app/directives/autoFocus.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var DirectivesModule = (function () {
    function DirectivesModule() {
    }
    DirectivesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormsModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__cursor_position__["a" /* CursorPosition */],
                __WEBPACK_IMPORTED_MODULE_3__toUpperCase__["a" /* UppercaseDirective */],
                __WEBPACK_IMPORTED_MODULE_4__autoFocus__["a" /* AutofocusDirective */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__cursor_position__["a" /* CursorPosition */],
                __WEBPACK_IMPORTED_MODULE_3__toUpperCase__["a" /* UppercaseDirective */],
                __WEBPACK_IMPORTED_MODULE_4__autoFocus__["a" /* AutofocusDirective */]
            ]
        })
    ], DirectivesModule);
    return DirectivesModule;
}());



/***/ }),

/***/ "./src/app/directives/toUpperCase.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UppercaseDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UppercaseDirective = (function () {
    function UppercaseDirective() {
        this.ngModelChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    UppercaseDirective.prototype.onInputChange = function ($event) {
        this.value = $event.target.value.toUpperCase();
        this.ngModelChange.emit(this.value);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], UppercaseDirective.prototype, "ngModelChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* HostListener */])('input', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], UppercaseDirective.prototype, "onInputChange", null);
    UppercaseDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Directive */])({
            selector: '[ngModel][uppercase]'
        })
    ], UppercaseDirective);
    return UppercaseDirective;
}());



/***/ }),

/***/ "./src/app/login/inputMobileNumber.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputMobileNo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InputMobileNo = (function () {
    function InputMobileNo(commonserve, modalService, ref, router) {
        this.commonserve = commonserve;
        this.modalService = modalService;
        this.ref = ref;
        this.router = router;
    }
    InputMobileNo = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/login/inputMobileNumber.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_common_service__["a" /* CommonBaseService */],
            __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], InputMobileNo);
    return InputMobileNo;
}());



/***/ }),

/***/ "./src/app/login/inputMobileNumber.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"signin-style\">\r\n    <div class=\"my-image\">\r\n        <div class=\"left-text-section\">\r\n            <div class=\"hello-there-text\"> Hello there! </div>\r\n            <p class=\"p-section\"> Sign in and start managing your Toll items</p>\r\n        </div>\r\n    </div>\r\n    <img src=\"../src/images/d-o-e-l-o-g-o.png\" class=\"doe-logo\" [routerLink]=\"['../home']\">\r\n    <div class=\"auth__content_section\">\r\n        <div class=\"siginin-div\">\r\n            <img src=\"../src/images/card-bg.jpg\" class=\"signin-box-img\">\r\n            <div class=\"siginin-tag\">\r\n                <p>Tap DOE card back of your device to login</p>\r\n            </div>\r\n        </div>\r\n        <form ng-submit=\"userLogin()\">\r\n            <div class=\"signin-box-text\" >FORGOT PASSWORD?</div>\r\n            <div class=\"sigin-content-forgotpassword\">\r\n                <div class=\"login-style\" style=\"text-align: center;\">Enter your registered mobile number</div>\r\n\r\n                <div class=\"login-password\">Mobile Number</div>\r\n                <div class=\"password-input\">\r\n                    <input type=\"password\"  ng-model=\"reqObj.mobileNumber\" name=\"name\" class=\"password-input-style\" value=\"\" placeholder=\"*****\" style=\"width: 94%;\">\r\n                </div>\r\n            </div>\r\n            <div class=\"signin-button-div\">\r\n                <a>\r\n                    <button ng-click=\"getOtp()\" class=\"signin-button hide-outline\" [routerLink]=\"['../inputOtp']\">NEXT<img class=\"arrow-go\" src=\"../src/images/arrow.png\"> </button>\r\n                </a>\r\n            </div>\r\n        </form>\r\n    </div>\r\n    <div>\r\n    <div>\r\n        <img src=\"../src/images/coin.png\" class=\"coin-logo\" />\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/login/inputNewPassword.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputNewPassword; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InputNewPassword = (function () {
    function InputNewPassword(commonserve, modalService, ref, router) {
        this.commonserve = commonserve;
        this.modalService = modalService;
        this.ref = ref;
        this.router = router;
    }
    InputNewPassword = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/login/login.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_common_service__["a" /* CommonBaseService */],
            __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], InputNewPassword);
    return InputNewPassword;
}());



/***/ }),

/***/ "./src/app/login/inputOtp.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputOtp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InputOtp = (function () {
    function InputOtp(commonserve, modalService, ref, router) {
        this.commonserve = commonserve;
        this.modalService = modalService;
        this.ref = ref;
        this.router = router;
    }
    InputOtp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/login/login.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_common_service__["a" /* CommonBaseService */],
            __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], InputOtp);
    return InputOtp;
}());



/***/ }),

/***/ "./src/app/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_dal_service__ = __webpack_require__("./src/app/services/dal.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common_popup_popup_component__ = __webpack_require__("./src/app/common/popup/popup.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_retry__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/retry.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var LoginComponent = (function () {
    //ViewChild for Fine Popup --end --
    function LoginComponent(commonserve, modalService, ref, router, dal) {
        this.commonserve = commonserve;
        this.modalService = modalService;
        this.ref = ref;
        this.router = router;
        this.dal = dal;
        if (this.commonserve.initReader) {
            this.dal.initializeSdr10(__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].sdrInitializeVariable);
            this.dal.initializeBuaroh(__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].buarohinitializeVariable);
            this.dal.initializeBuaroh(__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].buarohinitializeVariable2);
            this.commonserve.initReader = 0;
        }
        this.loginParams = {
            "username": "operator1r1",
            "password": "12345678"
        };
    }
    LoginComponent.prototype.login = function () {
        var _this = this;
        var parmas = {
            "username": this.loginParams.username,
            "password": this.loginParams.password
        };
        if (window.navigator.onLine) {
            if (this.loginParams.username && this.loginParams.password) {
                this.commonserve.loginApi(parmas).retry(5).subscribe(function (response) {
                    console.log("response in login", response);
                    _this.updateStore(response);
                    _this.router.navigate(['/dashboard']);
                }, function (err) {
                    console.log("error in login ", err);
                    if (err.error && err.error.message) {
                        _this.finePopup.message = err.error.message;
                        _this.finePopup.showPopup('WARNINGPOPUP');
                    }
                    else {
                        _this.finePopup.message = "check internet connection";
                        _this.finePopup.showPopup('WARNINGPOPUP');
                    }
                });
            }
            else {
                this.finePopup.message = "Please enter username and password";
                this.finePopup.showPopup('WARNINGPOPUP');
            }
        }
        else {
            this.finePopup.message = "Check Your Internet Connection";
            this.finePopup.showPopup('WARNINGPOPUP');
        }
    };
    LoginComponent.prototype.updateStore = function (auth) {
        var data = {
            "accesstoken": auth.accessToken,
            "refreshtoken": auth.refreshToken,
            "cartid": ""
        };
        this.commonserve.setStoreData(data);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_6__common_popup_popup_component__["a" /* PopupComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__common_popup_popup_component__["a" /* PopupComponent */])
    ], LoginComponent.prototype, "finePopup", void 0);
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/login/login.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_common_service__["a" /* CommonBaseService */],
            __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4__services_dal_service__["a" /* DalService */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/login/login.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"signin-style\">\r\n    <div class=\"my-image\">\r\n        <div class=\"left-text-section\">\r\n            <div class=\"hello-there-text\"> Hello there! </div>\r\n            <p class=\"p-section\"> Sign in and start managing your Toll items</p>\r\n        </div>\r\n    </div>\r\n    <img src=\"../src/images/d-o-e-l-o-g-o.png\" class=\"doe-logo\" [routerLink]=\"['../home']\">    \r\n    <div class=\"auth__content_section\">\r\n        <div class=\"siginin-div\">\r\n            <img src=\"../src/images/card-bg.jpg\" class=\"signin-box-img\">\r\n            <div class=\"siginin-tag\">\r\n                <p>Tap DOE card back of your device to login</p>\r\n            </div>\r\n        </div>\r\n        <div class=\"signin-box-text\" > SIGN IN </div>\r\n            <div class=\"sigin-content\">\r\n                <div class=\"login-style\"> LOGIN </div>\r\n                <div class=\"email-input\">\r\n                    <input type=\"text\" class=\"email-input-style\" value=\"\" placeholder=\"12345\" style=\" padding-top: 3%;width: 94%;\" [(ngModel)]=\"loginParams.username\" ngDefaultControl>\r\n                </div>\r\n                <div class=\"login-password\">PASSWORD</div>\r\n                <div class=\"password-input\">\r\n                    <input type=\"password\"  class=\"password-input-style\" value=\"\" placeholder=\"*****\" style=\"width: 94%;\" [(ngModel)]=\"loginParams.password\" ngDefaultControl>\r\n                </div>\r\n            </div>\r\n            <div class=\"signin-button-div\">\r\n                <button class=\"signin-button hide-outline\" (click)=\"login()\" >SIGN IN NOW <img class=\"arrow-go\" src=\"../src/images/arrow.png\" /> </button>\r\n            </div>\r\n            <div class=\"forget-password-div\">\r\n                <span class=\"forget-password\">FORGOT PASSWORD?</span><a href=\"#forgotpassword-number\" class=\"request-link\" [routerLink]=\"['../inputMobileNo']\">REQUEST</a>\r\n            </div>\r\n    </div>\r\n    <div>\r\n    <div>\r\n        <img src=\"../src/images/coin.png\" class=\"coin-logo\" />\r\n    </div>\r\n</div>\r\n<!-- Modal for card action popup ends-->\r\n<div>\r\n  <popup (setDefault)=\"definecardDefaults()\"></popup>\r\n</div>"

/***/ }),

/***/ "./src/app/pipes/pipes.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes__ = __webpack_require__("./src/app/pipes/pipes.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var PipesModule = (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__pipes__["a" /* DidisplayCardNumberPipe */],
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__pipes__["a" /* DidisplayCardNumberPipe */],
            ]
        })
    ], PipesModule);
    return PipesModule;
}());



/***/ }),

/***/ "./src/app/pipes/pipes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DidisplayCardNumberPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var DidisplayCardNumberPipe = (function () {
    function DidisplayCardNumberPipe() {
    }
    DidisplayCardNumberPipe.prototype.transform = function (value) {
        console.log("VALUE IS", value);
        value = value.toString();
        var url = "";
        var obj = value;
        var cnend = value.substring(value.length - 4, value.length);
        var res = "**** ****" + cnend;
        if (cnend) {
            console.log("filyerd value", res);
            return res;
        }
        else {
            return null;
        }
    };
    DidisplayCardNumberPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Pipe */])({ name: 'displayCardNumber' })
    ], DidisplayCardNumberPipe);
    return DidisplayCardNumberPipe;
}());



/***/ }),

/***/ "./src/app/routes/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login_component__ = __webpack_require__("./src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_inputMobileNumber_component__ = __webpack_require__("./src/app/login/inputMobileNumber.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_inputNewPassword_component__ = __webpack_require__("./src/app/login/inputNewPassword.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_inputOtp_component__ = __webpack_require__("./src/app/login/inputOtp.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: __WEBPACK_IMPORTED_MODULE_2__login_login_component__["a" /* LoginComponent */] },
    { path: 'inputNewPassword', component: __WEBPACK_IMPORTED_MODULE_4__login_inputNewPassword_component__["a" /* InputNewPassword */] },
    { path: 'inputMobileNo', component: __WEBPACK_IMPORTED_MODULE_3__login_inputMobileNumber_component__["a" /* InputMobileNo */] },
    { path: 'inputOtp', component: __WEBPACK_IMPORTED_MODULE_5__login_inputOtp_component__["a" /* InputOtp */] },
    { path: 'dashboard', loadChildren: 'app/dashboard/dashboard.module#DashboardModule' }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forRoot(routes),
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/services/base64.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Base64Service; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw__ = __webpack_require__("./node_modules/rxjs/_esm5/add/observable/throw.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var Base64Service = (function () {
    function Base64Service(http) {
        this.http = http;
        this.apiurl = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls;
        var c1;
        var c2;
        var c3;
        var _this = this;
        this.Base64 = {
            _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
            encode: function (e) {
                var t = "";
                var n, r, i, s, o, u, a;
                var f = 0;
                e = this.Base64._utf8_encode(e);
                while (f < e.length) {
                    n = e.charCodeAt(f++);
                    r = e.charCodeAt(f++);
                    i = e.charCodeAt(f++);
                    s = n >> 2;
                    o = (n & 3) << 4 | r >> 4;
                    u = (r & 15) << 2 | i >> 6;
                    a = i & 63;
                    if (isNaN(r)) {
                        u = a = 64;
                    }
                    else if (isNaN(i)) {
                        a = 64;
                    }
                    t = t + this.Base64._keyStr.charAt(s) + this.Base64._keyStr.charAt(o) + this.Base64._keyStr.charAt(u) + this.Base64._keyStr.charAt(a);
                }
                return t;
            },
            decode: function (e) {
                var t = "";
                var n, r, i;
                var s, o, u, a;
                var f = 0;
                e = e.replace(/[^A-Za-z0-9+/=]/g, "");
                while (f < e.length) {
                    s = this._keyStr.indexOf(e.charAt(f++));
                    o = this._keyStr.indexOf(e.charAt(f++));
                    u = this._keyStr.indexOf(e.charAt(f++));
                    a = this._keyStr.indexOf(e.charAt(f++));
                    n = s << 2 | o >> 4;
                    r = (o & 15) << 4 | u >> 2;
                    i = (u & 3) << 6 | a;
                    t = t + String.fromCharCode(n);
                    if (u != 64) {
                        t = t + String.fromCharCode(r);
                    }
                    if (a != 64) {
                        t = t + String.fromCharCode(i);
                    }
                }
                t = _this.Base64._utf8_decode(t);
                return t;
            },
            _utf8_encode: function (e) { e = e.replace(/rn/g, "n"); var t = ""; for (var n = 0; n < e.length; n++) {
                var r = e.charCodeAt(n);
                if (r < 128) {
                    t += String.fromCharCode(r);
                }
                else if (r > 127 && r < 2048) {
                    t += String.fromCharCode(r >> 6 | 192);
                    t += String.fromCharCode(r & 63 | 128);
                }
                else {
                    t += String.fromCharCode(r >> 12 | 224);
                    t += String.fromCharCode(r >> 6 & 63 | 128);
                    t += String.fromCharCode(r & 63 | 128);
                }
            } return t; },
            _utf8_decode: function (e) { var t = ""; var n = 0; var r = c1 = c2 = 0; while (n < e.length) {
                r = e.charCodeAt(n);
                if (r < 128) {
                    t += String.fromCharCode(r);
                    n++;
                }
                else if (r > 191 && r < 224) {
                    c2 = e.charCodeAt(n + 1);
                    t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                    n += 2;
                }
                else {
                    c2 = e.charCodeAt(n + 1);
                    c3 = e.charCodeAt(n + 2);
                    t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                    n += 3;
                }
            } return t; }
        };
    }
    Base64Service.prototype.decode = function (val) {
        var userinfo = this.Base64.decode(val);
        return userinfo;
    };
    Base64Service = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
    ], Base64Service);
    return Base64Service;
}());



/***/ }),

/***/ "./src/app/services/common.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommonBaseService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw__ = __webpack_require__("./node_modules/rxjs/_esm5/add/observable/throw.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CommonBaseService = (function () {
    function CommonBaseService(http) {
        this.http = http;
        this.storeinfodata = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].storeInfo;
        console.log("services loaded");
        this.initReader = 1;
    }
    CommonBaseService.prototype.getEpocTimestamp = function () {
        var currentDateTime = new Date();
        var dataInEpoc = currentDateTime.getTime();
        return dataInEpoc;
    };
    CommonBaseService.prototype.getIsoStringTime = function () {
        var date = new Date();
        var tostring = date.toISOString();
        return tostring;
    };
    //sort array element based on alphabet
    CommonBaseService.prototype.sortArray = function (element) {
        element.sort(function (a, b) {
            var nameA = a.info.numericCode, nameB = b.info.numericCode;
            if (nameA < nameB)
                return -1;
            if (nameA > nameB)
                return 1;
            return 0; //default return value (no sorting)
        });
    };
    //sort array for same vehicle cat label
    CommonBaseService.prototype.sortArrayOfObject = function (a, b) {
        if (a.label < b.label)
            return -1;
        if (a.label > b.label)
            return 1;
        return 0;
    };
    CommonBaseService.prototype.createNewTransaction = function (reqObj) {
        var headerOptions = this.getHeaders();
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].ds.activity;
        return this.http.post(url, reqObj, headerOptions)
            .map(function (response) {
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    CommonBaseService.prototype.updateDoeStore = function (reqObj) {
        var headerOptions = this.getHeaders();
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].ds.updateCard;
        return this.http.post(url, reqObj, headerOptions)
            .map(function (response) {
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    //logout operator
    CommonBaseService.prototype.deleteOperationInfo = function () {
        localStorage.clear();
    };
    //set the accesstoken in local stroage
    CommonBaseService.prototype.setStoreData = function (data) {
        //let store = localStorage.getItem("store");
        if (data) {
            //let storeInfo = JSON.parse(store);
            //storeInfo = data ? data : this.storeinfodata.toll;
            localStorage.setItem("store", JSON.stringify(data));
        }
        else {
            var tolldata = data ? data : this.storeinfodata.toll;
            localStorage.setItem("store", JSON.stringify(this.storeinfodata));
        }
    };
    //get the accesstoken in local stroage
    CommonBaseService.prototype.getStoreData = function () {
        var store = localStorage.getItem("store");
        console.log("local storage here =======>");
        console.log(store);
        if (store) {
            var storeInfo = JSON.parse(store);
            var tollstoreinfodata = storeInfo.toll
                ? storeInfo.toll
                : this.storeinfodata.toll;
            return storeInfo;
        }
        else {
            return this.storeinfodata.toll;
        }
    };
    CommonBaseService.prototype.getToken = function () {
        var store = localStorage.getItem("store");
        if (store) {
            var storeInfo = JSON.parse(store);
            var token = (storeInfo && storeInfo.accesstoken) ? storeInfo.accesstoken : "";
            return token;
        }
        else {
            var token = (this.storeinfodata.toll && this.storeinfodata.toll.accesstoken) ? this.storeinfodata.toll.accesstoken : "";
            return token;
        }
    };
    CommonBaseService.prototype.activateDoeStore = function (reqObj) {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].ds.activateCard;
        return this.http.post(url, reqObj)
            .map(function (response) {
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    //login api call
    CommonBaseService.prototype.loginApi = function (reqobj) {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls.loginApi + '/token';
        var txt = "username=" + reqobj.username + "&password=" + reqobj.password + "&grant_type=password";
        return this.http.post(url, txt)
            .map(function (response) {
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    //logout operator api
    CommonBaseService.prototype.logoutApi = function () {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls.loginApi + '/revoke';
        var auth = this.getStoreData();
        var txt = "refresh_token=" + auth.refreshtoken;
        var headerOptions = this.getHeaders();
        return this.http.post(url, txt, headerOptions)
            .map(function (response) {
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    //get operator info
    CommonBaseService.prototype.operatorInfo = function (reqObj) {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls.userserv + '/users/' + reqObj;
        var headerOptions = this.getHeaders();
        return this.http.get(url, headerOptions)
            .map(function (response) {
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    //get fee structure
    CommonBaseService.prototype.getFeeRevision = function (reqobj) {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls.domainserv + '/fee-revisions' + '?business_level_id=' + reqobj;
        var headerOptions = this.getHeaders();
        return this.http.get(url, headerOptions)
            .map(function (response) {
            console.log("fee revision in service", response);
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    //get category codes
    CommonBaseService.prototype.getFeeCode = function () {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls.domainserv + '/fee-codes';
        var headerOptions = this.getHeaders();
        return this.http.get(url, headerOptions)
            .map(function (response) {
            console.log("fee revision in service", response);
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    //check amount before recharge activity
    CommonBaseService.prototype.getRechargeCheck = function (reqobj) {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls.activityserv + '/analyse';
        var headerOptions = this.getHeaders();
        return this.http.post(url, reqobj, headerOptions)
            .map(function (response) {
            console.log("recharge check api in service", response);
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    //recharge Api
    CommonBaseService.prototype.recharge = function (reqobj) {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls.userserv + '/activities';
        var headerOptions = this.getHeaders();
        return this.http.post(url, reqobj, headerOptions)
            .map(function (response) {
            console.log("recharge api works", response);
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    CommonBaseService.prototype.apigetDomaindata = function (reqobj) {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls.domainserv + '/tolls' + reqobj;
        var headerOptions = this.getHeaders();
        return this.http.get(url, headerOptions)
            .map(function (response) {
            console.log("recharge api works", response);
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    CommonBaseService.prototype.activateCardApi = function (reqobj) {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls.cardserv + '/activate-card';
        var headerOptions = this.getHeaders();
        return this.http.post(url, reqobj, headerOptions)
            .map(function (response) {
            console.log("activate card api works", response);
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    CommonBaseService.prototype.updateCardApi = function (reqobj) {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls.cardserv + '/update-card-info';
        var headerOptions = this.getHeaders();
        return this.http.post(url, reqobj, headerOptions)
            .map(function (response) {
            console.log("update card api works", response);
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    //transaction deduction api
    CommonBaseService.prototype.transactionApi = function (reqobj) {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls.activityserv + '/activities';
        var headerOptions = this.getHeaders();
        return this.http.post(url, reqobj, headerOptions)
            .map(function (response) {
            console.log("deduction from card api works", response);
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    //get company details
    CommonBaseService.prototype.getCompanyDetails = function (reqobj) {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls.businessserv + '/business-levels/' + reqobj;
        var headerOptions = this.getHeaders();
        return this.http.get(url, headerOptions)
            .map(function (response) {
            console.log("company details api works", response);
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    //activate pass
    CommonBaseService.prototype.activatePassApi = function (reqobj) {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls.activityserv + '/activities';
        var headerOptions = this.getHeaders();
        return this.http.post(url, reqobj, headerOptions)
            .map(function (response) {
            console.log("activate pass api works", response);
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    CommonBaseService.prototype.passTransactionApi = function (reqobj) {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls.activityserv + '/activities';
        var headerOptions = this.getHeaders();
        return this.http.post(url, reqobj, headerOptions)
            .map(function (response) {
            console.log("pass transaction api works", response);
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    //get transaction log data
    CommonBaseService.prototype.transactionLogsApi = function (reqobj) {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiurls.activityserv + '/activities' + reqobj;
        var headerOptions = this.getHeaders();
        return this.http.get(url, headerOptions)
            .map(function (response) {
            console.log("transaction log api works", response);
            return response;
        }, function (error) {
            console.log(error);
        });
    };
    //offline mode
    CommonBaseService.prototype.extractData = function (res) {
        return res || {};
    };
    CommonBaseService.prototype.getOfflineAccessToken = function () {
        return this.http
            .get("./../src/assets/json/accessToken.json")
            .map(this.extractData);
    };
    CommonBaseService.prototype.getOfflineOperatorInfo = function () {
        return this.http
            .get("./../src/assets/json/operatorInfo.json")
            .map(this.extractData);
    };
    CommonBaseService.prototype.getOfflineCompanyDetails = function () {
        return this.http
            .get("./../src/assets/json/companyInfo.json")
            .map(this.extractData);
    };
    CommonBaseService.prototype.getOfflineFeeStructure = function () {
        return this.http
            .get("./../src/assets/json/feeStructure.json")
            .map(this.extractData);
    };
    CommonBaseService.prototype.getHeaders = function () {
        var token = this.getToken();
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpHeaders */]({ 'Authorization': 'Bearer ' + token });
        var options = { headers: headers };
        return options;
    };
    CommonBaseService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
    ], CommonBaseService);
    return CommonBaseService;
}());



/***/ }),

/***/ "./src/app/services/dal.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DalService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var dal_binding = __webpack_require__("./C:/posreaderlib/build/Release/posappnode.node");
var DalService = (function () {
    function DalService(http) {
        this.http = http;
    }
    DalService.prototype.initializeSdr10 = function (initializeVariable) {
        dal_binding.initializeReader(initializeVariable, function (response) {
            console.log("sdr10 initilised in service", JSON.stringify(response));
        });
    };
    DalService.prototype.initializeBuaroh = function (initializeVariable) {
        dal_binding.initializeReader(initializeVariable, function (response) {
            console.log("buaroh initilised in service", response);
        });
    };
    // stopDetectionSdr10(initializeVariable,callback){
    //     dal_binding.stopDetection(initializeVariable, (resultsdr) => {
    //         console.log("sdr10 stopDetection in beginning",resultsdr) 
    //         callback(resultsdr);
    //     });
    // }
    DalService.prototype.stopDetection = function (initializeVariable, callback) {
        dal_binding.stopDetection(initializeVariable, function (resultbuaroh) {
            console.log("stopDetection for readers in beginning in services", resultbuaroh);
            callback(resultbuaroh);
        });
    };
    DalService.prototype.isCardRemoved = function (initializeVariable, callback) {
        console.log("befor is card removed", initializeVariable);
        dal_binding.isCardRemoved(initializeVariable, function (resp) {
            console.log("card removed in service", resp);
            callback(resp);
        });
    };
    DalService.prototype.getGenericData = function (getgenericParams, callback) {
        dal_binding.getGenericData(getgenericParams, function (resgetgenericRes) {
            console.log("GENERIC DATA in service", resgetgenericRes);
            callback(resgetgenericRes);
        });
    };
    DalService.prototype.recharge = function (rechargeParams, callback) {
        dal_binding.recharge(rechargeParams, function (resRecharge) {
            console.log("recharge from reader", resRecharge);
            callback(resRecharge);
        });
    };
    DalService.prototype.updatePersonalData = function (updateParams, callback) {
        dal_binding.updatePersonalData(updateParams, function (resp) {
            console.log("update from reader", resp);
            callback(resp);
        });
    };
    DalService.prototype.activateCard = function (activateParams, callback) {
        dal_binding.activateCard(activateParams, function (res) {
            console.log("activate from reader", res);
            callback(res);
        });
    };
    DalService.prototype.usePass = function (usePassParams, callback) {
        dal_binding.usePass(usePassParams, function (resUsepass) {
            console.log("activate from reader", resUsepass);
            callback(resUsepass);
        });
    };
    DalService.prototype.pay = function (payparams, callback) {
        dal_binding.pay(payparams, function (resPay) {
            console.log("pay from reader", resPay);
            callback(resPay);
        });
    };
    DalService.prototype.activatePass = function (activatePassparams, callback) {
        dal_binding.activatePass(activatePassparams, function (resultsdr) {
            console.log("activatePass from reader", resultsdr);
            callback(resultsdr);
        });
    };
    DalService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
    ], DalService);
    return DalService;
}());



/***/ }),

/***/ "./src/app/services/token.interceptor.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TokenInterceptor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common_service__ = __webpack_require__("./src/app/services/common.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TokenInterceptor = (function () {
    function TokenInterceptor(commonServ) {
        this.commonServ = commonServ;
    }
    TokenInterceptor.prototype.intercept = function (request, next) {
        console.log('intercepted request ... ');
        if (request.url.includes('token') || request.url.includes('revoke')) {
            request = request.clone({ headers: request.headers.set('Content-Type', 'application/x-www-form-urlencoded') });
        }
        var token = this.commonServ.getToken();
        request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
        return next.handle(request);
    };
    TokenInterceptor = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__common_service__["a" /* CommonBaseService */]])
    ], TokenInterceptor);
    return TokenInterceptor;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var baseUrl = "http://139.59.35.232";
var environment = {
    production: true,
    apiurls: {
        "isLiveApi": true,
        //login Api
        "loginApi": baseUrl + "/v1/auth",
        //get operator
        "userserv": baseUrl + "/v1/account",
        //get fee revision
        "domainserv": baseUrl + "/v1/domain",
        //activity apis
        "activityserv": baseUrl + "/v1/account",
        //card related activities
        "cardserv": baseUrl + "/v1/device",
        "businessserv": baseUrl + "/v1/business",
        "entityserv": "http://139.59.5.154/v1/entity",
        "authserv": "http://139.59.5.154/v1/auth",
        "contentserv": "http://139.59.5.154/v1/content",
        "swmgmtserv": "http://139.59.5.154/",
        "deviceserv": "http://139.59.5.154/v1/asset",
        "checkoutserv": "http://139.59.5.154/v1/checkout",
        "supportserv": "http://139.59.5.154/v1/support",
        "trackingServ": "http://139.59.5.154/v1/tracking",
        "mmscheckoutserv": "http://139.59.5.154/v1/checkout",
        "mmsdeviceserv": "http://139.59.5.154/v1/asset",
        "mmsauthserv": "http://139.59.5.154/v1/business/auth",
        "mmssupportserv": "http://139.59.5.154/v1/support",
        "mmsuserserv": "http://139.59.5.154/v1/account",
        "mmsbusinessserv": "http://139.59.5.154/v1/business/business",
        "mmsdomainserv": "http://139.59.5.154/v1/domain"
    },
    "ds": {
        "activity": "http://localhost:9200/activities",
        "updateCard": "http://localhost:9200/cards/updateCard",
        "activateCard": "http://localhost:9200/cards/activateCard",
        "recentTransaction": "http://localhost:9200/activities/recent",
        "feeActivities": "http://localhost:9200/toll",
    },
    "readers": {
        'sdr10': {
            "path": "C:/felica_readers/SDR-10/build/Release/sdr-10_felica",
        },
        'duali': {
            "path": "D:/Duali/SampleApp/build/Release/duali_felica"
        },
        'mquest': {
            "path": "D:/UsbChatPortwithC/UsbChatPortwithC/build/Release/mquest_felica",
        },
        'sony': {
            "path": "D:/Sony/SampleApp/build/Release/sony_felica"
        },
        'buaroh': {
            //  "path" : "C:/BuarohSerialPort/BuarohSerialPort/build/Release/br_felica",
            "path": "C:/felica_readers/Buaroh_8210_lib/BuarohSerialPort/build/Release/br_felica",
        },
        'dal_binding': {
            "path": "C:/posreaderlib/build/Release/posappnode"
        }
    },
    //local storage object 
    "storeInfo": {
        "location": {},
        "toll": {
            "accesstoken": "",
            "refreshtoken": "",
            "cartid": "",
            "usertype": "TOLL-COMPANY",
            "companyaccesstoken": "",
            "branchaccesstoken": ""
        }
    },
    "vehicleType": {
        "bus/truck": {
            "image": "../src/images/truckblack@2x.png",
            "label": "",
        },
        "4 to 6 Axle Vehicle": {
            "image": "../src/images/axelblack-1@2x.png",
            "label": "",
        },
        "Car/Jeep/Van": {
            "image": "../src/images/carblack@2x.png",
            "label": "",
        },
        "Upto 3 Axle Vehicle": {
            "image": "../src/images/axelblack-1@2x.png",
            "label": "",
        },
        "LCV": {
            "image": "../src/images/vanblack@2x.png",
            "label": "",
        },
        "7 or more Axle Vehicle": {
            "image": "../src/images/axelblack-3@2x.png",
            "label": "",
        },
        "HCM/EME": {
            "image": "../src/images/axelblack-2@2x.png",
            "label": "",
        },
        "others": {
            "image": "../src/images/axelbiggerblack-3@2x.png",
            "label": ""
        },
    },
    "vehicleTypeFromCard": "",
    "feeStructure": {},
    "sdrInitializeVariable": {
        "readerType": "SCSPRO_READER",
        "readerIndex": 0,
        "serviceType": "TOLL",
        "branchId": "123456789"
    },
    "buarohinitializeVariable": {
        "readerType": "BUAROH_READER",
        "readerIndex": 0,
        "serviceType": "TOLL",
        "branchId": "123456789"
    },
    "buarohinitializeVariable2": {
        "readerType": "BUAROH_READER",
        "readerIndex": 1,
        "serviceType": "TOLL",
        "branchId": "123456789"
    },
    "feeStructureLimits": [],
    "cardInfo": {
        "passes": [],
    },
    "businessLevelId": {
        "sub": "",
    },
    "operatorDetails": {},
    "tollInfo": {},
    "companyInfo": {},
    "readerId": "1157610283554512716",
    "timeDuration": {
        "day": 86400000,
        "monthly": 2592000000,
        "weekly": 604800000
    },
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_16" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map