webpackJsonp(["common"],{

/***/ "../posreaderlib/build/Release/posappnode.node":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, module) {try {global.process.dlopen(module, "C:\\posreaderlib\\build\\Release\\posappnode.node"); } catch(e) {throw new Error('Cannot open ' + "C:\\posreaderlib\\build\\Release\\posappnode.node" + ': ' + e);}
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/webpack/buildin/global.js"), __webpack_require__("./node_modules/webpack/buildin/module.js")(module)))

/***/ }),

/***/ "./node_modules/webpack/buildin/module.js":
/***/ (function(module, exports) {

module.exports = function(module) {
	if(!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if(!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_base64_service__ = __webpack_require__("./src/app/services/base64.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var dal_binding = __webpack_require__("../posreaderlib/build/Release/posappnode.node");
var DashboardComponent = (function () {
    function DashboardComponent(commonserve, modalService, ref, base64, router) {
        var _this = this;
        this.commonserve = commonserve;
        this.modalService = modalService;
        this.ref = ref;
        this.base64 = base64;
        this.router = router;
        this.hide_First_Screen = false;
        this.hide_Second_Screen = true;
        this.vehicle_Cat_no = 1;
        this.wrongVehicleModalDisplay = false;
        this.tempCardInfo = {
            aadhaar: "",
            balance: "",
            cardNumber: "",
            name: "",
            mobile: "",
            vehicleNumber: "",
            vehicleType: "",
            passes: []
        };
        this.vehicl = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].vehicleType;
        this.vehicleName = [];
        this.vehicleNameSecond = [];
        this.vehicleNameFirst = [];
        //call api for business id
        this.getOperatorDetails();
        //buaroh vaiable
        this.payParams = {
            "serviceType": "TOLL",
            "action": "PAY",
            "posTerminalId": "123456789",
            "amount": '',
            "sourceBranchId": "123456789",
            "destinationBranchId": "12",
            "timeInMilliSeconds": this.getEpocTimestamp(),
            "readerType": "BUAROH_READER",
            "readerIndex": 0
        };
        //single journet return journey params
        this.payApiParams = {
            "cardNumber": '',
            "readerId": "1157610283554512716",
            "activityTime": this.getEpocTimestamp(),
            "amount": {
                "value": this.payParams.amount
            },
            "mode": "TOLL",
            "action": "TOLL_ENTRY",
            "vehicleNumber": '',
            "categoryCode": '',
            "activityCode": '',
        };
        //buaroh variable ends
        this.sdrInitializeVariable = {
            "readerType": "SCSPRO_READER",
            "readerIndex": 0,
            "serviceType": "TOLL",
            "branchId": "123456789"
        };
        this.buarohinitializeVariable = {
            "readerType": "BUAROH_READER",
            "readerIndex": 0,
            "serviceType": "TOLL",
            "branchId": "123456789"
        };
        this.ownername = "";
        this.holderInfo = {
            "vehicleNo": "",
            "cardNo": "",
            "holderName": "",
            "phoneNo": "",
            "aadharNo": "",
            "rfidNo": "",
            "currentAmount": ""
        };
        this.generic = {
            "vehiclenoActive": true,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "rechargeDisable": true,
            "activateDisable": true,
            "updateDisable": true,
            "lostCardActive": false,
            "clearallDisable": true,
            "rechargeInitited": false,
            "updateInitited": false,
            "activateInitited": false,
            "rechargeamountDisable": true,
            "activeVehicleClass": true
            //  "nextVehicleName" : true
        };
        this.recharge_amount = "";
        this.lostCard_number = "";
        this.backgroundBtn = {
            "activatebackground": false,
            "rechargebackground": false,
            "updatebackground": false,
        };
        this.reqObjectRecharge = {
            "cardNumber": "1512734294626",
            "readerId": "4775804607513017209",
            "activityTime": "2017-08-09T13:13:13.511Z",
            "amount": {
                "value": "1000"
            },
            "mode": "TOLL",
            "action": "CARD_TOP_UP"
        };
        this.nextVehicleName = true;
        //use pass variable
        this.usepassobj = {
            'readerType': "BUAROH_READER",
            'readerIndex': 0,
            'amount': 0,
            'action': 'pay',
            'serviceType': "TOLL",
            'posTerminalId': '123456789',
            'sourceBranchId': '123456789',
            'destinationBranchId': '123456789',
            'periodicity': [{
                    'limitPeriodicity': "MONTHLY",
                    'limitMaxCount': 15
                },
                {
                    'limitPeriodicity': "DAILY",
                    'limitMaxCount': 2
                }],
            'expiryDate': 1527773369,
            // 'expiryTimeInMilliSeconds' : 1525844410000,//Tuesday, March 6, 2018 12:06:15 PM GMT+05:30
            'timeInMilliSeconds': this.getEpocTimestamp(),
            'passType': "",
            'maxTrips': 0,
            'partnerTxnId': 9876543210,
            'isRenewal': false
        };
        //check recharge limit before activity
        this.checkRechargeparmas = {
            "mode": "TOLL",
            "amount": {
                "value": parseInt(this.recharge_amount),
            },
            "action": "CARD_TOP_UP",
            "readerId": "1157610283554512716",
            "cardNumber": "",
            "activityTime": this.getEpocTimestamp(),
        };
        this.dallib = dal_binding;
        this.categoryType = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].categoryType;
        if (this.commonserve.initReader) {
            this.dallib.initializeReader(this.sdrInitializeVariable, function (response) {
                console.log("sdr10 initilised", JSON.stringify(response));
            });
            this.dallib.initializeReader(this.buarohinitializeVariable, function (response) {
                console.log("buaroh initilised", response);
            });
            this.commonserve.initReader = 0;
        }
        this.dallib.stopDetection(this.sdrInitializeVariable, function (resultsdr) {
            console.log("sdr10 stopDetection in beginning", resultsdr);
            _this.dallib.stopDetection(_this.buarohinitializeVariable, function (resultbuaroh) {
                _this.definecardDefaults();
                console.log(" buaroh stopDetection in beginning", resultbuaroh);
            });
        });
        //get fee structure
        this.categoryType = "NH";
        this.error = {};
        this.feesetup = {
            "tollInfo": {},
            "structure": {},
        };
        this.feesetup.vehicleTypes = [];
        this.feesetup.journeyTypes = [];
        this.feesetup.passType = [];
        this.feesetup.structure = {};
        this.feesetup.startediting = false;
        this.feesetup.feeCodes = [];
        this.feesetup.feeStructure = {};
        this.feesetup.journeyTypes = [];
        this.feesetup.vehiclecategories = [];
        this.feesetup.tollInfo = {};
        this.feesetup.vehicleTypes = {
            "label": "",
            "class": ""
        };
        //activate card variable initiate
        this.vehicleItem = "";
        this.vehiCategoryExist = "";
        this.totalScreen = 1;
        this.activityCodeApi = "";
    }
    //get vehicle category code
    DashboardComponent.prototype.getVehicleType = function () {
        this.getVehiType = this.cardVehicleType;
        this.ref.detectChanges();
    };
    DashboardComponent.prototype.getVehiclecategory = function (item) {
        var getVehiCategoryTemp = item.replace("-", "_");
        this.getVehiCategory = getVehiCategoryTemp;
        console.log("card veh code", this.getVehiCategory);
        this.ref.detectChanges();
    };
    DashboardComponent.prototype.getCategorycodewithDash = function (item) {
        console.log("item is.....", item);
        var temp = item.replace("_", "-");
        return temp;
    };
    DashboardComponent.prototype.getEpocTimestamp = function () {
        var currentDateTime = new Date();
        var dataInEpoc = currentDateTime.getTime();
        return dataInEpoc;
    };
    //operator details test
    DashboardComponent.prototype.getOperatorDetails = function () {
        var auth = this.commonserve.getStoreData();
        console.log("token is", JSON.stringify(auth));
        var str = auth.accesstoken.split(".");
        var decodestr = str[1];
        var userinfo = this.base64.decode(decodestr);
        console.log("userinfo.....", userinfo);
        var s = userinfo.replace(/\\n/g, "\\n")
            .replace(/\\'/g, "\\'")
            .replace(/\\"/g, '\\"')
            .replace(/\\&/g, "\\&")
            .replace(/\\r/g, "\\r")
            .replace(/\\t/g, "\\t")
            .replace(/\\b/g, "\\b")
            .replace(/\\f/g, "\\f");
        s = s.replace(/[\u0000-\u0019]+/g, "");
        var obj = JSON.parse(s);
        this.userid = Object.keys(obj.businessProfile);
        this.gettoll(this.userid[0]);
    };
    ;
    //restrict character or number function ends
    DashboardComponent.prototype.onlyNumberKey = function (event) {
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    };
    DashboardComponent.prototype.restrictNumeric = function (e) {
        return (e.charCode == 8 || e.charCode == 0 || e.charCode == 32) ? null : e.charCode >= 97 && e.charCode <= 122 || e.charCode >= 65 && e.charCode <= 90;
    };
    DashboardComponent.prototype.restrictSpecialcharachter = function (e) {
        return (e.charCode == 8 || e.charCode == 0) ? null : e.charCode >= 97 && e.charCode <= 122 || e.charCode >= 65 && e.charCode <= 90 || e.charCode >= 48 && e.charCode <= 57;
    };
    //restrict character or number function ends
    DashboardComponent.prototype.numberButtonClick = function (rate, type) {
        if (this.recharge_amount.toString().length < 6) {
            var temp = this.recharge_amount + rate;
            this.recharge_amount = parseInt(temp);
            this.digitBackgroundVar = true;
        }
    };
    ;
    DashboardComponent.prototype.rateButtonClick = function (rate, type) {
        this.recharge_amount = rate;
    };
    ;
    DashboardComponent.prototype.previousScreen = function () {
        this.hide_Second_Screen = true;
        this.hide_First_Screen = false;
        this.vehicle_Cat_no = 1;
        this.totalScreen = 1;
    };
    DashboardComponent.prototype.nextScreen = function () {
        this.hide_Second_Screen = false;
        this.hide_First_Screen = true;
        this.vehicle_Cat_no = 2;
        this.totalScreen = 2;
    };
    //seperate vehicle type from vehicle code
    DashboardComponent.prototype.returnVehicleCode = function (split) {
        var VehicleCode = [];
        VehicleCode = split.split('_');
        var tempVehicleCode = [];
        for (var i = 2; i < VehicleCode.length; i++) {
            tempVehicleCode.push(VehicleCode[i]);
            console.log("came code", tempVehicleCode);
        }
        var vehicleCodeTemp;
        vehicleCodeTemp = tempVehicleCode.join('_');
        console.log("came now.....", tempVehicleCode);
        return vehicleCodeTemp;
    };
    //seperate vehicle type from vehicle code
    DashboardComponent.prototype.returnVehicleCategorycode = function (split) {
        var VehicleCode = [];
        VehicleCode = split.split('_');
        var tempVehicleCode = [];
        for (var i = 0; i <= 1; i++) {
            tempVehicleCode.push(VehicleCode[i]);
            console.log("came code", tempVehicleCode);
        }
        var vehicleCodeTemp;
        vehicleCodeTemp = tempVehicleCode.join('-');
        console.log("came now.....", tempVehicleCode);
        return vehicleCodeTemp;
    };
    //end
    //changeaadhar mode
    DashboardComponent.prototype.changeAadharMode = function (flag) {
        this.editAadharFlag = flag;
        this.ref.detectChanges();
    };
    //navigate to pass type for activation
    DashboardComponent.prototype.passNavigate = function (passtype) {
        console.log("pass type navigate", passtype);
        switch (passtype) {
            case "Single Journey":
                // this.router.navigate(['/dashboard/passes/singlepass']);   
                break;
            case "Commercial Vehicle Registered within the district of plaza":
                this.router.navigate(['/dashboard/passes/localpass']);
                break;
            case "Return Journey":
                // this.router.navigate(['/dashboard/passes/returnpass']);   
                break;
            case "Monthly Pass/Concessions":
                this.router.navigate(['/dashboard/passes/monthlypass']);
                break;
        }
    };
    //clear all button action
    DashboardComponent.prototype.clearAll = function () {
        this.holderInfo = {
            "vehicleNo": "",
            "cardNo": "",
            "holderName": "",
            "phoneNo": "",
            "aadharNo": "",
            "rfidNo": "",
            "currentAmount": "",
        };
        this.lostCard_number = "";
        this.ref.detectChanges();
        this.getGenericDataFromCard();
    };
    //card function starts to call defualt card display
    DashboardComponent.prototype.definecardDefaults = function () {
        this.payParams.amount = "";
        this.cardInfo = "";
        this.holderInfo = {
            "vehicleNo": "",
            "cardNo": "",
            "holderName": "",
            "phoneNo": "",
            "aadharNo": "",
            "rfidNo": "",
            "currentAmount": "",
        };
        this.lostCard_number = "";
        this.generic = {
            "rechargeInitited": false,
            "updateInitited": false,
            "activateInitited": false,
            "rechargeDisable": true,
            "activateDisable": true,
            "updateDisable": true,
            "rechargeamountDisable": true,
            "doneDisable": true,
            "vehiclenoActive": true,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "lostCardActive": false,
            "clearallDisable": true,
        };
        this.recharge_amount = "";
        this.backgroundBtn = {
            "activatebackground": false,
            "rechargebackground": false,
            "updatebackground": false,
        };
        this.cardVehicleType = "";
        this.vehiCategoryExist = "";
        this.activityCodeApi = "";
        this.ref.detectChanges();
        this.getGenericDataFromCard();
    };
    //get pass type
    DashboardComponent.prototype.getPassType = function (type) {
        switch (type) {
            case "MONTHLY_PASS":
                return "MONTHLY";
            case "COMMERCIAL_VEHICLE_WITHIN_DISTRICT":
                return "LOCAL";
            case "RETURN_JOURNEY":
                return "RETURN_JOURNEY";
            case "SINGLE_JOURNEY":
                return "SINGLE_JOURNEY";
        }
    };
    //display generic data function
    DashboardComponent.prototype.genericAction = function () {
        this.editAadharFlag = false;
        this.holderInfo = {
            "vehicleNo": this.cardInfo.vehicleNumber,
            "cardNo": this.cardInfo.cardNumber,
            "holderName": this.cardInfo.name,
            "phoneNo": this.cardInfo.mobile,
            "aadharNo": this.cardInfo.aadhaar,
            "rfidNo": "",
            "currentAmount": this.cardInfo.balance,
        };
        __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].vehicleTypeFromCard = this.cardInfo.vehicleType;
        this.cardVehicleType = this.returnVehicleCode(this.cardInfo.vehicleType);
        this.vehiCategoryExist = this.returnVehicleCategorycode(this.cardInfo.vehicleType);
        this.backgroundBtn = {
            "activatebackground": false,
            "rechargebackground": false,
            "updatebackground": false,
        };
        this.recharge_amount = "";
        this.generic = {
            "vehiclenoActive": true,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "rechargeDisable": false,
            "activateDisable": true,
            "updateDisable": false,
            "lostCardActive": true,
            "clearallDisable": true,
            "rechargeInitited": false,
            "updateInitited": false,
            "activateInitited": false,
            "rechargeamountDisable": true,
            "doneDisable": true,
            "activeVehicleClass": true,
        };
        this.ref.detectChanges();
    };
    //doestore api to store recharge record
    DashboardComponent.prototype.doeStoreRechargeapi = function () {
        this.reqObjectRecharge = {
            "cardNumber": "1512734294626",
            "readerId": "4775804607513017209",
            "activityTime": "2017-08-09T13:13:13.511Z",
            "amount": {
                "value": "100"
            },
            "mode": "TOLL",
            "action": "CARD_TOP_UP"
        };
        this.commonserve.createNewTransaction(this.reqObjectRecharge).subscribe(function (response) {
            console.log("response", response);
        }, function (err) {
            console.log("error is ", err);
        });
    };
    //doestore to store update record
    DashboardComponent.prototype.doeStoreUpdateapi = function () {
        var reqObjectUpdate = {
            "name": this.holderInfo.holderName,
            "cardId": this.holderInfo.cardNo,
        };
        this.commonserve.updateDoeStore(reqObjectUpdate).subscribe(function (response) {
            console.log("response", response);
        }, function (err) {
            console.log("error is ", err);
        });
    };
    //doestore to store newly activated card
    DashboardComponent.prototype.doeStoreActivateapi = function () {
        var reqObjectActivate = {
            "cardNumber": this.holderInfo.cardNo,
            "allocation": {
                "allocatedBusinessLevelId": "",
                "issuerBusinessLevelId": "",
                "readerId": "",
                "allocationType": "",
                "previousCardId": ""
            },
            "status": "PROCURED",
            "cardName": this.holderInfo.holderName,
            "aadhaar": "this.holderInfo.aadharNo",
            "primaryCardUser": {
                "mobileNumber": "string"
            },
            "initialBalance": {
                "currency": "",
                "value": 100
            }
        };
        this.commonserve.activateDoeStore(reqObjectActivate).subscribe(function (response) {
            console.log("response", response);
        }, function (err) {
            console.log("error is ", err);
        });
    };
    //recharge button action
    DashboardComponent.prototype.recharge = function () {
        console.log("reader type in recharge", this.cardInfo.readerType);
        console.log("time is", this.currentDateTime);
        if (this.cardInfo.readerType == "SCSPRO_READER") {
            this.backgroundBtn = {
                "rechargebackground": true,
            };
            this.generic = {
                "rechargeDisable": false,
                "activateDisable": true,
                "updateDisable": true,
                "rechargeInitited": true,
                "lostCardActive": true,
                "rechargeamountDisable": false,
                "vehiclenoActive": true,
                "nameActive": true,
                "mobilenoActive": true,
                "cardnoActive": true,
                "aadharnoActive": true,
                "rfidActive": true,
                "clearallDisable": true,
            };
            this.ref.detectChanges();
        }
    };
    //update button
    DashboardComponent.prototype.update = function () {
        //  this.editAadharFlag = false;
        this.backgroundBtn = {
            "updatebackground": true
        };
        this.generic = {
            "aadharnoActive": false,
            "rechargeDisable": true,
            "activateDisable": true,
            "updateDisable": false,
            "lostCardActive": true,
            "updateInitited": true,
            "rechargeamountDisable": true,
            "clearallDisable": true,
            "doneDisable": false,
        };
        this.ref.detectChanges();
    };
    //activate button
    DashboardComponent.prototype.activate = function () {
        this.backgroundBtn = {
            "activatebackground": true,
        };
        this.generic = {
            "rechargeDisable": true,
            "activateDisable": true,
            "updateDisable": true,
            "rechargeamountDisable": true,
            "lostCardActive": true,
            "activateInitited": true,
            "doneDisable": false,
        };
        this.ref.detectChanges();
    };
    //check before activation
    DashboardComponent.prototype.checkBeforeActiveCard = function () {
        if (this.holderInfo.holderName && this.holderInfo.phoneNo && this.holderInfo.vehicleNo && this.holderInfo.cardNo && this.getVehiType && this.getVehiCategory)
            return true;
    };
    //after recharge action
    DashboardComponent.prototype.afterRecharge = function () {
        this.tempAction = true;
        this.backgroundBtn = {
            "rechargebackground": false,
        };
        this.generic = {
            "rechargeDisable": false,
            "activateDisable": false,
            "updateDisable": false,
            "rechargeamountDisable": true,
            "lostCardActive": false,
            "updateInitited": false,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "vehiclenoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "clearallDisable": true,
            "doneDisable": true,
        };
        this.recharge_amount = '';
        this.cardActionResponse(this.popup);
        this.ref.detectChanges();
    };
    //after update action
    DashboardComponent.prototype.afterUpdate = function () {
        this.backgroundBtn = {
            "updatebackground": false
        };
        this.generic = {
            "rechargeDisable": false,
            "activateDisable": false,
            "updateDisable": false,
            "updateInitited": false,
            "rechargeamountDisable": true,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "vehiclenoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "clearallDisable": true,
            "doneDisable": true,
        };
        //this.editAadharFlag = false;
        this.lostCard_number = "";
        this.cardActionResponse(this.popup);
        this.ref.detectChanges();
    };
    //after activate action
    DashboardComponent.prototype.afterActivate = function () {
        this.backgroundBtn = {
            "activatebackground": false,
        };
        this.generic = {
            "rechargeDisable": false,
            "activateDisable": true,
            "updateDisable": false,
            "activateInitited": false,
            "rechargeamountDisable": true,
            "doneDisable": true,
            "vehiclenoActive": true,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "lostCardActive": false,
            "clearallDisable": true,
        };
        this.lostCard_number = "";
        this.cardActionResponse(this.popup);
        this.ref.detectChanges();
    };
    //first tym activation action
    DashboardComponent.prototype.activateCardAction = function () {
        window.alert("Pleasee activate the card first.");
        this.generic = {
            "rechargeDisable": true,
            "activateDisable": false,
            "updateDisable": true,
        };
        this.ref.detectChanges();
    };
    //done button click action
    DashboardComponent.prototype.doneButtonClick = function (popup) {
        var _this = this;
        this.popup = popup;
        console.log("reader type in done", this.cardInfo.readerType);
        if (this.cardInfo.readerType == "SCSPRO_READER") {
            if (this.generic.updateInitited == true) {
                dal_binding.stopDetection(this.sdrInitializeVariable, function (resultForUpdate) {
                    var updateParams = {
                        "firstName": _this.holderInfo.holderName,
                        "name": _this.holderInfo.holderName,
                        "middleName": _this.holderInfo.holderName,
                        "lastName": _this.holderInfo.holderName,
                        "gender": "MALE",
                        "dateOfBirth": 1234567,
                        "mobile": parseInt(_this.holderInfo.phoneNo),
                        "aadhaar": parseInt(_this.holderInfo.aadharNo),
                        "timeInMilliSeconds": _this.getEpocTimestamp(),
                        "readerType": "SCSPRO_READER",
                        "readerIndex": 0
                    };
                    dal_binding.updatePersonalData(updateParams, function (resp) {
                        console.log("update", resp);
                        // this.doeStoreUpdateapi();
                        _this.afterUpdate();
                        _this.ref.detectChanges();
                        dal_binding.isCardRemoved(_this.sdrInitializeVariable, function (response) {
                            if (response.cardRemoved == true) {
                                _this.definecardDefaults();
                            }
                        });
                    });
                });
            }
            if (this.generic.activateInitited == true) {
                if (this.checkBeforeActiveCard() == true) {
                    this.dallib.stopDetection(this.sdrInitializeVariable, function (resultForRecharge) {
                        //params for dal activate card
                        var activateparams = {
                            "serviceType": "TOLL",
                            "action": "PAY",
                            "posTerminalId": "1234",
                            "amount": 100,
                            "sourceBranchId": "231",
                            "destinationBranchId": "12",
                            "timeInMilliSeconds": _this.getEpocTimestamp(),
                            "readerType": "SCSPRO_READER",
                            "readerIndex": 0,
                            "cardNumber": _this.holderInfo.cardNo,
                            "name": _this.holderInfo.holderName,
                            "mobile": parseInt(_this.holderInfo.phoneNo),
                            "aadhaar": parseInt(_this.holderInfo.aadharNo),
                            "vehicleType": _this.getVehiCategory + '_' + _this.getVehiType,
                            "vehicleNumber": _this.holderInfo.vehicleNo,
                            "cardStatus": "ACTIVATE"
                        };
                        //params for Activate Card Api
                        var activateApiParams = {
                            "cardName": _this.holderInfo.holderName,
                            "cardNumber": _this.holderInfo.cardNo,
                            "aadhaar": _this.holderInfo.aadharNo,
                            "allocation": {
                                "issuerBusinessLevelId": "51134012824156808",
                                "readerId": "1157610283554512716"
                            },
                            "vehicleNumber": _this.holderInfo.vehicleNo,
                            "vehicleType": _this.getVehiCategory + '_' + _this.getVehiType,
                            "deviceTransactionId": _this.getEpocTimestamp(),
                            "primaryCardUser": {
                                "mobileNumber": parseInt(_this.holderInfo.phoneNo)
                            },
                            "initialBalance": {
                                "value": 100
                            }
                        };
                        console.log("activateparams", activateparams);
                        dal_binding.activateCard(activateparams, function (res) {
                            console.log("activate done....................", res);
                            _this.afterActivate();
                            //cal api for Activate Card
                            _this.commonserve.activateCardApi(activateApiParams).subscribe(function (respActivate) {
                                console.log("activate done Api sucess....................", respActivate);
                            }, function (err) {
                                console.log("error in Activate ", err);
                            });
                            _this.ref.detectChanges();
                            dal_binding.isCardRemoved(_this.sdrInitializeVariable, function (response) {
                                if (response.cardRemoved == true) {
                                    _this.definecardDefaults();
                                }
                            });
                        });
                    });
                }
                else {
                    alert("Enter all the field");
                }
            }
            if (this.generic.rechargeInitited == true) {
                if (window.navigator.onLine) {
                    if (this.recharge_amount) {
                        var rechargeParams_1 = {
                            "serviceType": "TOLL",
                            "action": "PAY",
                            "posTerminalId": "1234",
                            "amount": parseInt(this.recharge_amount),
                            "sourceBranchId": "231",
                            "destinationBranchId": "12",
                            "timeInMilliSeconds": this.getEpocTimestamp(),
                            "readerType": "SCSPRO_READER",
                            "readerIndex": 0
                        };
                        //get the amount is valid
                        this.checkRechargeparmas.amount.value = parseInt(this.recharge_amount);
                        this.checkRechargeparmas.cardNumber = this.holderInfo.cardNo;
                        console.log("check recharge is working", this.checkRechargeparmas);
                        this.commonserve.getRechargeCheck(this.checkRechargeparmas).subscribe(function (response) {
                            console.log("response of rech limitssss", response);
                            _this.dallib.stopDetection(_this.sdrInitializeVariable, function (resultsdr) {
                                console.log("stop det in done......", resultsdr);
                                _this.dallib.recharge(rechargeParams_1, function (resRecharge) {
                                    console.log("recharge from reader", resRecharge);
                                    // this.checkRechargeparmas.amount.value = parseInt(this.recharge_amount);
                                    //rechrage api
                                    _this.commonserve.recharge(_this.checkRechargeparmas).subscribe(function (response) {
                                        console.log("recharge done res", response);
                                        var dt = new Date();
                                        var date1 = dt.toISOString(); // current date nd time
                                        _this.holderInfo.currentAmount = resRecharge.amount;
                                        _this.afterRecharge();
                                    }, function (err) {
                                        console.log("error in recharge ", err);
                                        alert(err.error.message);
                                    });
                                    //recharge api end
                                    _this.dallib.isCardRemoved(_this.sdrInitializeVariable, function (resp) {
                                        if (resp.cardRemoved == true) {
                                            _this.definecardDefaults();
                                        }
                                    });
                                });
                            });
                        }, function (err) {
                            console.log("u have crossed 20000 limit", err.error.message);
                            alert(err.error.message);
                        });
                        //end
                    }
                    else {
                        alert("Please enter the amount");
                    }
                }
                else {
                    alert("cannot recharge in offline mode");
                }
            }
        }
    };
    //single journey && RETURN JOURNET
    DashboardComponent.prototype.noPassActivity = function () {
        var vehicleTypePayparams = this.cardInfo.vehicleType;
        if (this.cardInfo.tollLogs.length == 0) {
            this.activityCodeApi = "SINGLE_JOURNEY";
            this.payParams.amount = parseInt(this.feesetup.structure[vehicleTypePayparams].SINGLE_JOURNEY.value);
        }
        else {
            for (var i = 0; i < this.cardInfo.tollLogs.length; i++) {
                var next24Hours = this.cardInfo.tollLogs[0].inDateTime * 1000 + 86400000;
                if (this.getEpocTimestamp() < next24Hours) {
                    this.activityCodeApi = "RETURN_JOURNEY";
                    this.payParams.amount = parseInt(this.feesetup.structure[vehicleTypePayparams].RETURN_JOURNEY.value) - parseInt(this.feesetup.structure[vehicleTypePayparams].SINGLE_JOURNEY.value);
                }
                else {
                    this.activityCodeApi = "SINGLE_JOURNEY";
                    this.payParams.amount = parseInt(this.feesetup.structure[vehicleTypePayparams].SINGLE_JOURNEY.value);
                }
            }
        }
    };
    DashboardComponent.prototype.scsproGetGenericDataFromCard = function () {
        var _this = this;
        this.dallib.getGenericData(this.sdrInitializeVariable, function (resgetgError) {
            console.log("getg in error if card is removed in between", resgetgError);
            _this.dallib.stopDetection(_this.buarohinitializeVariable, function (buarohstop) {
                console.log("buaroh stop detection IN sdr10", buarohstop);
            });
            _this.cardInfo = resgetgError;
            _this.genericAction();
            _this.dallib.isCardRemoved(_this.sdrInitializeVariable, function (respCardremError) {
                if (respCardremError.cardRemoved == true) {
                    console.log("card removed from sdr10", respCardremError);
                    _this.definecardDefaults();
                }
            });
        });
    };
    DashboardComponent.prototype.buarohGetGenericDataFromCard = function () {
        var _this = this;
        this.dallib.getGenericData(this.buarohinitializeVariable, function (resgetgenericError) {
            console.log("getGenericDataFromCard in error in buaroh", resgetgenericError);
            if (resgetgenericError.error) {
                console.log("error in error handling of buaroh", resgetgenericError.error);
                _this.getGenericDataFromCard();
            }
            else {
                _this.dallib.stopDetection(_this.sdrInitializeVariable, function (resultForRecharge) {
                    console.log("sdr10 stop detection IN BUAROH", resultForRecharge);
                });
                _this.payParams.amount = parseInt(_this.feesetup.structure.NON_COMMERCIAL_CAR.SINGLE_JOURNEY.value);
                _this.dallib.pay(_this.payParams, function (resPayError) {
                    _this.cardInfo = resgetgenericError;
                    _this.genericAction();
                    _this.holderInfo.currentAmount = resPayError.amount;
                    _this.ref.detectChanges();
                    console.log("payment is deducted in error", resPayError);
                    _this.dallib.isCardRemoved(_this.buarohinitializeVariable, function (resultError) {
                        console.log("buaroh reader card remeoved in error handling", resultError);
                        if (resultError.cardRemoved == true) {
                            console.log("card removed from buaroh in error handling", resultError);
                            _this.definecardDefaults();
                        }
                    });
                });
            }
        });
    };
    //call getGenericDataFromCard from card
    DashboardComponent.prototype.getGenericDataFromCard = function () {
        var _this = this;
        this.dallib.getGenericData(this.buarohinitializeVariable, function (res) {
            _this.payParams = {
                "serviceType": "TOLL",
                "action": "PAY",
                "posTerminalId": "123456789",
                "amount": '',
                "sourceBranchId": "123456789",
                "destinationBranchId": "12",
                "timeInMilliSeconds": _this.getEpocTimestamp(),
                "readerType": "BUAROH_READER",
                "readerIndex": 0
            };
            if (res.error) {
                console.log("buarohError", res);
                if (res.error.message == "Card Authentication failed") {
                    _this.buarohGetGenericDataFromCard();
                }
                if (res.error.message == "Polling Command Failed") {
                    _this.buarohGetGenericDataFromCard();
                }
                if (res.error.message == "reading from the card failed") {
                    _this.buarohGetGenericDataFromCard();
                }
            }
            else {
                console.log("buaroh generic data", res);
                // STOP DETECTION FOR SDR10
                _this.dallib.stopDetection(_this.sdrInitializeVariable, function (resultForRecharge) {
                    console.log("sdr10 stop detection IN BUAROH", resultForRecharge);
                });
                _this.cardInfo = res;
                _this.noPassActivity();
                _this.payApiParams = {
                    "cardNumber": _this.cardInfo.cardNumber,
                    "readerId": "1157610283554512716",
                    "activityTime": _this.getEpocTimestamp(),
                    "amount": {
                        "value": _this.payParams.amount
                    },
                    "mode": "TOLL",
                    "action": "TOLL_ENTRY",
                    "vehicleNumber": _this.cardInfo.vehicleNumber,
                    "categoryCode": _this.cardInfo.vehicleType,
                    "activityCode": _this.activityCodeApi,
                };
                if (res.passes[0] == null) {
                    //CAL SINGLE JOURNEY AND RETURN JOURNEY
                    _this.dallib.pay(_this.payParams, function (resPay) {
                        _this.genericAction();
                        _this.holderInfo.currentAmount = resPay.amount;
                        //api cal for transaction
                        _this.payApiParams.amount.value = _this.payParams.amount;
                        _this.commonserve.transactionApi(_this.payApiParams)
                            .subscribe(function (respTransaction) {
                            console.log("transaction api success", respTransaction);
                        }, function (err) {
                            console.log("error in transaction ", err);
                            alert(err.error.message);
                        });
                        _this.ref.detectChanges();
                        console.log("payment is deducted with no pass", resPay);
                        _this.dallib.isCardRemoved(_this.buarohinitializeVariable, function (result) {
                            if (result.cardRemoved == true) {
                                console.log("card removed from buaroh", result);
                                _this.definecardDefaults();
                            }
                        });
                    });
                }
                else {
                    //use pass test
                    _this.payParams.amount = "";
                    var maxlimit = _this.cardInfo.passes[0].maxTrips - 1;
                    _this.usepassobj = {
                        'readerType': "BUAROH_READER",
                        'readerIndex': 0,
                        'amount': 0,
                        'action': 'pay',
                        'serviceType': "TOLL",
                        'posTerminalId': '123456789',
                        'sourceBranchId': '123456789',
                        'destinationBranchId': '123456789',
                        'periodicity': [{
                                'limitPeriodicity': "MONTHLY",
                                'limitMaxCount': 15
                            },
                            {
                                'limitPeriodicity': "DAILY",
                                'limitMaxCount': 2
                            }],
                        'expiryDate': 1534748898,
                        'timeInMilliSeconds': _this.getEpocTimestamp(),
                        'passType': _this.cardInfo.passes[0].passType,
                        'maxTrips': maxlimit,
                        'partnerTxnId': 9876543210,
                        'isRenewal': false
                    };
                    _this.dallib.usePass(_this.usepassobj, function (resUsepass) {
                        console.log("used pass word", resUsepass);
                        console.log("max trip is", _this.cardInfo.passes[0].maxTrips);
                        _this.genericAction();
                        _this.holderInfo.currentAmount = res.balance;
                        _this.ref.detectChanges();
                        console.log("payment is deducted", resUsepass);
                        _this.dallib.isCardRemoved(_this.buarohinitializeVariable, function (result) {
                            console.log("buaroh reader card remeoved", result);
                            if (result.cardRemoved == true) {
                                console.log("card removed from buaroh", result);
                                _this.definecardDefaults();
                            }
                        });
                    });
                    //use pass test end
                }
            }
        });
        this.dallib.getGenericData(this.sdrInitializeVariable, function (response) {
            console.log("getg of sdr10.....", response);
            if (response.error) {
                console.log("SDR10 ERR", response);
                if (response.error.message == "Card Authentication failed") {
                    console.log("card removed in middle");
                    _this.scsproGetGenericDataFromCard();
                }
                if (response.error.message == "Polling Command Failed") {
                    console.log("polling error middle");
                    _this.scsproGetGenericDataFromCard();
                }
            }
            else {
                //STOP DETECTION FOR BUAROH
                _this.dallib.stopDetection(_this.buarohinitializeVariable, function (buarohstop) {
                    console.log("buaroh stop detection IN sdr10", buarohstop);
                });
                _this.cardInfo = response;
                _this.tempCardInfo = response;
                console.log("sdr10 generic data", _this.cardInfo);
                if (_this.lostCard_number) {
                    //this.generic.activateDisable = false;
                    if (_this.cardInfo.phoneno) {
                        alert('card is already activated');
                    }
                    else {
                        _this.holderInfo.cardNo = _this.cardInfo.cardno;
                    }
                }
                else {
                    _this.genericAction();
                    if (response.cardStatus != 'ACTIVATE') {
                        _this.activateCardAction();
                    }
                    _this.dallib.isCardRemoved(_this.sdrInitializeVariable, function (resp) {
                        if (resp.cardRemoved == true) {
                            console.log("card removed from sdr10", resp);
                            _this.definecardDefaults();
                        }
                    });
                }
            }
        });
    };
    DashboardComponent.prototype.open = function (content) {
        var _this = this;
        this.modalService.open(content).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    DashboardComponent.prototype.otpFingerscan = function (otpFinger) {
        var _this = this;
        this.modalService.open(otpFinger).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    DashboardComponent.prototype.otpFingerConfirmed = function (otpFingerconfirmed) {
        var _this = this;
        this.modalService.open(otpFingerconfirmed).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    DashboardComponent.prototype.otpFingerNotRecognized = function (otpFingernotRecognized) {
        var _this = this;
        this.modalService.open(otpFingernotRecognized).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    DashboardComponent.prototype.otpConfirmedFinger = function (otpconfirmedFinger) {
        var _this = this;
        this.modalService.open(otpconfirmedFinger).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    DashboardComponent.prototype.optInputFinger = function (optinputFinger) {
        var _this = this;
        this.modalService.open(optinputFinger).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    DashboardComponent.prototype.otpOrFinger = function (otporFinger) {
        var _this = this;
        this.modalService.open(otporFinger).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    DashboardComponent.prototype.cardActionResponse = function (cardactionResponse) {
        var _this = this;
        this.modalService.open(cardactionResponse).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    DashboardComponent.prototype.getDismissReason = function (reason) {
        if (reason === __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["a" /* ModalDismissReasons */].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["a" /* ModalDismissReasons */].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    //get fee structure fuctn
    DashboardComponent.prototype.gettoll = function (bussiness_id) {
        var _this = this;
        var queryparam = "?business_level_id=" + bussiness_id;
        this.commonserve.apigetDomaindata(queryparam)
            .subscribe(function (response) {
            console.log("toll info=======================>>");
            _this.domaindataresponse = response;
            _this.feesetup.tollInfo = _this.domaindataresponse && _this.domaindataresponse.items && _this.domaindataresponse.items[0] ? _this.domaindataresponse.items[0] : {};
            console.log(response);
            for (var i = 0; i < _this.feesetup.tollInfo.feeRevisions.length; i++) {
                var effectiveDate = new Date(_this.feesetup.tollInfo.feeRevisions[i].effectiveTill).getTime();
                var todayDate = new Date().getTime();
                if (todayDate < effectiveDate) {
                    _this.feesetup.feeStructure = _this.feesetup.tollInfo.feeRevisions[i];
                    console.log("fee structure ===========>>");
                    console.log(_this.feesetup.feeStructure);
                    _this.updateFeeStructure();
                }
            }
        });
    };
    DashboardComponent.prototype.updateFeeStructure = function () {
        var _this = this;
        this.feesetup.vehicleTypes = [];
        this.feesetup.journeyTypes = [];
        this.feesetup.passType = [];
        this.feesetup.structure = {};
        if (this.feesetup.feeStructure.baseFees) {
            this.feesetup.feeStructure.baseFees.forEach(function (item, index) {
                if (_this.feesetup.structure[item.categoryCode]) {
                    _this.feesetup.structure[item.categoryCode][item.activityCode] = {
                        "id": item.id,
                        "data": item,
                        "value": item.amount.value
                    };
                }
                else {
                    _this.feesetup.structure[item.categoryCode] = {};
                    _this.feesetup.structure[item.categoryCode][item.activityCode] = {
                        "id": item.id,
                        "data": item,
                        "value": item.amount.value
                    };
                }
                var jindex = _this.feesetup.journeyTypes.findIndex(function (x) { return x.code == item.activityCode; });
                if (!(jindex > -1)) {
                    _this.feesetup.journeyTypes.push({
                        "code": item.activityCode,
                        "label": item.activityCodeInfo.label.replace("_", ""),
                        "info": item.activityCodeInfo
                    });
                    console.log("cattt type is", _this.feesetup.journeyTypes);
                }
                var vindex = _this.feesetup.vehicleTypes.findIndex(function (y) { return y.code == item.categoryCode; });
                if (!(vindex > -1)) {
                    var label = item.categoryCodeInfo.label.split('_');
                    var VehicleCode = [];
                    VehicleCode = item.categoryCodeInfo.code.split('_');
                    //let vehicleCode = code1.indexOf('_',code1.indexOf('_') + 1);
                    console.log("vehicle code=============================>", VehicleCode);
                    console.log("lable=============================>", label);
                    //take vehicle code
                    var tempVehicleCode = [];
                    for (var i = 2; i < VehicleCode.length; i++) {
                        tempVehicleCode.push(VehicleCode[i]);
                        console.log("came code", tempVehicleCode);
                    }
                    var vehicleCodeTemp;
                    vehicleCodeTemp = tempVehicleCode.join('_');
                    console.log("came now.....", tempVehicleCode);
                    var vehicleClas = ["vehicle-buttons--bus", "vehicle-buttons--car", "vehicle-buttons--small_truck", "vehicle-buttons--container", "vehicle-buttons--bigger_truck", "vehicle-buttons--Road_lorry", "vehicle-buttons--lorry", "vehicle-buttons--truck"];
                    for (var i = vehicleClas.length - 1; i < _this.feesetup.vehicleTypes.length; i++) {
                        vehicleClas = vehicleClas.concat(vehicleClas);
                    }
                    var vehicleCategory = [];
                    for (var i = 0; i < _this.feesetup.vehicleTypes.length; i++) {
                        var category = item.categoryCodeInfo.label.split("_");
                        var categoryTemp = category[0].toUpperCase();
                        vehicleCategory.push(categoryTemp);
                    }
                    //sort the array vehicleCategory and remove same category code
                    vehicleCategory.sort();
                    for (var i = 0; i < vehicleCategory.length; i++) {
                        if (vehicleCategory[i] == vehicleCategory[i + 1]) {
                            vehicleCategory.splice(i, 1);
                            i--;
                        }
                    }
                    console.log("demo cat", vehicleCategory);
                    console.log("demo cat ll", vehicleCategory.length);
                    _this.feesetup.vehiclecategories = vehicleCategory;
                    _this.feesetup.vehicleTypes.push({
                        "code": item.categoryCode,
                        "label": label[1],
                        "info": item.categoryCodeInfo,
                        "class": vehicleClas,
                        "vehicleCode": vehicleCodeTemp
                    });
                }
                /*if(!this.feesetup.journeyTypes.includes(item.activityCode)){
                  this.feesetup.journeyTypes.push(item.activityCode);
                }
  
                if(!this.feesetup.vehicleTypes.includes(item.categoryCode)){
                  this.feesetup.vehicleTypes.push(item.categoryCode);
                }*/
            });
        }
        console.log("fee cat isiiiiisss", this.feesetup.vehiclecategories);
        console.log("this is it..............", this.feesetup.vehicleTypes);
        console.log("fee setup is", this.feesetup.structure);
        __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].feeStructure = this.feesetup.structure;
        __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].feeStructureLimits = this.feesetup.feeStructure;
        this.vehicleName = this.feesetup.vehicleTypes;
        //test
        for (var i = 0; i < 8; i++) {
            if (this.vehicleName[i]) {
                this.vehicleNameFirst.push(this.vehicleName[i]);
            }
            else {
                break;
            }
        }
        var countVehicleName = document.getElementById("open-gate-section__top").children.length;
        console.log("number..... number", countVehicleName);
        if (countVehicleName > 8) {
            for (var i = 4; i < 15; i++) {
                if (this.vehicleName[i]) {
                    this.nextVehicleName = false;
                    this.vehicleNameSecond.push(this.vehicleName[i]);
                }
                else {
                    break;
                }
            }
        }
        //test end
        console.log("envv", __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].feeStructureLimits);
        console.log(this.feesetup.feeStructure);
        //fee structure ends
    };
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/dashboard.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_common_service__["a" /* CommonBaseService */],
            __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_5__services_base64_service__["a" /* Base64Service */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"parent-div\">\r\n  <div class=\"dashboard-header\">\r\n    <div class=\"header-section\" style=\"border-right: 1px solid white;\">\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>SERVICE</span>\r\n        </div>\r\n        <div class=\"content-item text-background\">\r\n          <span>DOE PARKING</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>ZONE</span>\r\n        </div>\r\n        <div class=\"content-item text-background\">\r\n          <span>XXXXXXXX</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"header-section\" style=\"border-right: 1px solid white;\">\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>PLAZA NAME</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>NDTL TOLL</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>LANE NO.</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>7</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"header-section\" style=\"border-right: 1px solid white;\">\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>SHIFT NO.</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>787878</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>SUB SHIFT NO.</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>99</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"header-section\" style=\"border-right: 1px solid white;\">\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>LOGIC DATE TIME</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>12/03/2017</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>TOLL COLLECTOR ID</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>1</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"header-section\">\r\n      <div class=\"reader-div\">\r\n        <div class=\"reader-status\">\r\n          <span>READER STATUS</span>\r\n        </div>\r\n        <div class=\"reader-status-button\">\r\n          <button type=\"\" class=\"button-green\"></button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"toll-dashboard\">\r\n    <div class=\" open-gate-section1 \" style=\"display:flex;flex-direction:column;\">\r\n      <div class=\"open-gate-section\" [ngSwitch]=\"categoryType\">\r\n        <div class=\"toll-flavour-style\">\r\n          <div class=\"open-gate-section__top\" id=\"open-gate-section__top\" *ngSwitchCase=\"'NH'\" >\r\n            <div class=\"vehicle_type vehicle_type--border--top\" *ngFor=\"let item of vehicleNameFirst ; let i = index\" [hidden] = \"hide_First_Screen\">\r\n              <button id=\"rate-button\" *ngIf=\"item.vehicleCode != cardVehicleType\" class=\"vehicle-buttons  {{item.class[i]}} hide-outline\" value=\"500\" [ngStyle] = \"{'background-image': 'url(' + vehicl[item.code].image +')'}\" (click)=\"cardVehicleType = item.vehicleCode; getVehicleType();\" >\r\n              <!-- <p>&#2352; </p> -->\r\n              </button>\r\n              <button id=\"rate-button\" *ngIf=\"item.vehicleCode == cardVehicleType\" class=\"vehicle-buttons  {{item.class[i]+'-active'}} hide-outline\" value=\"500\" [ngStyle] = \"{'background-image': 'url(' + vehicl[item.code].image +')'}\" (click)=\"getVehicleType(item.vehicleCode)\">\r\n                 <p *ngIf=\"payParams.amount\">&#2352; {{payParams.amount}}</p>\r\n              </button>\r\n              <p class=\"p-vehicle-type\">{{item.label}}</p>              \r\n            </div>\r\n            <div class=\"vehicle_type vehicle_type--border--top\" *ngFor=\"let item of vehicleNameSecond ; let i = index\" [hidden] = \"hide_Second_Screen\">\r\n              <button id=\"rate-button\" *ngIf=\"item.vehicleCode != cardVehicleType\" class=\"vehicle-buttons  {{item.class[i]}} hide-outline\"  value=\"500\" [ngStyle] = \"{'background-image': 'url(' + vehicl[item.code].image +')'}\" (click)=\"getVehicleType(item.vehicleCode)\">\r\n              <!--   <p>&#2352; </p> -->\r\n              </button> \r\n              <button id=\"rate-button\" *ngIf=\"item.vehicleCode == cardVehicleType\" class=\"vehicle-buttons  {{item.class[i]}} hide-outline\"  value=\"500\" [ngStyle] = \"{'background-image': 'url(' + vehicl[item.code].image +')'}\" (click)=\"getVehicleType(item.vehicleCode)\">\r\n              <!--   <p>&#2352; </p> -->\r\n              </button> \r\n              <p class=\"p-vehicle-type\">{{item.label}}</p>\r\n            </div> \r\n          </div>\r\n        </div>\r\n        <div class=\"vehicle-type-screens-div\">\r\n \r\n          <button class=\"left-arrow-button\" (click)=\"previousScreen();\">\r\n            <img src=\"../src/images/next-copy@2x.png\" alt=\"\" />\r\n          </button>\r\n          <span>{{vehicle_Cat_no}} / {{totalScreen}}</span>\r\n          <button class=\"right-arrow-button\" (click)=\"nextScreen();\" [disabled]=\"nextVehicleName\">\r\n            <img src=\"../src/images/next@2x.png\" alt=\"\" />\r\n          </button>\r\n        </div>\r\n        <div class=\"Vehicle-gate-open\">\r\n          <button type=\"button\" name=\"button\"  class=\"open_gate_button\">Open Gate</button>\r\n          <button type=\"button\" class=\"sync_button\">\r\n            <img src=\"../src/images/sync.png\" alt=\"\" />\r\n            <p>SYNC</p>\r\n          </button>\r\n        </div>\r\n        <!-- get journey type buttons -->\r\n        <div class=\"toll-payment_types\" *ngIf=\"cardInfo && cardInfo.passes[0]\" >\r\n          <button type=\"button\" name=\"button \" class=\"payment_type payment_type--one-time\"  *ngFor=\"let item of feesetup.journeyTypes\" (click)=\"passNavigate(item.label)\" [ngClass]= \"{'payment_type--active' : cardInfo.passes[0].passType == getPassType(item.code),'payment_type' : cardInfo.passes[0].passType != getPassType(item.code)}\">{{item.label}}</button>\r\n        </div>\r\n        <div class=\"toll-payment_types\" *ngIf=\"!cardInfo \" >\r\n          <button type=\"button\" name=\"button \" class=\"payment_type payment_type--one-time\"  *ngFor=\"let item of feesetup.journeyTypes\" (click)=\"passNavigate(item.label)\" >{{item.label}}</button>\r\n        </div>\r\n        <div class=\"toll-payment_types\" *ngIf=\"cardInfo && !cardInfo.passes[0] \" >\r\n          <button type=\"button\" name=\"button \" class=\"payment_type payment_type--one-time\"  *ngFor=\"let item of feesetup.journeyTypes\" (click)=\"passNavigate(item.label)\" [ngClass]= \"{'payment_type--active' : activityCodeApi == getPassType(item.code),'payment_type' :  activityCodeApi != getPassType(item.code)}\">{{item.label}}</button>\r\n        </div>\r\n        <!-- <div class=\"toll-payment_types\" *ngIf=\"cardInfo\" >\r\n          <button type=\"button\" name=\"button \" class=\"payment_type payment_type--one-time\"  *ngFor=\"let item of feesetup.journeyTypes\" (click)=\"passNavigate(item.label)\" [ngClass]= \"{'payment_type--active' : activityCodeApi == getPassType(item.code),'payment_type' : cardInfo.passes[0].passType != getPassType(item.code)}\" >{{item.label}}</button>\r\n        </div> -->\r\n        <!-- get journey type buttons ends-->\r\n        <div class=\"vehicle-usage\" *ngFor=\"let item of feesetup.vehiclecategories ;let i = index\">\r\n          <button type=\"button\" name=\"button\" *ngIf=\"item == vehiCategoryExist\" class=\"vehicle-use-active\">{{item}}</button>\r\n          <button type=\"button\" name=\"button\"  *ngIf=\"item != vehiCategoryExist\" class=\"vehicle-use\" (click)=\" vehiCategoryExist = item; getVehiclecategory(item);\" >{{item}}</button>\r\n        </div>\r\n      </div>\r\n      <div class=\"overview\">\r\n        <button type=\"button\" name=\"button\" ngDefaultControl class=\"overview__dashboard-meter\" id=\"datepicker\">\r\n          <img src=\"../src/images/dashboard-icon.png\" alt=\"\" />\r\n          <p>Dashboard</p>\r\n        </button>\r\n        <button type=\"button\" name=\"button\" ngDefaultControl class=\"overview__settlements\">\r\n          <img src=\"../src/images/system-view.png\" alt=\"\" />\r\n          <p>SYSTEM VIEW</p>\r\n        </button>\r\n        <button type=\"button\" name=\"button\" ngDefaultControl class=\"overview__settlements\" [routerLink]=\"['settlement']\">\r\n          <img src=\"../src/images/settlements-icon copy.png\" alt=\"\" />\r\n          <p>Settlements</p>\r\n        </button>\r\n        <button type=\"button\" name=\"button\" ngDefaultControl class=\"overview__settlements\" (click)=\"open(content)\">\r\n          <img src=\"../src/images/parking.png\" alt=\"\" />\r\n          <p>Wrong Vehicle</p>\r\n        </button>\r\n        <div class=\"overview-lost-card\">\r\n          <div class=\"lost-card__data\">\r\n            <!-- <p>Lost card code</p> -->\r\n            <input type=\"text\" name=\"name\" class=\"lost-card__code\" value=\"\" placeholder=\"Lost card code\" [(ngModel)]=\"lostCard_number\" [readonly]=\"generic.lostCardActive\" (click)=\"lostCardTextbox()\">\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"toll-gate-form \">\r\n      <div class=\"data-feild\">\r\n        <div class=\"rfid-number\">\r\n          <p>RFID</p>\r\n          <input type=\"text\" name=\"name\" class=\"rfid-number-value\" placeholder=\"*********\" [readonly]=\"generic.rfidActive\">\r\n        </div>\r\n        <div class=\"rfid-number\">\r\n          <p class=\"text-margin-bottom\">VEHICLE NO.</p>\r\n          <input type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"VEHICLE NO\" [(ngModel)]=\"holderInfo.vehicleNo\" [readonly]=\"generic.vehiclenoActive\" maxlength=\"10\" (keypress)=\"restrictSpecialcharachter\">\r\n        </div>\r\n      </div>\r\n      <div>\r\n        <img src=\"../src/images/vehicle-no@2x.png\" class=\"car-number\" alt=\"\" width=\"612px\" />\r\n      </div>\r\n      <div class=\"data-feild\">\r\n        <div class=\"rfid-number\">\r\n          <p class=\"text-margin-bottom\">CARD NO.</p>\r\n          <input type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"*********\" [(ngModel)]=\"holderInfo.cardNo\" [readonly]=\"generic.cardnoActive\" maxlength=\"16\" (keypress)=\"onlyNumberKey($event)\">\r\n        </div>\r\n        <div class=\"rfid-number\">\r\n          <p class=\"text-margin-bottom\">NAME</p>\r\n          <input type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"NAME\" [(ngModel)]=\"holderInfo.holderName\" [readonly]=\"generic.nameActive\" (keypress)=\"restrictNumeric($event)\">\r\n        </div>\r\n      </div>\r\n      <div class=\"data-feild\">\r\n        <div class=\"mobile-number\">\r\n          <p class=\"text-margin-bottom\">MOBILE NO.</p>\r\n          <input  type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"*** *** ****\" size=\"10\" maxlength=\"10\" [(ngModel)]=\"holderInfo.phoneNo\" [readonly]=\"generic.mobilenoActive\" (keypress)=\"onlyNumberKey($event)\" ngDefaultControl>\r\n        </div>\r\n        <div class=\"adhar-number\">\r\n          <p class=\"text-margin-bottom\">AADHAR NO.</p>\r\n\r\n          <input *ngIf=\"!editAadharFlag\" (click)=\" changeAadharMode(true);\" type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"**** **** ****\" [ngModel]=\"holderInfo.aadharNo | displayCardNumber\" maxlength=\"12\" [readonly]=\"generic.aadharnoActive\" (keypress)=\"onlyNumberKey($event)\" ngDefaultControl>\r\n\r\n          <input *ngIf=\"editAadharFlag\"  type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"**** **** ****\" [(ngModel)]=\"holderInfo.aadharNo\" maxlength=\"12\" [readonly]=\"generic.aadharnoActive\" (keypress)=\"onlyNumberKey($event)\" ngDefaultControl>\r\n        </div>\r\n        <div class=\"verify-button\">\r\n          <button (click)=\"otpFingerscan(otpFinger)\">VERIFY</button>\r\n        </div>\r\n      </div>\r\n      <div class=\"recharge-card-margin\">\r\n        <div class=\"recharge-balance-row\">\r\n          <div class=\"width-adjust\">\r\n            <button id=\"activate\" class=\"hide-outline \" (click)=\"recharge()\" [ngClass]=\"{'green-bckg': backgroundBtn.rechargebackground, 'blue-bckg':!backgroundBtn.rechargebackground}\" [disabled]=\"generic.rechargeDisable\">RECHARGE</button>\r\n            <button id=\"recharge\" class=\"hide-outline\" (click)=\"activate()\" [ngClass]=\"{'green-bckg': backgroundBtn.activatebackground, 'blue-bckg':!backgroundBtn.activatebackground}\" [disabled]=\"generic.activateDisable\">ACTIVATE</button>\r\n            <button id=\"recharge\" class=\"hide-outline\" (click)=\"update()\" [ngClass]=\"{'green-bckg': backgroundBtn.updatebackground, 'blue-bckg':!backgroundBtn.updatebackground}\" [disabled]=\"generic.updateDisable\">UPDATE</button>\r\n          </div>\r\n          <div class=\"width-fix\">\r\n            <div class=\"balance-card-amount\">\r\n              <p>Balance Amount</p>\r\n              <div class=\"amount-section\">\r\n                <p class=\"total-balance-p\" style=\"width:10%;\">&#2352;</p>\r\n                <div style=\"text-align: left;\">\r\n                  <input class=\"balance-input\" id=\"amount\" [(ngModel)]=\"holderInfo.currentAmount\" [ngClass]=\"amountStyle\" placeholder=\"10000\" maxlength=\"7\" readonly>\r\n                </div>\r\n                <!-- <p class=\"last-digit-p\" [ngClass]=\"lastDigitStyle\">00</p> -->\r\n              </div>\r\n            </div>\r\n            <div class=\"clear-all-button\">\r\n              <button class=\"\" (click)=\"clearAll()\" [disabled]=\"generic.clearallDisable\">CLEAR ALL</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"toll-rate-cards\">\r\n          <div class=\"rate-card--view\">\r\n            <button type=\"button\" name=\"button\" ngDefaultControl class=\"rate-card__done-button\" (click)=\"doneButtonClick(cardactionResponse)\"  [disabled]=\"generic.doneDisable\">DONE</button>\r\n            <div class=\"rate-card--recharge\">\r\n              <p>Recharge Amount</p>\r\n              <input type=\"text\" name=\"name\" id=\"rate-card--recharge__value\" class=\"rate-card--recharge__value\" maxlength=\"6\" value=\"\" placeholder=\"&#2352; 10000\" pattern=\"[0-9]\" [(ngModel)]=\"recharge_amount\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>\r\n            </div>\r\n            <div class=\"vehicles-rate-buttons\">\r\n              <div class=\"price-button-row\">\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('200',rate_button);\" value=\"200\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#2352; 200</button>\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('300',rate_button);\" value=\"300\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#2352; 300</button>\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('400',rate_button);\" value=\"400\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#2352; 400</button>\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('500',rate_button);\" value=\"500\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#2352; 500</button>\r\n              </div>\r\n              <div class=\"price-button-row\">\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('1500',rate_button);\" value=\"1500\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#2352; 1500</button>\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('2000',rate_button);\" value=\"2000\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#2352; 2000</button>\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('5000',rate_button);\" value=\"5000\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#2352; 5000</button>\r\n                <button type=\"button\" name=\"button\" id=\"rate-button1\" class=\"rate-button\" (click)=\"rateButtonClick('10000',rate_button);\" value=\"10000\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#2352; 10000</button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"rate-card rate-card--values\">\r\n            <button type=\"button\" name=\"button\" class=\"\r\n            number-button\" (click)=\"numberButtonClick('7',rate_button);\" value=\"7\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>7</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('8',rate_button);\" value=\"8\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>8</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('9',rate_button);\" value=\"9\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>9</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('4',rate_button);\" value=\"4\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>4</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('5',rate_button);\" value=\"5\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>5</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('6',rate_button);\" value=\"6\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>6</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('1',rate_button);\" value=\"1\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>1</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('2',rate_button);\" value=\"2\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>2</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('3',rate_button);\" value=\"3\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>3</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('0',rate_button);\" value=\"0\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>0</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('00',rate_button);\" value=\"00\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>00</button>\r\n            <button type=\"button\" name=\"button\" cursor [amount]=\"recharge_amount\" id=\"eraseNumber\" class=\"number-button\" [disabled]=\"generic.rechargeamountDisable\" [(ngModel)]=\"eraseButton\" ngDefaultControl><img src=\"../src/images/delete.png\" /></button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- Modal -->\r\n  <ng-template #content let-c=\"close\" let-d=\"dismiss\">\r\n    <div class=\"total-content\">\r\n      <div class=\"modal-body\">\r\n        <img class=\"wrong-vehicle-design\" src=\"../src/images/car.png\" alt=\"\" />\r\n      </div>\r\n      <div class=\"wrong-vehicle-text\">\r\n        ARE YOU SURE ?\r\n      </div>\r\n      <div class=\"button-yes-no\">\r\n        <button class=\"no-confirmation\" (click)=\"c('Close click')\">NO</button>\r\n        <button class=\"yes-confirmation\">YES</button>\r\n      </div>\r\n    </div>\r\n    \r\n  </ng-template>\r\n  <!-- Modal for otp verification and fingerprint verification -->\r\n  <ng-template #otpFinger let-c=\"close\" let-d=\"dismiss\">\r\n    <div class=\"total-content\">\r\n      <div class=\"close-modal\">\r\n        <button>\r\n          <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n        </button>\r\n      </div>\r\n      <div class=\"otp-finger-content\">\r\n        <div class=\"otp-verification\">\r\n          <div class=\"dont-have-text\">\r\n            <p>Have a Fingerprint Enabled Device?</p>\r\n          </div>\r\n          <div class=\"opt-verification-img\">\r\n            <button>OTP VERIFICATION</button>\r\n          </div>\r\n        </div>\r\n        <div class=\"border-div\">\r\n        </div>\r\n        <div class=\"finger-scanner\">\r\n          <div class=\"finger-on-text\">\r\n            <p>Place your Finger on the Device Now.</p>\r\n          </div>\r\n          <div class=\"fingerprint-img\">\r\n            <img src=\"../src/images/fingerprint.png\" alt=\"\" />\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"fingerprint-modal-footer\">\r\n        <div>\r\n          <img src=\"../src/images/alert.png\" alt=\"\" />\r\n        </div>\r\n        <div class=\"footer-text\">\r\n          <p>Loreum Ipsum is simply Dummytext</p>\r\n        </div>\r\n        <button class=\"skip-button\" (click)=\"otpFingerConfirmed(otpFingerconfirmed)\">SKIP</button>\r\n      </div>\r\n    </div>\r\n  </ng-template>\r\n  <!-- Modal for fingerprint verified-->\r\n  <ng-template #otpFingerconfirmed let-c=\"close\" let-d=\"dismiss\">\r\n    <div class=\"total-content\">\r\n      <div class=\"close-modal \">\r\n        <button>\r\n          <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n        </button>\r\n      </div>\r\n      <div class=\"otp-finger-content\">\r\n        <div class=\"otp-verification\">\r\n          <div class=\"dont-have-text\">\r\n            <p>Have a Fingerprint Enabled Device?</p>\r\n          </div>\r\n          <div class=\"opt-verification-img\">\r\n            <button>OTP VERIFICATION</button>\r\n          </div>\r\n        </div>\r\n        <div class=\"border-div\">\r\n        </div>\r\n        <div class=\"finger-scanner-verified\">\r\n          <div class=\"tick-img\">\r\n            <img src=\"../src/images/tick.png\">\r\n          </div>\r\n          <div class=\"fingerprint-recognized\">\r\n            <p>FINGERPRINT RECOGNIZED</p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"fingerprint-modal-footer\">\r\n        <div>\r\n          <img src=\"../src/images/alert.png\" alt=\"\" />\r\n        </div>\r\n        <div class=\"footer-text\">\r\n          <p>Loreum Ipsum is seems to</p>\r\n        </div>\r\n        <button class=\"skip-button\" (click)=\"otpFingerNotRecognized(otpFingernotRecognized)\">SKIP</button>\r\n      </div>\r\n    </div>\r\n  </ng-template>\r\n  <!-- Modal for fingerprint not verified-->\r\n  <ng-template #otpFingernotRecognized let-c=\"close\" let-d=\"dismiss\">\r\n    <div class=\"total-content\">\r\n      <div class=\"close-modal \">\r\n        <button>\r\n          <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n        </button>\r\n      </div>\r\n      <div class=\"otp-finger-content\">\r\n        <div class=\"otp-verification\">\r\n          <div class=\"dont-have-text\">\r\n            <p>Have a Fingerprint Enabled Device?</p>\r\n          </div>\r\n          <div class=\"opt-verification-img\">\r\n            <button>OTP VERIFICATION</button>\r\n          </div>\r\n        </div>\r\n        <div class=\"border-div\">\r\n        </div>\r\n        <div class=\"finger-scanner-notverified\">\r\n          <div class=\"notsuccess-img\">\r\n            <img src=\"../src/images/notsuccess.png\">\r\n          </div>\r\n          <div class=\"notrecognized-text\">\r\n            <p>Fingerprint not recognized.</p>\r\n          </div>\r\n          <div class=\"try-again\">\r\n            <button>TRY AGAIN</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"fingerprint-modal-footer\">\r\n        <div>\r\n          <img src=\"../src/images/alert.png\" alt=\"\" />\r\n        </div>\r\n        <div class=\"footer-text\">\r\n          <p>Loreum Ipsum is heems to</p>\r\n        </div>\r\n        <button class=\"skip-button\" (click)=\"otpConfirmedFinger(otpconfirmedFinger)\">SKIP</button>\r\n      </div>\r\n    </div>\r\n  </ng-template>\r\n  <!-- Modal for otp verified success-->\r\n  <ng-template #otpconfirmedFinger let-c=\"close\" let-d=\"dismiss\">\r\n    <div class=\"total-content\">\r\n      <div class=\"close-modal \">\r\n        <button>\r\n          <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n        </button>\r\n      </div>\r\n      <div class=\"otp-finger-content\">\r\n        <div class=\"finger-scanner-verified\">\r\n          <div class=\"tick-img\">\r\n            <img src=\"../src/images/tick.png\">\r\n          </div>\r\n          <div class=\"fingerprint-recognized\">\r\n            <p>OTP Verified</p>\r\n          </div>\r\n        </div>\r\n        <div class=\"border-div\">\r\n        </div>\r\n        <div class=\"finger-scanner-notverified\">\r\n          <div class=\"start-fingerprint-text\">\r\n            <p>Start Using Your Fingerprint</p>\r\n          </div>\r\n          <div class=\"show-fingerprint-img\">\r\n            <img src=\"../src/images/fingerprint.png\" alt=\"\" />\r\n          </div>\r\n          <div class=\"use-fingerprint\">\r\n            <button>USE FINGERPRINT</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"fingerprint-modal-footer\">\r\n        <div>\r\n          <img src=\"../src/images/alert.png\" alt=\"\" />\r\n        </div>\r\n        <div class=\"footer-text\">\r\n          <p>Loreum Ipsum is heems to</p>\r\n        </div>\r\n        <button class=\"skip-button\" (click)=\"optInputFinger(optinputFinger)\">SKIP</button>\r\n      </div>\r\n    </div>\r\n  </ng-template>\r\n  <!-- Modal to give otp code-->\r\n  <ng-template #optinputFinger let-c=\"close\" let-d=\"dismiss\">\r\n    <div class=\"total-content\">\r\n      <div class=\"close-modal \">\r\n        <button>\r\n          <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n        </button>\r\n      </div>\r\n      <div class=\"otp-finger-content\">\r\n        <div class=\"input-otp-verify\">\r\n          <div class=\"otp-text\">\r\n            <span>OTP</span>\r\n          </div>\r\n          <div class=\"otp-sent-text\">\r\n            <p>we have sent OTP to the Mobile number for which aadhar card sync</p>\r\n          </div>\r\n          <div class=\"enter-otp\">\r\n            <p>Enter OTP Here</p>\r\n          </div>\r\n          <div class=\"otp-feild\">\r\n            <input class=\"otp-value\" type=\"password\" name=\"\" maxlength=\"1\">\r\n            <input class=\"otp-value\" type=\"password\" name=\"\" maxlength=\"1\">\r\n            <input class=\"otp-value\" type=\"password\" name=\"\" maxlength=\"1\">\r\n            <input class=\"otp-value\" type=\"password\" name=\"\" maxlength=\"1\">\r\n            <input class=\"otp-value\" type=\"password\" name=\"\" maxlength=\"1\">\r\n            <input class=\"otp-value\" type=\"password\" name=\"\" maxlength=\"1\">\r\n          </div>\r\n          <div class=\"resend-text\">\r\n            <span>Resend</span>\r\n          </div>\r\n          <div class=\"otp-submit-button\">\r\n            <button>VERIFY</button>\r\n          </div>\r\n        </div>\r\n        <div class=\"border-div\">\r\n        </div>\r\n        <div class=\"finger-scanner-notverified\">\r\n          <div class=\"start-fingerprint-text\">\r\n            <p>Start Using Your Fingerprint</p>\r\n          </div>\r\n          <div class=\"show-fingerprint-img\">\r\n            <img src=\"../src/images/fingerprint.png\" alt=\"\" />\r\n          </div>\r\n          <div class=\"use-fingerprint\">\r\n            <button>USE FINGERPRINT</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"fingerprint-modal-footer\">\r\n        <div>\r\n          <img src=\"../src/images/alert.png\" alt=\"\" />\r\n        </div>\r\n        <div class=\"footer-text\">\r\n          <p>Loreum Ipsum is heems to</p>\r\n        </div>\r\n        <button class=\"skip-button\" (click)=\"otpOrFinger(otporFinger)\">SKIP</button>\r\n      </div>\r\n    </div>\r\n  </ng-template>\r\n  <!-- Modal to SELECT OTP OR FINGERPRINT-->\r\n  <ng-template #otporFinger let-c=\"close\" let-d=\"dismiss\">\r\n    <div class=\"total-content\">\r\n      <div class=\"close-modal \">\r\n        <button>\r\n          <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n        </button>\r\n      </div>\r\n      <div class=\"otp-finger-content\">\r\n        <div class=\"otp-verification\">\r\n          <div class=\"otp-text\">\r\n            <span>OTP</span>\r\n          </div>\r\n          <div class=\"otp-send-img\">\r\n            <img src=\"../src/images/send-otp.png\" alt=\"\" />\r\n          </div>\r\n          <div class=\"otp-submit-button\">\r\n            <button>SEND OTP</button>\r\n          </div>\r\n        </div>\r\n        <div class=\"border-div\">\r\n        </div>\r\n        <div class=\"finger-scanner\">\r\n          <div class=\"finger-on-text\">\r\n            <p>Start Using Your Fingerprint</p>\r\n          </div>\r\n          <div class=\"fingerprint-img\">\r\n            <img src=\"../src/images/fingerprint.png\" alt=\"\" />\r\n          </div>\r\n          <div class=\"use-fingerprint\">\r\n            <button>USE FINGERPRINT</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"fingerprint-modal-footer\">\r\n        <div>\r\n          <img src=\"../src/images/alert.png\" alt=\"\" />\r\n        </div>\r\n        <div class=\"footer-text\">\r\n          <p>Loreum Ipsum is simply Dummytext</p>\r\n        </div>\r\n        <button class=\"skip-button\">SKIP</button>\r\n      </div>\r\n    </div>\r\n  </ng-template>\r\n\r\n  \r\n\r\n <!-- Modal for card action popup-->\r\n <ng-template role=\"dialog\"   let-c=\"close\" let-d=\"dismiss\" #cardactionResponse >\r\n     <div class=\"modal-content-card\">\r\n        <div class=\"close-modal \">\r\n          <button>\r\n            <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n          </button>\r\n        </div>\r\n        <div class=\"message-button\">\r\n          <span>Successful</span>\r\n          <button  (click)=\"c('Close click')\" autofocus>Done</button>\r\n        </div>\r\n     </div>\r\n  </ng-template>\r\n\r\n  <!-- Modal for card action popup ends-->"

/***/ })

});
//# sourceMappingURL=common.chunk.js.map