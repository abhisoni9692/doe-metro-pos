webpackJsonp(["dashboard.module"],{

/***/ "./src/app/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_dal_service__ = __webpack_require__("./src/app/services/dal.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_base64_service__ = __webpack_require__("./src/app/services/base64.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common_popup_popup_component__ = __webpack_require__("./src/app/common/popup/popup.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DashboardComponent = (function () {
    function DashboardComponent(commonserve, ref, base64, router, dal) {
        this.commonserve = commonserve;
        this.ref = ref;
        this.base64 = base64;
        this.router = router;
        this.dal = dal;
        this.hide_First_Screen = false;
        this.hide_Second_Screen = true;
        this.vehicle_Cat_no = 1;
        this.tempCardInfo = {
            aadhaar: "",
            balance: "",
            cardNumber: "",
            name: "",
            mobile: "",
            vehicleNumber: "",
            vehicleType: "",
            passes: []
        };
        this.vehicl = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].vehicleType;
        this.vehicleName = [];
        this.vehicleNameSecond = [];
        this.vehicleNameFirst = [];
        //call api for business id
        this.getOperatorDetails();
        //buaroh vaiable
        this.payParams = {
            "serviceType": "TOLL",
            "action": "PAY",
            "posTerminalId": "123456789",
            "amount": 0,
            "sourceBranchId": "123456789",
            "destinationBranchId": "12",
            "timeInMilliSeconds": '',
            "readerType": __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable.readerType,
            "readerIndex": 0
        };
        this.ownername = "";
        this.holderInfo = {
            "vehicleNo": "",
            "cardNo": "",
            "holderName": "",
            "phoneNo": "",
            "aadharNo": "",
            "rfidNo": "",
            "currentAmount": "",
        };
        this.generic = {
            "vehiclenoActive": true,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "rechargeDisable": true,
            "activateDisable": true,
            "updateDisable": true,
            "lostCardActive": false,
            "clearallDisable": true,
            "rechargeInitited": false,
            "updateInitited": false,
            "activateInitited": false,
            "rechargeamountDisable": true,
            "activeVehicleClass": true,
        };
        this.recharge_amount = "";
        this.lostCard_number = "";
        this.backgroundBtn = {
            "activatebackground": false,
            "rechargebackground": false,
            "updatebackground": false,
        };
        this.nextVehicleName = true;
        //use pass variable
        //stop all readers and define defaults and then intialize all
        this.stopallReadersAndDefineDefaults();
        //get fee structure
        this.error = {};
        this.feesetup = {
            "tollInfo": {},
            "structure": {},
        };
        this.feesetup.vehicleTypes = [];
        this.feesetup.journeyTypes = [];
        this.feesetup.passType = [];
        this.feesetup.structure = {};
        this.feesetup.startediting = false;
        this.feesetup.feeCodes = [];
        this.feesetup.feeStructure = {};
        this.feesetup.vehiclecategories = [];
        this.feesetup.tollInfo = {};
        this.feesetup.vehicleTypes = {
            "label": "",
            "class": ""
        };
        //activate card variable initiate
        this.vehicleItem = "";
        this.vehiCategoryExist = "";
        this.totalScreen = 1;
        this.activityCodeApi = "";
        this.selectVehicle = {
            "vehicleType": {
                "label": "",
                "numericCode": [],
                "class": ""
            },
            "vehicleCat": {
                "category": "",
                "numericCode": []
            },
            "numericCode": 0
        };
        this.successFlag = 0;
    }
    DashboardComponent.prototype.stopallReadersAndDefineDefaults = function () {
        var _this = this;
        this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resultsdr) {
            console.log("sdr10 stopDetection in beginning", resultsdr);
            _this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable, function (resultbuaroh) {
                _this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable2, function (resultbuaroh2) {
                    _this.definecardDefaults();
                    console.log(" buaroh2 stopDetection in beginning", resultbuaroh2);
                });
                console.log(" buaroh stopDetection in beginning", resultbuaroh);
            });
        });
    };
    //get vehicle category code and type
    DashboardComponent.prototype.getVehicleType = function (item) {
        this.selectVehicle.vehicleType = item;
        console.log("type is", item);
        this.ref.detectChanges();
    };
    DashboardComponent.prototype.getVehiclecategory = function (item) {
        this.selectVehicle.vehicleCat = item;
        console.log("cat is", item);
        this.ref.detectChanges();
    };
    //get vehicle category code and type ends
    DashboardComponent.prototype.getCategorycodewithDash = function (item) {
        console.log("item is.....", item);
        var temp = item.replace("_", "-");
        return temp;
    };
    //operator details test
    DashboardComponent.prototype.getOperatorDetails = function () {
        var auth = this.commonserve.getStoreData();
        console.log("token is", JSON.stringify(auth));
        var str = auth.accesstoken.split(".");
        var decodestr = str[1];
        var userinfo = this.base64.decode(decodestr);
        console.log("userinfo.....", userinfo);
        var s = userinfo.replace(/\\n/g, "\\n")
            .replace(/\\'/g, "\\'")
            .replace(/\\"/g, '\\"')
            .replace(/\\&/g, "\\&")
            .replace(/\\r/g, "\\r")
            .replace(/\\t/g, "\\t")
            .replace(/\\b/g, "\\b")
            .replace(/\\f/g, "\\f");
        s = s.replace(/[\u0000-\u0019]+/g, "");
        var obj = JSON.parse(s);
        this.userid = Object.keys(obj.businessProfile);
        __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].businessLevelId = obj;
        this.getOperatorInfo(obj.sub);
        this.gettoll(this.userid[0]);
    };
    ;
    //get company details
    DashboardComponent.prototype.getTransactionDetails = function (parentid) {
        var _this = this;
        if (window.navigator.onLine) {
            this.commonserve.getCompanyDetails(parentid['businessProfile']['businessLevel']['parentId']).subscribe(function (response) {
                console.log("company  info", response);
                __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].companyInfo = response;
            }, function (err) {
                _this.finePopup.message = err;
                _this.finePopup.showPopup('WARNINGPOPUP');
                console.log("error in login ", err);
            });
        }
        else {
            this.finePopup.message = "Check Your Internet Connection";
            this.finePopup.showPopup('WARNINGPOPUP');
        }
    };
    //restrict character or number function ends
    DashboardComponent.prototype.onlyNumberKey = function (event) {
        this.editAadharFlag = true;
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    };
    DashboardComponent.prototype.restrictNumeric = function (e) {
        return (e.charCode == 8 || e.charCode == 0 || e.charCode == 32) ? null : e.charCode >= 97 && e.charCode <= 122 || e.charCode >= 65 && e.charCode <= 90;
    };
    DashboardComponent.prototype.restrictSpecialcharachter = function (e) {
        return (e.charCode == 8 || e.charCode == 0) ? null : e.charCode >= 97 && e.charCode <= 122 || e.charCode >= 65 && e.charCode <= 90 || e.charCode >= 48 && e.charCode <= 57;
    };
    //restrict character or number function ends
    DashboardComponent.prototype.enterFromKeyboard = function (e, popup) {
        if (e.charCode == 13) {
            this.doneButtonClick(popup);
            this.validClick = 1;
        }
    };
    DashboardComponent.prototype.numberButtonClick = function (rate, type) {
        if (this.validClick !== 1) {
            if (this.recharge_amount.toString().length < 6) {
                var temp = this.recharge_amount + rate;
                this.recharge_amount = parseInt(temp);
                this.digitBackgroundVar = true;
            }
        }
        this.validClick = 0;
    };
    ;
    DashboardComponent.prototype.rateButtonClick = function (rate, type) {
        this.recharge_amount = rate;
    };
    ;
    DashboardComponent.prototype.previousScreen = function () {
        this.hide_Second_Screen = true;
        this.hide_First_Screen = false;
        this.vehicle_Cat_no = 1;
        this.totalScreen = 1;
    };
    DashboardComponent.prototype.nextScreen = function () {
        this.hide_Second_Screen = false;
        this.hide_First_Screen = true;
        this.vehicle_Cat_no = 2;
        this.totalScreen = 2;
    };
    //seperate vehicle type from vehicle code
    DashboardComponent.prototype.returnVehicleCode = function (split) {
        if (!split) {
            //alert("reader stopped working")
            this.finePopup.message = "reader stopped working";
            this.finePopup.showPopup('WARNINGPOPUP');
        }
        else {
            var VehicleCode = [];
            var vehLabel = void 0;
            VehicleCode = split.split('_');
            console.log("type of", this.feesetup.vehicleTypes.length);
            for (var i = 0; i < this.feesetup.vehicleTypes.length; i++) {
                if (this.feesetup.vehicleTypes[i].code == split) {
                    console.log("array cat code in loop", this.feesetup.vehicleTypes[i].vehicleCode);
                    return this.feesetup.vehicleTypes[i].vehicleCode;
                }
            }
        }
    };
    //seperate vehicle type from vehicle code
    DashboardComponent.prototype.returnVehicleCategorycode = function (split) {
        var VehicleCode = [];
        VehicleCode = split.split('_');
        var tempVehicleCode = [];
        if (VehicleCode.length > 2) {
            for (var i = 0; i <= 1; i++) {
                tempVehicleCode.push(VehicleCode[i]);
                console.log("came code", tempVehicleCode);
            }
            var vehicleCodeTemp;
            vehicleCodeTemp = tempVehicleCode.join('-');
            console.log("came now.....", vehicleCodeTemp);
            return vehicleCodeTemp;
        }
        else {
            for (var i = 0; i < 1; i++) {
                tempVehicleCode.push(VehicleCode[i]);
                console.log("came code", tempVehicleCode);
            }
            return tempVehicleCode;
        }
    };
    //changeaadhar mode
    DashboardComponent.prototype.changeAadharMode = function (flag) {
        this.editAadharFlag = flag;
        this.ref.detectChanges();
    };
    //navigate to pass type for activation
    DashboardComponent.prototype.passNavigate = function (passtype) {
        if (this.cardInfo) {
            switch (passtype) {
                case "Single Journey":
                    // this.router.navigate(['/dashboard/passes/singlepass']);.
                    break;
                case "COMMERCIAL_VEHICLE_WITHIN_DISTRICT":
                    if (this.cardInfo.passes[0]) {
                        this.finePopup.message = "Same Pass Already Exist";
                        this.finePopup.showPopup('WARNINGPOPUP');
                    }
                    else
                        this.router.navigate(['/dashboard/passes/localpass']);
                    break;
                case "Return Journey":
                    // this.router.navigate(['/dashboard/passes/returnpass']);   
                    break;
                case "MONTHLY_PASS":
                    var passPresent = 0;
                    console.log("environment.feeStructureLimits", __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].feeStructureLimits);
                    for (var i = 0; i < __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].feeStructureLimits['baseFees'].length; i++) {
                        if ((__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].vehicleTypeFromCard == __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].feeStructureLimits['baseFees'][i].categoryCode) && __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].feeStructureLimits['baseFees'][i].activityCode == "MONTHLY_PASS") {
                            passPresent = 1;
                        }
                    }
                    if (passPresent == 1) {
                        if (!this.cardInfo.passes[0]) {
                            this.router.navigate(['/dashboard/passes/monthlypass']);
                            break;
                        }
                        else {
                            this.finePopup.message = "Pass already exist for this Toll";
                            this.finePopup.showPopup('WARNINGPOPUP');
                        }
                    }
                    else {
                        this.finePopup.message = "Passes for this Vehicle Type not available";
                        this.finePopup.showPopup('WARNINGPOPUP');
                    }
                    break;
            }
        }
        else {
            this.finePopup.message = "Please tap the card";
            this.finePopup.showPopup('WARNINGPOPUP');
        }
    };
    //clear all button action
    DashboardComponent.prototype.clearAll = function () {
        this.holderInfo = {
            "vehicleNo": "",
            "cardNo": "",
            "holderName": "",
            "phoneNo": "",
            "aadharNo": "",
            "rfidNo": "",
            "currentAmount": "",
        };
        this.cardVehicleType = "";
        this.vehiCategoryExist = "";
        this.lostCard_number = "";
        this.ref.detectChanges();
        this.getGenericDataFromCard();
    };
    //Successfully done popup
    DashboardComponent.prototype.donePopup = function (message) {
        var _this = this;
        this.finePopup.showPopup(message);
        setTimeout(function () {
            _this.finePopup.closePopup();
        }, 1000);
    };
    //successfully done popup ends
    //card function starts to call defualt card display
    DashboardComponent.prototype.definecardDefaults = function () {
        var currentUrl = this.router.url;
        if (currentUrl == '/dashboard') {
            this.payParams.amount = "";
            this.cardInfo = "";
            this.holderInfo = {
                "vehicleNo": "",
                "cardNo": "",
                "holderName": "",
                "phoneNo": "",
                "aadharNo": "",
                "rfidNo": "",
                "currentAmount": "",
            };
            this.lostCard_number = "";
            this.generic = {
                "rechargeInitited": false,
                "updateInitited": false,
                "activateInitited": false,
                "rechargeDisable": true,
                "activateDisable": true,
                "updateDisable": true,
                "rechargeamountDisable": true,
                "doneDisable": true,
                "vehiclenoActive": true,
                "nameActive": true,
                "mobilenoActive": true,
                "cardnoActive": true,
                "aadharnoActive": true,
                "rfidActive": true,
                "lostCardActive": false,
                "clearallDisable": true,
            };
            this.recharge_amount = "";
            this.backgroundBtn = {
                "activatebackground": false,
                "rechargebackground": false,
                "updatebackground": false,
            };
            this.cardVehicleType = "";
            this.vehiCategoryExist = "";
            this.activityCodeApi = "";
            this.selectVehicle = {
                "vehicleType": {
                    "label": "",
                    "numericCode": [],
                    "class": ""
                },
                "vehicleCat": {
                    "category": "",
                    "numericCode": []
                },
                "numericCode": 0
            };
            for (var i = 0; i < this.feesetup.journeyTypes.length; i++) {
                this.feesetup.journeyTypes[i].selected = false;
            }
            this.successFlag = 0;
            this.ref.detectChanges();
            this.getGenericDataFromCard();
        }
    };
    //get pass type
    DashboardComponent.prototype.getPassType = function (type) {
        switch (type) {
            case "MONTHLY_PASS":
                return "MONTHLY";
            case "COMMERCIAL_VEHICLE_WITHIN_DISTRICT":
                return "LOCAL";
            case "RETURN_JOURNEY":
                return "RETURN_JOURNEY";
            case "SINGLE_JOURNEY":
                return "SINGLE_JOURNEY";
        }
    };
    //sort array for same vehicle cat label ends
    //apply class in journey Type
    DashboardComponent.prototype.getJourneyTypeClass = function () {
        if (this.cardInfo && this.cardInfo.passes[0]) {
            for (var i = 0; i < this.feesetup.journeyTypes.length; i++) {
                if (this.cardInfo.passes[0].passType == this.getPassType(this.feesetup.journeyTypes[i].code)) {
                    this.feesetup.journeyTypes[i].selected = true;
                }
            }
        }
        else if (this.cardInfo && !this.cardInfo.passes[0]) {
            for (var i = 0; i < this.feesetup.journeyTypes.length; i++) {
                if (this.activityCodeApi == this.getPassType(this.feesetup.journeyTypes[i].code)) {
                    this.feesetup.journeyTypes[i].selected = true;
                }
            }
        }
    };
    //get vehiclwe type and vehicle code from card
    DashboardComponent.prototype.getVehicleDetailFromCard = function () {
        var vehicleCode = this.cardInfo.vehicleType;
        for (var i = 0; i < this.feesetup.vehicleTypes.length; i++) {
            var vehicleCodeFromDB = void 0;
            if (this.feesetup.vehicleTypes[i].code[1] == '_') {
                vehicleCodeFromDB = this.feesetup.vehicleTypes[i].code.substr(2);
            }
            else {
                vehicleCodeFromDB = this.feesetup.vehicleTypes[i].code;
            }
            if (vehicleCode == vehicleCodeFromDB) {
                var response = {
                    "label": this.feesetup.vehicleTypes[i].label,
                    "numericCode": this.feesetup.vehicleTypes[i].info.numericCode,
                    "category": this.feesetup.vehicleTypes[i].vehicleCategoryType
                };
                console.log("response1", response);
                // return response;
                var objVehicle = {
                    "label": response.label,
                    "numericCode": [response.numericCode],
                    "class": ""
                };
                var objCat = {
                    "category": response.category,
                    "numericCode": [response.category]
                };
                this.getVehicleType(objVehicle);
                this.getVehiclecategory(objCat);
            }
        }
    };
    //get vehicle type and code from card ends
    //display generic data function
    DashboardComponent.prototype.genericAction = function () {
        this.editAadharFlag = false;
        this.holderInfo = {
            "vehicleNo": this.cardInfo.vehicleNumber,
            "cardNo": this.cardInfo.cardNumber,
            "holderName": this.cardInfo.name,
            "phoneNo": this.cardInfo.mobile,
            "aadharNo": this.cardInfo.aadhaar,
            "rfidNo": "",
            "currentAmount": this.cardInfo.balance,
        };
        __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].vehicleTypeFromCard = this.cardInfo.vehicleType;
        this.getVehicleDetailFromCard();
        // this.cardVehicleType = this.returnVehicleCode(this.cardInfo.vehicleType);
        // this.vehiCategoryExist = this.returnVehicleCategorycode(this.cardInfo.vehicleType)
        this.backgroundBtn = {
            "activatebackground": false,
            "rechargebackground": false,
            "updatebackground": false,
        };
        this.recharge_amount = "";
        this.generic = {
            "vehiclenoActive": true,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "rechargeDisable": false,
            "activateDisable": true,
            "updateDisable": false,
            "lostCardActive": true,
            "clearallDisable": true,
            "rechargeInitited": false,
            "updateInitited": false,
            "activateInitited": false,
            "rechargeamountDisable": true,
            "doneDisable": true,
            "activeVehicleClass": true,
        };
        this.getJourneyTypeClass();
        if (this.readerMode == "Outer_Reader") {
            this.generic.rechargeDisable = true;
            this.generic.updateDisable = true;
        }
        __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].cardInfo = this.cardInfo;
        this.ref.detectChanges();
    };
    //is card removed dal function for sdr10
    DashboardComponent.prototype.sdrCardRemoved = function () {
        var _this = this;
        this.dal.isCardRemoved(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resp) {
            console.log("sdr card removd..", resp);
            if (resp.cardRemoved == true) {
                __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].cardInfo = {
                    passes: []
                };
                _this.definecardDefaults();
            }
        });
    };
    //is card removed dal function for buaroh
    DashboardComponent.prototype.buarohCardRemoved = function () {
        var _this = this;
        this.dal.isCardRemoved(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable, function (resp) {
            if (resp.cardRemoved == true) {
                _this.definecardDefaults();
            }
        });
    };
    //is card removed dal function for buaroh2
    DashboardComponent.prototype.buarohCardRemoved2 = function () {
        var _this = this;
        this.dal.isCardRemoved(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable2, function (resp) {
            if (resp.cardRemoved == true) {
                _this.definecardDefaults();
            }
        });
        console.log("code for 2nd bauroh reader");
    };
    //test to get same vehicle type and category ina array
    DashboardComponent.prototype.getVehicleTypeNumCode = function () {
        this.collectionVehicleTypeNumCode = [];
        var j = 0;
        var i = 0;
        var vehicleClas = ["vehicle-buttons--bus", "vehicle-buttons--car", "vehicle-buttons--small_truck", "vehicle-buttons--container", "vehicle-buttons--bigger_truck", "vehicle-buttons--Road_lorry", "vehicle-buttons--lorry", "vehicle-buttons--truck"];
        for (var j_1 = vehicleClas.length - 1; j_1 < this.feesetup.vehicleTypes.length; j_1++) {
            vehicleClas = vehicleClas.concat(vehicleClas);
        }
        do {
            if (i == 0) {
                this.collectionVehicleTypeNumCode.push({
                    "label": this.feesetup.vehicleTypes[i].label,
                    "numericCode": [this.feesetup.vehicleTypes[i].info.numericCode],
                    "class": vehicleClas
                });
            }
            else {
                var checklableResult = {
                    "available": false,
                    "index": -1
                };
                checklableResult = this.checkLabelinCollectionVehicleTypeArray(this.feesetup.vehicleTypes[i].label);
                if (checklableResult.available) {
                    this.collectionVehicleTypeNumCode[checklableResult.index].numericCode.push(this.feesetup.vehicleTypes[i].info.numericCode);
                }
                else {
                    this.collectionVehicleTypeNumCode.push({
                        "label": this.feesetup.vehicleTypes[i].label,
                        "numericCode": [this.feesetup.vehicleTypes[i].info.numericCode],
                        "class": vehicleClas
                    });
                }
            }
            i++;
        } while (i < this.feesetup.vehicleTypes.length);
        console.log("label numbric code in array", this.collectionVehicleTypeNumCode);
    };
    DashboardComponent.prototype.checkLabelinCollectionVehicleTypeArray = function (lbl) {
        for (var i = 0; i < this.collectionVehicleTypeNumCode.length; i++) {
            if (this.collectionVehicleTypeNumCode[i].label == lbl)
                return {
                    "available": true,
                    "index": i
                };
        }
        return {
            "available": false,
            "index": -1
        };
        ;
    };
    //test to get same vehicle type and category ina array ends   
    //get vehicle cat from fee structrue
    DashboardComponent.prototype.getVehicleCatNumCode = function () {
        this.collectionVehicleCatNumCode = [];
        var i = 0;
        do {
            if (i == 0) {
                this.collectionVehicleCatNumCode.push({
                    "category": this.feesetup.vehicleTypes[i].vehicleCategoryType,
                    "numericCode": [this.feesetup.vehicleTypes[i].info.numericCode],
                });
            }
            else {
                var checkCategoryResult = {
                    "available": false,
                    "index": -1
                };
                checkCategoryResult = this.checkCategoryinCollectionVehicleTypeArray(this.feesetup.vehicleTypes[i].vehicleCategoryType);
                if (checkCategoryResult.available) {
                    this.collectionVehicleCatNumCode[checkCategoryResult.index].numericCode.push(this.feesetup.vehicleTypes[i].info.numericCode);
                }
                else {
                    this.collectionVehicleCatNumCode.push({
                        "category": this.feesetup.vehicleTypes[i].vehicleCategoryType,
                        "numericCode": [this.feesetup.vehicleTypes[i].info.numericCode],
                    });
                }
            }
            i++;
        } while (i < this.feesetup.vehicleTypes.length);
        console.log("caregory numbric code in array", this.collectionVehicleCatNumCode);
    };
    DashboardComponent.prototype.checkCategoryinCollectionVehicleTypeArray = function (lbl) {
        for (var i = 0; i < this.collectionVehicleCatNumCode.length; i++) {
            if (this.collectionVehicleCatNumCode[i].category == lbl)
                return {
                    "available": true,
                    "index": i
                };
        }
        return {
            "available": false,
            "index": -1
        };
        ;
    };
    //get vehicle cat from fee structure ends
    //to get numeric code on vehicle type click
    DashboardComponent.prototype.getSeletcedNumericCode = function (vehicleType, vehicleCategory) {
        for (var i = 0; i < vehicleType.length; i++) {
            for (var j = 0; j < vehicleCategory.length; j++)
                if (vehicleType[i] == vehicleCategory[j]) {
                    // code...
                    return vehicleType[i];
                }
            return null;
        }
    };
    //to get numeric code on vehicle type click ends
    //recharge button action
    DashboardComponent.prototype.recharge = function () {
        if (this.cardInfo.readerType == __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable.readerType) {
            this.backgroundBtn = {
                "rechargebackground": true,
            };
            this.generic = {
                "rechargeDisable": false,
                "activateDisable": true,
                "updateDisable": true,
                "rechargeInitited": true,
                "lostCardActive": true,
                "rechargeamountDisable": false,
                "vehiclenoActive": true,
                "nameActive": true,
                "mobilenoActive": true,
                "cardnoActive": true,
                "aadharnoActive": true,
                "rfidActive": true,
                "clearallDisable": true,
            };
            this.ref.detectChanges();
        }
    };
    //update button
    DashboardComponent.prototype.update = function () {
        //  this.editAadharFlag = false;
        this.backgroundBtn = {
            "updatebackground": true
        };
        this.generic = {
            "vehiclenoActive": false,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "rechargeDisable": true,
            "activateDisable": true,
            "updateDisable": false,
            "lostCardActive": true,
            "updateInitited": true,
            "rechargeamountDisable": true,
            "clearallDisable": true,
            "doneDisable": false,
        };
        this.ref.detectChanges();
    };
    //activate button
    DashboardComponent.prototype.activate = function () {
        this.backgroundBtn = {
            "activatebackground": true,
        };
        this.generic = {
            "activateDisable": false,
            "rechargeDisable": true,
            "updateDisable": true,
            "rechargeamountDisable": true,
            "lostCardActive": true,
            "activateInitited": true,
            "doneDisable": false,
        };
        this.ref.detectChanges();
    };
    //check before activation
    DashboardComponent.prototype.checkBeforeActiveCard = function () {
        if (this.holderInfo.holderName && this.holderInfo.phoneNo && this.holderInfo.vehicleNo && this.holderInfo.cardNo && this.holderInfo.aadharNo && this.selectVehicle.vehicleType.label && this.selectVehicle.vehicleCat.category)
            return true;
    };
    //after recharge action
    DashboardComponent.prototype.afterRecharge = function () {
        this.tempAction = true;
        this.backgroundBtn = {
            "rechargebackground": false,
        };
        this.generic = {
            "rechargeDisable": false,
            "activateDisable": true,
            "updateDisable": false,
            "rechargeamountDisable": true,
            "lostCardActive": false,
            "updateInitited": false,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "vehiclenoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "clearallDisable": true,
            "doneDisable": true,
        };
        this.recharge_amount = '';
        this.donePopup('DONE');
        // this.cardActionResponse(this.popup);
        //this.ref.detectChanges();
    };
    //after update action
    DashboardComponent.prototype.afterUpdate = function () {
        this.backgroundBtn = {
            "updatebackground": false
        };
        this.generic = {
            "rechargeDisable": false,
            "activateDisable": true,
            "updateDisable": false,
            "updateInitited": false,
            "rechargeamountDisable": true,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "vehiclenoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "clearallDisable": true,
            "doneDisable": true,
        };
        //this.editAadharFlag = false;
        this.lostCard_number = "";
        //this.cardActionResponse(this.popup);
        // this.ref.detectChanges();
        this.donePopup('DONE');
    };
    //after activate action
    DashboardComponent.prototype.afterActivate = function () {
        this.backgroundBtn = {
            "activatebackground": false,
        };
        this.generic = {
            "rechargeDisable": false,
            "activateDisable": true,
            "updateDisable": false,
            "activateInitited": false,
            "rechargeamountDisable": true,
            "doneDisable": true,
            "vehiclenoActive": true,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "lostCardActive": false,
            "clearallDisable": true,
        };
        this.lostCard_number = "";
        //  this.cardActionResponse(this.popup);
        this.donePopup('DONE');
        //this.ref.detectChanges();
    };
    //first tym activation action
    DashboardComponent.prototype.activateCardAction = function () {
        this.generic = {
            "rechargeDisable": true,
            "activateDisable": false,
            "updateDisable": true,
        };
        this.finePopup.message = "PLEASE ACTIVATE THE CARD";
        this.finePopup.showPopup('WARNINGPOPUP');
        this.ref.detectChanges();
    };
    //update action
    DashboardComponent.prototype.updateAction = function () {
        var _this = this;
        this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resultForUpdate) {
            var updateParams = {
                "firstName": _this.holderInfo.holderName,
                "name": _this.holderInfo.holderName,
                "middleName": _this.holderInfo.holderName,
                "lastName": _this.holderInfo.holderName,
                "gender": "MALE",
                "dateOfBirth": 1234567,
                "vehicleNumber": _this.holderInfo.vehicleNo,
                "mobile": parseInt(_this.holderInfo.phoneNo),
                "aadhaar": parseInt(_this.holderInfo.aadharNo),
                "timeInMilliSeconds": _this.commonserve.getEpocTimestamp(),
                "readerType": __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable.readerType,
                "readerIndex": 0
            };
            var updateApiParams = {
                "cardNumber": _this.holderInfo.cardNo,
                "vehicleNumber": _this.holderInfo.vehicleNo,
                "vehicleType": _this.cardInfo.vehicleType
            };
            _this.dal.updatePersonalData(updateParams, function (resp) {
                console.log("update", resp);
                // this.doeStoreUpdateapi();
                _this.afterUpdate();
                //api cal for update card info
                _this.commonserve.updateCardApi(updateApiParams).subscribe(function (respActivate) {
                    console.log("update done Api sucess....................", respActivate);
                }, function (err) {
                    alert(err.error.message);
                    console.log("error in update ", err);
                });
                _this.ref.detectChanges();
                _this.sdrCardRemoved();
            });
        });
    };
    //recharge action
    DashboardComponent.prototype.rechargeAction = function () {
        var _this = this;
        this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resultsdr) {
            console.log("stop det in done......", resultsdr);
            if (_this.recharge_amount) {
                var rechargeParams_1 = {
                    "serviceType": "TOLL",
                    "action": "PAY",
                    "posTerminalId": "1234",
                    "amount": parseInt(_this.recharge_amount),
                    "sourceBranchId": "231",
                    "destinationBranchId": "12",
                    "timeInMilliSeconds": _this.commonserve.getEpocTimestamp(),
                    "readerType": __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable.readerType,
                    "readerIndex": 0
                };
                //rechargeAPI params
                var rechargeParamsApi = {
                    "mode": "TOLL",
                    "amount": {
                        "value": parseInt(_this.recharge_amount),
                    },
                    "action": "CARD_TOP_UP",
                    "readerId": __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].readerId,
                    "cardNumber": _this.holderInfo.cardNo,
                    "activityTime": _this.commonserve.getIsoStringTime(),
                };
                _this.commonserve.getRechargeCheck(rechargeParamsApi).subscribe(function (response) {
                    console.log("response of rech limitssss", response);
                    _this.dal.recharge(rechargeParams_1, function (resRecharge) {
                        console.log(":recharge dal params", rechargeParams_1);
                        console.log(":recharge from reader", resRecharge);
                        // this.checkRechargeparmas.amount.value = parseInt(this.recharge_amount);
                        //rechrage api
                        _this.commonserve.recharge(rechargeParamsApi).subscribe(function (response) {
                            //  console.log("recharge params api...",this.checkRechargeparmas)
                            console.log("recharge done res", response);
                            var dt = new Date();
                            var date1 = dt.toISOString(); // current date nd time
                            _this.holderInfo.currentAmount = resRecharge.amount;
                            _this.afterRecharge();
                            _this.ref.detectChanges();
                            _this.sdrCardRemoved();
                        }, function (err) {
                            console.log("error in recharge ", err);
                            alert(err.error.message);
                            _this.sdrCardRemoved();
                        });
                        //recharge api end
                        // this.sdrCardRemoved();
                    });
                }, function (err) {
                    console.log("u have crossed 20000 limit", err.error.message);
                    alert(err.error.message);
                    _this.sdrCardRemoved();
                });
                //end
            }
            else {
                _this.finePopup.message = "Please enter the amount";
                _this.finePopup.showPopup('WARNINGPOPUP');
                _this.sdrCardRemoved();
            }
        });
    };
    //get same numeric code in vehicle type and vehicle category
    DashboardComponent.prototype.getVehicleCode = function (numericCode) {
        var vehicleCodeFromDB;
        for (var i = 0; i < this.feesetup.vehicleTypes.length; i++) {
            if (this.feesetup.vehicleTypes[i].info.numericCode == numericCode) {
                if (this.feesetup.vehicleTypes[i].code[1] == '_') {
                    vehicleCodeFromDB = this.feesetup.vehicleTypes[i].code.substr(2);
                    return vehicleCodeFromDB;
                }
                else {
                    return this.feesetup.vehicleTypes[i].code;
                }
            }
        }
    };
    //get same numeric code in vehicle type and vehicle category ends 
    //activate action
    DashboardComponent.prototype.activateAction = function () {
        var _this = this;
        this.selectVehicle.numericCode = this.getSeletcedNumericCode(this.selectVehicle.vehicleType.numericCode, this.selectVehicle.vehicleCat.numericCode);
        console.log("selected nummmmmmmm", this.selectVehicle.numericCode);
        this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resultForRecharge) {
            //params for dal activate card
            if (_this.checkBeforeActiveCard() == true) {
                var activateparams = {
                    "serviceType": "TOLL",
                    "action": "PAY",
                    "posTerminalId": "1234",
                    "amount": 100,
                    "sourceBranchId": "231",
                    "destinationBranchId": "12",
                    "timeInMilliSeconds": _this.commonserve.getEpocTimestamp(),
                    "readerType": __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable.readerType,
                    "readerIndex": 0,
                    "cardNumber": _this.holderInfo.cardNo,
                    "name": _this.holderInfo.holderName,
                    "mobile": parseInt(_this.holderInfo.phoneNo),
                    "aadhaar": parseInt(_this.holderInfo.aadharNo),
                    "vehicleType": _this.getVehicleCode(_this.selectVehicle.numericCode),
                    "vehicleNumber": _this.holderInfo.vehicleNo,
                    "cardStatus": "ACTIVATE"
                };
                //params for Activate Card Api
                var activateApiParams = {
                    "cardName": _this.holderInfo.holderName,
                    "cardNumber": _this.holderInfo.cardNo,
                    "aadhaar": _this.holderInfo.aadharNo,
                    "allocation": {
                        "issuerBusinessLevelId": "51134012824156808",
                        "readerId": __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].readerId
                    },
                    "vehicleNumber": _this.holderInfo.vehicleNo,
                    "vehicleType": _this.getVehiCategory + '_' + _this.getVehiType,
                    "deviceTransactionId": _this.commonserve.getEpocTimestamp(),
                    "primaryCardUser": {
                        "mobileNumber": parseInt(_this.holderInfo.phoneNo)
                    },
                    "initialBalance": {
                        "value": 100
                    }
                };
                console.log("activateparams", activateparams);
                _this.commonserve.activateCardApi(activateApiParams).subscribe(function (respActivate) {
                    console.log("activate done Api sucess....................", respActivate);
                    _this.dal.activateCard(activateparams, function (res) {
                        console.log("activate done....................", res);
                        _this.afterActivate();
                        _this.sdrCardRemoved();
                        _this.ref.detectChanges();
                    });
                }, function (err) {
                    alert(err.error.message);
                    console.log("error in Activate ", err);
                    _this.sdrCardRemoved();
                });
            }
            else {
                // this.finePopup.message = "Enter all the field";
                // this.finePopup.showPopup('WARNINGPOPUP');
                var arr = [_this.holderInfo.holderName, _this.holderInfo.phoneNo, _this.holderInfo.vehicleNo, _this.holderInfo.cardNo, _this.holderInfo.aadharNo, _this.selectVehicle.vehicleType.label, _this.selectVehicle.vehicleCat.category];
                console.log("empty arrrr", arr);
                var arrStrMsg = ["NAME", " MOBILE NUMBER", " VEHICLE NUMBER", " CARD NUMBER", " AADHAR NUMBER", " VEHICLE TYPE", " VEHICLE CATEGORY"];
                var msg = [];
                var selectMsg = [];
                // arr.forEach((ele, index) => {
                //     if(!ele){
                //         console.log("ele nt there,,,,,......",ele)
                //         console.log("index nt there,,,,,.....",index)
                //     }
                // })
                console.log("ele nt there,,,,,......", arr);
                // console.log("index nt there,,,,,.....",index)
                for (var i = 0; i < arr.length; i++) {
                    if (!arr[i]) {
                        // msg.push(arrStrMsg[i]);
                        if (i < 5) {
                            msg.push(arrStrMsg[i]);
                        }
                        else {
                            selectMsg.push(arrStrMsg[i]);
                        }
                    }
                }
                console.log("msgggg in array22222.....", msg);
                var emptyValueMessage = msg.join(",");
                var selectValuMessage = selectMsg.join(",");
                console.log("msg created", emptyValueMessage);
                if (msg.length) {
                    _this.finePopup.message = "PLEASE ENTER " + emptyValueMessage;
                }
                if (!msg.length && selectMsg.length) {
                    _this.finePopup.message = "PLEASE SELECT " + selectValuMessage;
                }
                if (msg.length && selectMsg.length) {
                    _this.finePopup.message = "PLEASE ENTER " + emptyValueMessage + " & SELECT " + selectValuMessage;
                }
                _this.finePopup.showPopup('WARNINGPOPUP');
                _this.sdrCardRemoved();
            }
        });
    };
    //done button click action
    DashboardComponent.prototype.doneButtonClick = function (popup) {
        this.popup = popup;
        console.log("reader type in done", this.cardInfo.readerType);
        if (this.cardInfo.readerType == __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable.readerType) {
            if (window.navigator.onLine) {
                if (this.generic.updateInitited == true) {
                    this.updateAction();
                }
                if (this.generic.activateInitited == true) {
                    this.activateAction();
                }
                if (this.generic.rechargeInitited == true) {
                    this.rechargeAction();
                }
            }
            else {
                this.finePopup.message = "cannot recharge in offline mode";
                this.finePopup.showPopup('WARNINGPOPUP');
            }
        }
    };
    //single journey && RETURN JOURNET
    DashboardComponent.prototype.noPassActivity = function () {
        var vehicleTypePayparams = this.cardInfo.vehicleType;
        if (this.cardInfo.passes[0]) {
            if (this.cardInfo.passes[0].passType == "LOCAL") {
                if (this.feesetup.structure[vehicleTypePayparams].COMMERCIAL_VEHICLE_WITHIN_DISTRICT.value) {
                    this.activityCodeApi = "COMMERCIAL_VEHICLE_WITHIN_DISTRICT";
                    this.payParams.amount = parseInt(this.feesetup.structure[vehicleTypePayparams].COMMERCIAL_VEHICLE_WITHIN_DISTRICT.value);
                }
                else {
                    this.successFlag = 1;
                }
            }
        }
        else {
            if (this.cardInfo.tollLogs.length == 0) {
                if (this.feesetup.structure[vehicleTypePayparams]) {
                    this.activityCodeApi = "SINGLE_JOURNEY";
                    this.payParams.amount = parseInt(this.feesetup.structure[vehicleTypePayparams].SINGLE_JOURNEY.value);
                }
                else {
                    this.successFlag = 1;
                }
            }
            else {
                for (var i = 0; i < this.cardInfo.tollLogs.length; i++) {
                    var next24Hours = this.cardInfo.tollLogs[0].inDateTime * 1000 + __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].timeDuration.day;
                    console.log("epoccc tymmmmm", this.commonserve.getEpocTimestamp());
                    if (this.commonserve.getEpocTimestamp() < next24Hours) {
                        if (!this.feesetup.structure[vehicleTypePayparams].RETURN_JOURNEY) {
                            this.activityCodeApi = "SINGLE_JOURNEY";
                            this.payParams.amount = parseInt(this.feesetup.structure[vehicleTypePayparams].SINGLE_JOURNEY.value);
                        }
                        else {
                            this.activityCodeApi = "RETURN_JOURNEY";
                            this.payParams.amount = parseInt(this.feesetup.structure[vehicleTypePayparams].RETURN_JOURNEY.value) - parseInt(this.feesetup.structure[vehicleTypePayparams].SINGLE_JOURNEY.value);
                        }
                    }
                    else {
                        this.activityCodeApi = "SINGLE_JOURNEY";
                        this.payParams.amount = parseInt(this.feesetup.structure[vehicleTypePayparams].SINGLE_JOURNEY.value);
                    }
                }
            }
        }
    };
    DashboardComponent.prototype.PassActivityDal = function (activityParams) {
        var _this = this;
        this.dal.usePass(activityParams, function (resPass) {
            console.log("dal pass deduction activity", resPass);
            _this.genericAction();
            _this.holderInfo.currentAmount = resPass.balance;
            if (activityParams.readerIndex == 1) {
                _this.buarohCardRemoved2();
            }
            else {
                _this.buarohCardRemoved();
            }
        });
    };
    //get getenric data from sdr10
    DashboardComponent.prototype.scsproGetGenericDataFromCard = function () {
        var _this = this;
        this.dal.getGenericData(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (response) {
            console.log("getg of sdr10.....", response);
            if (response.error) {
                console.log("SDR10 ERR", response);
                if (response.error.name == "APP_ERROR") {
                    console.log("app error sdr10");
                }
                else {
                    _this.stopallReadersAndDefineDefaults();
                }
            }
            else {
                //STOP DETECTION FOR BUAROH
                _this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable, function (buarohstop) {
                    console.log("buaroh stop detection IN sdr10", buarohstop);
                    _this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable2, function (buarohstop2) {
                        console.log("buaroh2 stop detection IN sdr10", buarohstop2);
                    });
                });
                _this.cardInfo = response;
                _this.tempCardInfo = response;
                console.log("sdr10 generic data", _this.cardInfo);
                if (_this.lostCard_number) {
                    //this.generic.activateDisable = false;
                    if (_this.cardInfo.phoneno) {
                        alert('card is already activated');
                    }
                    else {
                        _this.holderInfo.cardNo = _this.cardInfo.cardno;
                    }
                }
                else {
                    _this.readerMode = "Inner_Reader";
                    _this.genericAction();
                    if (response.cardStatus != 'ACTIVATE') {
                        _this.activateCardAction();
                    }
                    _this.sdrCardRemoved();
                }
            }
        });
    };
    //get getenric data from buaroh
    DashboardComponent.prototype.buarohGetGenericDataFromCard = function () {
        var _this = this;
        this.dal.getGenericData(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable, function (res) {
            _this.payParams.timeInMilliSeconds = _this.commonserve.getEpocTimestamp();
            if (res.error) {
                console.log("buarohError", res);
                if (res.error.name == "APP_ERROR") {
                    console.log("app error buaroh");
                }
                else {
                    _this.stopallReadersAndDefineDefaults();
                }
            }
            else {
                console.log("buaroh generic data", res);
                // STOP DETECTION FOR SDR10
                _this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resultForRecharge) {
                    console.log("sdr10 stop detection IN BUAROH", resultForRecharge);
                    _this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable2, function (resultForRecharge2) {
                        console.log("BUAROH2 stop detection IN BUAROH", resultForRecharge2);
                    });
                });
                _this.cardInfo = res;
                if (res.cardStatus != 'ACTIVATE') {
                    _this.activateCardAction();
                    _this.buarohCardRemoved();
                }
                else {
                    _this.noPassActivity();
                    if (_this.payParams.amount < 0) {
                        _this.payParams.amount = 0;
                    }
                    else {
                        _this.payParams.amount = _this.payParams.amount;
                    }
                    var payApiParams = {
                        "cardNumber": _this.cardInfo.cardNumber,
                        "readerId": __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].readerId,
                        "activityTime": _this.commonserve.getIsoStringTime(),
                        "amount": {
                            "value": _this.payParams.amount
                        },
                        "mode": "TOLL",
                        "action": "TOLL_ENTRY",
                        "vehicleNumber": _this.cardInfo.vehicleNumber,
                        "categoryCode": _this.cardInfo.vehicleType,
                        "activityCode": _this.activityCodeApi,
                    };
                    if (res.passes[0] == null || _this.cardInfo.passes[0].passType == "LOCAL") {
                        //CAL SINGLE JOURNEY AND RETURN JOURNEY
                        //offline mode for demo
                        if (window.navigator.onLine) {
                            if (_this.successFlag == 0) {
                                if (_this.cardInfo.balance >= _this.payParams.amount) {
                                    _this.commonserve.transactionApi(payApiParams)
                                        .subscribe(function (respTransaction) {
                                        console.log("transaction api success", respTransaction);
                                        _this.payParams.readerIndex = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable.readerIndex;
                                        _this.dal.pay(_this.payParams, function (resPay) {
                                            if (!resPay.error) {
                                                _this.readerMode = "Outer_Reader";
                                                _this.genericAction();
                                                _this.holderInfo.currentAmount = resPay.amount;
                                                _this.ref.detectChanges();
                                                console.log("payment is deducted with no pass", resPay);
                                            }
                                            else {
                                                _this.finePopup.tap_again_message = "Tap Card Again";
                                                _this.donePopup('TAPAGAIN');
                                            }
                                            _this.buarohCardRemoved();
                                        });
                                    }, function (err) {
                                        console.log("error in transaction ", err);
                                        alert(err.error.message);
                                        _this.buarohCardRemoved();
                                    });
                                }
                                else {
                                    _this.finePopup.message = "Insufficient Amount";
                                    _this.finePopup.showPopup('WARNINGPOPUP');
                                    _this.buarohCardRemoved();
                                }
                            }
                            else {
                                _this.finePopup.tap_again_message = "No Such Vehicle Type Exist For this Toll";
                                _this.donePopup('TAPAGAIN');
                                _this.buarohCardRemoved();
                            }
                        }
                        else {
                            _this.finePopup.tap_again_message = "No Internet Connection";
                            _this.donePopup('TAPAGAIN');
                            _this.buarohCardRemoved();
                        }
                    }
                    else {
                        //use pass test
                        _this.payParams.amount = 0;
                        var maxlimit = _this.cardInfo.passes[0].maxTrips - 1;
                        var usepassobj = {
                            'readerType': __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable.readerType,
                            'readerIndex': 0,
                            'amount': 0,
                            'action': 'pay',
                            'serviceType': "TOLL",
                            'posTerminalId': '123456789',
                            'sourceBranchId': '123456789',
                            'destinationBranchId': '123456789',
                            'periodicity': [{
                                    'limitPeriodicity': "MONTHLY",
                                    'limitMaxCount': 15
                                },
                                {
                                    'limitPeriodicity': "DAILY",
                                    'limitMaxCount': 2
                                }],
                            'expiryDate': 1534748898,
                            'timeInMilliSeconds': _this.commonserve.getEpocTimestamp(),
                            'passType': _this.cardInfo.passes[0].passType,
                            'maxTrips': maxlimit,
                            'partnerTxnId': 9876543210,
                            'isRenewal': false
                        };
                        var passActivityApi = {
                            "cardNumber": _this.cardInfo.cardNumber,
                            "readerId": __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].readerId,
                            "activityTime": _this.commonserve.getIsoStringTime(),
                            "mode": "TOLL",
                            "action": _this.cardInfo.passes[0].passType + "_PASS",
                            "vehicleNumber": _this.cardInfo.vehicleNumber,
                            "categoryCode": _this.cardInfo.vehicleType,
                            "activityCode": _this.cardInfo.passes[0].passType + "_PASS",
                            "pass": {
                                "tripsPending": maxlimit,
                                "passType": _this.cardInfo.passes[0].passType + "_PASS",
                                "periodicityLimits": {
                                    "DAILY": {
                                        "tripsPending": 0
                                    },
                                    "MONTHLY": {
                                        "tripsPending": 0
                                    }
                                }
                            }
                        };
                        if (window.navigator.onLine) {
                            //transaction api for pass transaction
                            _this.commonserve.passTransactionApi(passActivityApi).subscribe(function (respPassActivity) {
                                console.log("pass transaction Api sucess....................", respPassActivity);
                                _this.genericAction();
                                _this.holderInfo.currentAmount = res.balance;
                                //pass api for transaction
                                //api pass transaction
                                console.log("activity pass params", passActivityApi);
                                //    this.payParams.readerIndex = environment.buarohinitializeVariable.readerIndex;
                                _this.PassActivityDal(usepassobj);
                                //   this.buarohCardRemoved();
                                _this.ref.detectChanges();
                            }, function (err) {
                                alert(err.error.message);
                                console.log("error in pass Activate ", err);
                                _this.buarohCardRemoved();
                            });
                            //use pass test end
                        }
                        else {
                            _this.finePopup.tap_again_message = "No Internet Connection";
                            _this.donePopup('TAPAGAIN');
                            _this.buarohCardRemoved();
                        }
                    }
                }
            }
        });
    };
    //get getenric data from buaroh2
    DashboardComponent.prototype.buarohGetGenericDataFromCard2 = function () {
        var _this = this;
        console.log("entered new buaroh function");
        this.dal.getGenericData(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable2, function (res) {
            _this.payParams.timeInMilliSeconds = _this.commonserve.getEpocTimestamp();
            if (res.error) {
                console.log("buarohError", res);
                if (res.error.name == "APP_ERROR") {
                    console.log("app error buaroh");
                }
                else {
                    _this.stopallReadersAndDefineDefaults();
                }
            }
            else {
                console.log("buaroh generic data", res);
                // STOP DETECTION FOR SDR10
                _this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resultForRecharge) {
                    console.log("sdr10 stop detection IN BUAROH", resultForRecharge);
                    _this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable, function (resultbuaroh) {
                        console.log("Buaroh stop Detection in BUAROH");
                    });
                });
                _this.cardInfo = res;
                if (res.cardStatus != 'ACTIVATE') {
                    _this.activateCardAction();
                    _this.buarohCardRemoved2();
                }
                else {
                    _this.noPassActivity();
                    if (_this.payParams.amount < 0) {
                        _this.payParams.amount = 0;
                    }
                    else {
                        _this.payParams.amount = _this.payParams.amount;
                    }
                    var payApiParams = {
                        "cardNumber": _this.cardInfo.cardNumber,
                        "readerId": __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].readerId,
                        "activityTime": _this.commonserve.getIsoStringTime(),
                        "amount": {
                            "value": _this.payParams.amount
                        },
                        "mode": "TOLL",
                        "action": "TOLL_ENTRY",
                        "vehicleNumber": _this.cardInfo.vehicleNumber,
                        "categoryCode": _this.cardInfo.vehicleType,
                        "activityCode": _this.activityCodeApi,
                    };
                    if (res.passes[0] == null || _this.cardInfo.passes[0].passType == "LOCAL") {
                        //CAL SINGLE JOURNEY AND RETURN JOURNEY
                        //offline mode for demo
                        if (window.navigator.onLine) {
                            if (_this.successFlag == 0) {
                                if (_this.cardInfo.balance >= _this.payParams.amount) {
                                    _this.commonserve.transactionApi(payApiParams)
                                        .subscribe(function (respTransaction) {
                                        console.log("transaction api success", respTransaction);
                                        _this.payParams.readerIndex = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable2.readerIndex;
                                        _this.dal.pay(_this.payParams, function (resPay) {
                                            if (!resPay.error) {
                                                _this.readerMode = "Outer_Reader";
                                                _this.genericAction();
                                                _this.holderInfo.currentAmount = resPay.amount;
                                                _this.ref.detectChanges();
                                                console.log("payment is deducted with no pass", resPay);
                                            }
                                            else {
                                                _this.finePopup.tap_again_message = "Tap Card Again";
                                                _this.donePopup('TAPAGAIN');
                                            }
                                            _this.buarohCardRemoved2();
                                        });
                                    }, function (err) {
                                        console.log("error in transaction ", err);
                                        alert(err.error.message);
                                        _this.buarohCardRemoved2();
                                    });
                                }
                                else {
                                    _this.finePopup.message = "Insufficient Amount";
                                    _this.finePopup.showPopup('WARNINGPOPUP');
                                    _this.buarohCardRemoved2();
                                }
                            }
                            else {
                                _this.finePopup.tap_again_message = "No Such Vehicle Type Exist For this Toll";
                                _this.donePopup('TAPAGAIN');
                                _this.buarohCardRemoved2();
                            }
                        }
                        else {
                            _this.finePopup.tap_again_message = "No Internet Connection";
                            _this.donePopup('TAPAGAIN');
                            _this.buarohCardRemoved2();
                        }
                    }
                    else {
                        //use pass test
                        _this.payParams.amount = 0;
                        var maxlimit = _this.cardInfo.passes[0].maxTrips - 1;
                        var usepassobj = {
                            'readerType': __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable2.readerType,
                            'readerIndex': 1,
                            'amount': 0,
                            'action': 'pay',
                            'serviceType': "TOLL",
                            'posTerminalId': '123456789',
                            'sourceBranchId': '123456789',
                            'destinationBranchId': '123456789',
                            'periodicity': [{
                                    'limitPeriodicity': "MONTHLY",
                                    'limitMaxCount': 15
                                },
                                {
                                    'limitPeriodicity': "DAILY",
                                    'limitMaxCount': 2
                                }],
                            'expiryDate': 1534748898,
                            'timeInMilliSeconds': _this.commonserve.getEpocTimestamp(),
                            'passType': _this.cardInfo.passes[0].passType,
                            'maxTrips': maxlimit,
                            'partnerTxnId': 9876543210,
                            'isRenewal': false
                        };
                        var passActivityApi = {
                            "cardNumber": _this.cardInfo.cardNumber,
                            "readerId": __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].readerId,
                            "activityTime": _this.commonserve.getIsoStringTime(),
                            "mode": "TOLL",
                            "action": _this.cardInfo.passes[0].passType + "_PASS",
                            "vehicleNumber": _this.cardInfo.vehicleNumber,
                            "categoryCode": _this.cardInfo.vehicleType,
                            "activityCode": _this.cardInfo.passes[0].passType + "_PASS",
                            "pass": {
                                "tripsPending": maxlimit,
                                "passType": _this.cardInfo.passes[0].passType + "_PASS",
                                "periodicityLimits": {
                                    "DAILY": {
                                        "tripsPending": 0
                                    },
                                    "MONTHLY": {
                                        "tripsPending": 0
                                    }
                                }
                            }
                        };
                        if (window.navigator.onLine) {
                            //transaction api for pass transaction
                            _this.commonserve.passTransactionApi(passActivityApi).subscribe(function (respPassActivity) {
                                console.log("pass transaction Api sucess....................", respPassActivity);
                                _this.genericAction();
                                _this.holderInfo.currentAmount = res.balance;
                                //pass api for transaction
                                //api pass transaction
                                console.log("activity pass params", passActivityApi);
                                //this.payParams.readerIndex = environment.buarohinitializeVariable2.readerIndex;
                                _this.PassActivityDal(usepassobj);
                                //this.buarohCardRemoved2();
                                _this.ref.detectChanges();
                            }, function (err) {
                                alert(err.error.message);
                                console.log("error in pass Activate ", err);
                                _this.buarohCardRemoved2();
                            });
                            //use pass test end
                        }
                        else {
                            _this.finePopup.tap_again_message = "No Internet Connection";
                            _this.donePopup('TAPAGAIN');
                            _this.buarohCardRemoved2();
                        }
                    }
                }
            }
        });
    };
    //call getGenericDataFromCard from card
    DashboardComponent.prototype.getGenericDataFromCard = function () {
        this.scsproGetGenericDataFromCard();
        this.buarohGetGenericDataFromCard();
        this.buarohGetGenericDataFromCard2();
    };
    //aadhar card popup call
    DashboardComponent.prototype.otpFingerscan = function () {
        this.finePopup.otpFingerscan();
    };
    DashboardComponent.prototype.otpFingerConfirmed = function () {
        this.finePopup.otpFingerConfirmed();
    };
    DashboardComponent.prototype.otpFingerNotRecognized = function () {
        this.finePopup.otpFingerNotRecognized();
    };
    DashboardComponent.prototype.otpConfirmedFinger = function () {
        this.finePopup.otpFingerConfirmed();
    };
    DashboardComponent.prototype.optInputFinger = function () {
        this.finePopup.otpFingerConfirmed();
    };
    DashboardComponent.prototype.otpOrFinger = function () {
        this.finePopup.otporFingerPopup();
    };
    //get operator info
    DashboardComponent.prototype.getOperatorInfo = function (operatorid) {
        var _this = this;
        if (window.navigator.onLine) {
            this.commonserve.operatorInfo(operatorid)
                .subscribe(function (response) {
                __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].operatorDetails = response;
                console.log("opeatr info", __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].operatorDetails);
                _this.getTransactionDetails(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].operatorDetails);
            });
        }
        else {
            this.finePopup.message = "Check Your Internet Connection";
            this.finePopup.showPopup('WARNINGPOPUP');
        }
    };
    //get fee structure fuctn
    DashboardComponent.prototype.gettoll = function (bussiness_id) {
        var _this = this;
        var queryparam = "?business_level_id=" + bussiness_id;
        if (window.navigator.onLine) {
            this.commonserve.apigetDomaindata(queryparam)
                .subscribe(function (response) {
                console.log("toll info=======================>>");
                _this.domaindataresponse = response;
                _this.feesetup.tollInfo = _this.domaindataresponse && _this.domaindataresponse.items && _this.domaindataresponse.items[0] ? _this.domaindataresponse.items[0] : {};
                __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].tollInfo = response;
                console.log(response);
                for (var i = 0; i < _this.feesetup.tollInfo.feeRevisions.length; i++) {
                    var effectiveDate = new Date(_this.feesetup.tollInfo.feeRevisions[i].effectiveTill).getTime();
                    var todayDate = new Date().getTime();
                    if (todayDate < effectiveDate) {
                        _this.feesetup.feeStructure = _this.feesetup.tollInfo.feeRevisions[i];
                        console.log("fee structure ===========>>");
                        console.log(_this.feesetup.feeStructure);
                        _this.updateFeeStructure();
                    }
                }
            });
        }
        else {
            this.finePopup.message = "Check Your Internet Connection";
            this.finePopup.showPopup('WARNINGPOPUP');
        }
    };
    DashboardComponent.prototype.updateFeeStructure = function () {
        var _this = this;
        var vehicleCategory = [];
        this.feesetup.vehicleTypes = [];
        this.feesetup.journeyTypes = [];
        this.feesetup.passType = [];
        this.feesetup.structure = {};
        if (this.feesetup.feeStructure.baseFees) {
            this.feesetup.feeStructure.baseFees.forEach(function (item, index) {
                if (_this.feesetup.structure[item.categoryCode]) {
                    _this.feesetup.structure[item.categoryCode][item.activityCode] = {
                        "id": item.id,
                        "data": item,
                        "value": item.amount.value
                    };
                }
                else {
                    _this.feesetup.structure[item.categoryCode] = {};
                    _this.feesetup.structure[item.categoryCode][item.activityCode] = {
                        "id": item.id,
                        "data": item,
                        "value": item.amount.value
                    };
                }
                var jindex = _this.feesetup.journeyTypes.findIndex(function (x) { return x.code == item.activityCode; });
                if (!(jindex > -1)) {
                    _this.feesetup.journeyTypes.push({
                        "code": item.activityCode,
                        "label": item.activityCodeInfo.label.replace("_", ""),
                        "info": item.activityCodeInfo,
                        "selected": false
                    });
                }
                _this.commonserve.sortArray(_this.feesetup.journeyTypes);
                console.log("cattt type is", _this.feesetup.journeyTypes);
                var vindex = _this.feesetup.vehicleTypes.findIndex(function (y) { return y.code == item.categoryCode; });
                if (!(vindex > -1)) {
                    var label = item.categoryCodeInfo.label.split('_');
                    var VehicleCode = [];
                    VehicleCode = item.categoryCodeInfo.code.split('_');
                    //let vehicleCode = code1.indexOf('_',code1.indexOf('_') + 1);
                    console.log("vehicle code=============================>", VehicleCode);
                    console.log("lable=============================>", label);
                    //take vehicle code
                    var tempVehicleCode = [];
                    for (var i = 0; i < VehicleCode.length; i++) {
                        tempVehicleCode.push(VehicleCode[i]);
                        console.log("came code.....", tempVehicleCode);
                    }
                    var vehicleCodeTemp;
                    vehicleCodeTemp = tempVehicleCode.join('_');
                    console.log("came now.....", tempVehicleCode);
                    console.log("chck the vehi code", vehicleCodeTemp);
                    for (var i = 0; i < _this.feesetup.vehicleTypes.length; i++) {
                        var categoryTemp = label[0];
                        vehicleCategory.push(categoryTemp);
                        console.log("array for vehicle category in loop", vehicleCategory);
                    }
                    //sort the array vehicleCategory and remove same category code
                    console.log("vehicleCategory to debug", vehicleCategory);
                    vehicleCategory.sort();
                    for (var i = 0; i < vehicleCategory.length; i++) {
                        if (vehicleCategory[i] == vehicleCategory[i + 1]) {
                            vehicleCategory.splice(i, 1);
                            i--;
                        }
                    }
                    console.log("demo cat", vehicleCategory);
                    console.log("demo cat ll", vehicleCategory.length);
                    //sort journey types and remove same code
                    _this.feesetup.vehiclecategories = vehicleCategory;
                    _this.feesetup.vehicleTypes.push({
                        "code": item.categoryCode,
                        "label": label[1],
                        "info": item.categoryCodeInfo,
                        "vehicleCode": vehicleCodeTemp,
                        "vehicleCategoryType": label[0]
                    });
                }
            });
        }
        console.log("this is it..............", this.feesetup.vehicleTypes);
        console.log("fee setup is", this.feesetup.structure);
        __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].feeStructure = this.feesetup.structure;
        __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].feeStructureLimits = this.feesetup.feeStructure;
        this.vehicleName = this.feesetup.vehicleTypes;
        //test end
        console.log("envv", __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].feeStructureLimits);
        console.log(this.feesetup.feeStructure);
        this.getVehicleTypeNumCode();
        this.getVehicleCatNumCode();
        //step to show vehicle category for first pagination page (vehicle type)
        for (var i = 0; i < 8; i++) {
            if (this.collectionVehicleTypeNumCode[i]) {
                this.vehicleNameFirst.push(this.collectionVehicleTypeNumCode[i]);
            }
            else {
                break;
            }
        }
        //fee structure ends
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_6__common_popup_popup_component__["a" /* PopupComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__common_popup_popup_component__["a" /* PopupComponent */])
    ], DashboardComponent.prototype, "finePopup", void 0);
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/dashboard.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_common_service__["a" /* CommonBaseService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_5__services_base64_service__["a" /* Base64Service */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3__services_dal_service__["a" /* DalService */]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"parent-div\">\r\n  <div class=\"dashboard-header\">\r\n    <div class=\"header-section\" style=\"border-right: 1px solid white;\">\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>SERVICE</span>\r\n        </div>\r\n        <div class=\"content-item text-background\">\r\n          <span>DOE PARKING</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>ZONE</span>\r\n        </div>\r\n        <div class=\"content-item text-background\">\r\n          <span>XXXXXXXX</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"header-section\" style=\"border-right: 1px solid white;\">\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>PLAZA NAME</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>NDTL TOLL</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>LANE NO.</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>7</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"header-section\" style=\"border-right: 1px solid white;\">\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>SHIFT NO.</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>787878</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>SUB SHIFT NO.</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>99</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"header-section\" style=\"border-right: 1px solid white;\">\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>LOGIC DATE TIME</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>12/03/2017</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>TOLL COLLECTOR ID</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>1</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"header-section\">\r\n      <div class=\"reader-div\">\r\n        <div class=\"reader-status\">\r\n          <span>READER STATUS</span>\r\n        </div>\r\n        <div class=\"reader-status-button\">\r\n          <button type=\"\" class=\"button-green\"></button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"toll-dashboard\">\r\n    <div class=\" open-gate-section1 \" style=\"display:flex;flex-direction:column;\">\r\n      <div class=\"open-gate-section\" >\r\n        <div class=\"toll-flavour-style\">\r\n          <div class=\"open-gate-section__top\" id=\"open-gate-section__top\" *ngIf = \"vehicleNameFirst.length\" >\r\n            <div class=\"vehicle_type vehicle_type--border--top\" *ngFor=\"let item of vehicleNameFirst ; let i = index\" [hidden] = \"hide_First_Screen\">\r\n              <button id=\"rate-button\" *ngIf=\"item.label != selectVehicle.vehicleType.label\" class=\"vehicle-buttons  {{item.class[i]}} hide-outline\" value=\"500\" [ngStyle] = \"{'background-image': 'url(' + vehicl[item.label].image +')'}\" (click)=\" getVehicleType(item);\" [disabled]=\"generic.activateDisable\">\r\n              <!-- <p>&#8377; </p> -->\r\n              </button>\r\n              <button id=\"rate-button\" *ngIf=\"item.label == selectVehicle.vehicleType.label\" class=\"vehicle-buttons  {{item.class[i]+'-active'}} hide-outline\" value=\"500\" [ngStyle] = \"{'background-image': 'url(' + vehicl[item.label].image +')'}\" (click)=\"getVehicleType(item.vehicleCode)\" [disabled]=\"generic.activateDisable\">\r\n                 <p *ngIf=\"payParams.amount\">&#8377; {{payParams.amount}}</p>\r\n              </button>\r\n              <p class=\"p-vehicle-type\">{{item.label}}</p>              \r\n            </div>\r\n            <div class=\"vehicle_type vehicle_type--border--top\" *ngFor=\"let item of vehicleNameSecond ; let i = index\" [hidden] = \"hide_Second_Screen\">\r\n              <button id=\"rate-button\" *ngIf=\"item.vehicleCode != cardVehicleType\" class=\"vehicle-buttons  {{item.class[i]}} hide-outline\"  value=\"500\" [ngStyle] = \"{'background-image': 'url(' + vehicl[item.code].image +')'}\" (click)=\"getVehicleType(item.vehicleCode)\" [disabled]=\"generic.activateDisable\">\r\n              <!--   <p>&#8377; </p> -->\r\n              </button> \r\n              <button id=\"rate-button\" *ngIf=\"item.vehicleCode == cardVehicleType\" class=\"vehicle-buttons  {{item.class[i]}} hide-outline\"  value=\"500\" [ngStyle] = \"{'background-image': 'url(' + vehicl[item.code].image +')'}\" (click)=\"getVehicleType(item.vehicleCode)\" [disabled]=\"generic.activateDisable\">\r\n              <!--   <p>&#8377; </p> -->\r\n              </button> \r\n              <p class=\"p-vehicle-type\">{{item.label}}</p>\r\n            </div> \r\n          </div>\r\n        </div>\r\n        <div class=\"vehicle-type-screens-div\">\r\n \r\n          <button class=\"left-arrow-button\" (click)=\"previousScreen();\">\r\n            <img src=\"../src/images/next-copy@2x.png\" alt=\"\" />\r\n          </button>\r\n          <span>{{vehicle_Cat_no}} / {{totalScreen}}</span>\r\n          <button class=\"right-arrow-button\" (click)=\"nextScreen();\" [disabled]=\"nextVehicleName\">\r\n            <img src=\"../src/images/next@2x.png\" alt=\"\" />\r\n          </button>\r\n        </div>\r\n        <div class=\"Vehicle-gate-open\">\r\n          <button type=\"button\" name=\"button\"  class=\"open_gate_button\">Open Gate</button>\r\n          <button type=\"button\" class=\"sync_button\">\r\n            <img src=\"../src/images/sync.png\" alt=\"\" />\r\n            <p>SYNC</p>\r\n          </button>\r\n        </div>\r\n        <!-- get journey type buttons -->\r\n        <div class=\"toll-payment_types\">\r\n          <ng-container  *ngFor=\"let item of feesetup.journeyTypes; let i = index\">\r\n            <button [id]=\"i\" type=\"button\" name=\"button \" class=\"payment_type\"  [ngClass]=\"{'payment_type': !item.selected, 'payment_type--active': item.selected}\"  [disabled]=\"generic.rechargeDisable\"  (click)= \"passNavigate(item.code)\">{{item.label}}</button>\r\n          </ng-container>\r\n          \r\n        </div>\r\n        <!-- get journey type buttons ends-->\r\n        <div class=\"vehicle-category\">\r\n          <div class=\"vehicle-usage\" *ngFor=\"let item of collectionVehicleCatNumCode ;let i = index\">\r\n            <button type=\"button\" name=\"button\" *ngIf=\"item.category == selectVehicle.vehicleCat.category\" class=\"vehicle-use-active\" [disabled]=\"generic.activateDisable\">{{item.category}}</button>\r\n            <button type=\"button\" name=\"button\"  *ngIf=\"item.category != selectVehicle.vehicleCat.category\" class=\"vehicle-use\" (click)=\"getVehiclecategory(item);\" [disabled]=\"generic.activateDisable\" >{{item.category}}</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"overview\">\r\n        <button type=\"button\" name=\"button\" ngDefaultControl class=\"overview__dashboard-meter\" id=\"datepicker\">\r\n          <img src=\"../src/images/dashboard-icon.png\" alt=\"\" />\r\n          <p>Dashboard</p>\r\n        </button>\r\n        <button type=\"button\" name=\"button\" ngDefaultControl class=\"overview__settlements\">\r\n          <img src=\"../src/images/system-view.png\" alt=\"\" />\r\n          <p>SYSTEM VIEW</p>\r\n        </button>\r\n        <button type=\"button\" name=\"button\" ngDefaultControl class=\"overview__settlements\" [routerLink]=\"['settlement']\">\r\n          <img src=\"../src/images/settlements-icon copy.png\" alt=\"\" />\r\n          <p>Settlements</p>\r\n        </button>\r\n        <button type=\"button\" name=\"button\" ngDefaultControl class=\"overview__settlements\" (click)=\"open(content)\">\r\n          <img src=\"../src/images/parking.png\" alt=\"\" />\r\n          <p>Wrong Vehicle</p>\r\n        </button>\r\n        <div class=\"overview-lost-card\">\r\n          <div class=\"lost-card__data\">\r\n            <!-- <p>Lost card code</p> -->\r\n            <input type=\"text\" name=\"name\" class=\"lost-card__code\" value=\"\" placeholder=\"Lost card code\" [(ngModel)]=\"lostCard_number\" [readonly]=\"generic.lostCardActive\" (click)=\"lostCardTextbox()\">\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"toll-gate-form \">\r\n      <div class=\"data-feild\">\r\n        <div class=\"rfid-number\">\r\n          <div class=\"rfid-p-style\">\r\n            <p>RFID</p>\r\n          </div>\r\n          <input type=\"text\" name=\"name\" class=\"rfid-number-value\" placeholder=\"*********\" [readonly]=\"generic.rfidActive\">\r\n        </div>\r\n        <div class=\"rfid-number\">\r\n          <div class=\"rfid-p-style\">\r\n            <p class=\"text-margin-bottom\">VEHICLE NO.</p>\r\n          </div>\r\n          <input type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"VEHICLE NO\" [(ngModel)]=\"holderInfo.vehicleNo\" [readonly]=\"generic.vehiclenoActive\" maxlength=\"10\"  (keypress)=\"restrictSpecialcharachter($event)\" uppercase>\r\n        </div>\r\n      </div>\r\n      <div class=\"data-feild\">\r\n        <div class=\"rfid-number\">\r\n          <img src=\"../src/images/no-1@2x.png\" class=\"car-number\" alt=\"\" />\r\n        </div>\r\n        <div class=\"rfid-number\">\r\n          <img src=\"../src/images/no-2@2x.png\" class=\"car-number\" alt=\"\"  />\r\n        </div>\r\n      </div>\r\n      <!-- <div>\r\n        <img src=\"../src/images/no-1@2x.png\" class=\"car-number\" alt=\"\" />\r\n      </div> -->\r\n      <div class=\"data-feild\">\r\n        <div class=\"rfid-number\">\r\n          <div class=\"rfid-p-style\">\r\n            <p class=\"text-margin-bottom\">CARD NO.</p>\r\n          </div>\r\n          <input type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"*********\" [(ngModel)]=\"holderInfo.cardNo\" [readonly]=\"generic.cardnoActive\" maxlength=\"16\" (keypress)=\"onlyNumberKey($event)\">\r\n        </div>\r\n        <div class=\"rfid-number\">\r\n          <div class=\"rfid-p-style\">\r\n            <p class=\"text-margin-bottom\">NAME</p>\r\n          </div>\r\n          <input type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"NAME\" [(ngModel)]=\"holderInfo.holderName\" [readonly]=\"generic.nameActive\" (keypress)=\"restrictNumeric($event)\">\r\n        </div>\r\n      </div>\r\n      <div class=\"data-feild\">\r\n        <div class=\"mobile-number\">\r\n          <div class=\"rfid-p-style\">\r\n            <p class=\"text-margin-bottom\">MOBILE NO.</p>\r\n          </div>\r\n          <input  type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"*** *** ****\" size=\"10\" maxlength=\"10\" [(ngModel)]=\"holderInfo.phoneNo\" [readonly]=\"generic.mobilenoActive\" (keypress)=\"onlyNumberKey($event)\" ngDefaultControl>\r\n        </div>\r\n        <div class=\"adhar-number\">\r\n          <div class=\"rfid-p-style\">\r\n            <p class=\"text-margin-bottom\">AADHAR NO.</p>\r\n          </div>  \r\n          <input *ngIf=\"!editAadharFlag\" (click)=\" changeAadharMode(true);\" type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"**** **** ****\" [ngModel]=\"holderInfo.aadharNo | displayCardNumber\" maxlength=\"12\" [readonly]=\"generic.aadharnoActive\" (keypress)=\"onlyNumberKey($event)\" ngDefaultControl>\r\n\r\n          <input *ngIf=\"editAadharFlag\"  type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"**** **** ****\" [(ngModel)]=\"holderInfo.aadharNo\" maxlength=\"12\" [readonly]=\"generic.aadharnoActive\" (keypress)=\"onlyNumberKey($event)\" ngDefaultControl>\r\n        </div>\r\n        <div class=\"verify-button\">\r\n          <button (click)=\"otpFingerscan()\">VERIFY</button>\r\n        </div>\r\n      </div>\r\n      <div class=\"recharge-card-margin\">\r\n        <div class=\"recharge-balance-row\">\r\n          <div class=\"width-adjust\">\r\n            <button id=\"activate\" class=\"hide-outline \" (click)=\"recharge()\" [ngClass]=\"{'green-bckg': backgroundBtn.rechargebackground, 'blue-bckg':!backgroundBtn.rechargebackground}\" [disabled]=\"generic.rechargeDisable\">RECHARGE</button>\r\n            <button id=\"recharge\" class=\"hide-outline\" (click)=\"activate()\" [ngClass]=\"{'green-bckg': backgroundBtn.activatebackground, 'blue-bckg':!backgroundBtn.activatebackground}\" [disabled]=\"generic.activateDisable\">ACTIVATE</button>\r\n            <button id=\"recharge\" class=\"hide-outline\" (click)=\"update()\" [ngClass]=\"{'green-bckg': backgroundBtn.updatebackground, 'blue-bckg':!backgroundBtn.updatebackground}\" [disabled]=\"generic.updateDisable\">UPDATE</button>\r\n          </div>\r\n          <div class=\"width-fix\">\r\n            <div class=\"balance-card-amount\">\r\n              <p>Balance Amount</p>\r\n              <div class=\"amount-section\">\r\n                <p class=\"total-balance-p\" style=\"width:10%;\">&#8377;</p>\r\n                <div style=\"text-align: left;\">\r\n                  <input class=\"balance-input\" id=\"amount\" [(ngModel)]=\"holderInfo.currentAmount\" [ngClass]=\"amountStyle\" placeholder=\"10000\" maxlength=\"7\" readonly>\r\n                </div>\r\n                <!-- <p class=\"last-digit-p\" [ngClass]=\"lastDigitStyle\">00</p> -->\r\n              </div>\r\n            </div>\r\n            <div class=\"clear-all-button\">\r\n              <button class=\"\" (click)=\"clearAll()\" [disabled]=\"generic.clearallDisable\">CLEAR ALL</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"toll-rate-cards\">\r\n          <div class=\"rate-card--view\">\r\n            <button type=\"button\" name=\"button\" ngDefaultControl class=\"rate-card__done-button\" (click)=\"doneButtonClick(cardactionResponse)\" [ngClass]=\"{'rate-card__done-button--active': !generic.doneDisable, 'rate-card__done-button--inactive':generic.doneDisable}\" [disabled]=\"generic.doneDisable\">DONE</button>\r\n            <div class=\"rate-card--recharge\">\r\n              <p>Recharge Amount</p>\r\n              <input type=\"text\" *ngIf=\"generic.rechargeamountDisable\"  name=\"name\" id=\"rate-card--recharge__value1\" class=\"rate-card--recharge__value\" maxlength=\"6\" value=\"\" placeholder=\"&#8377; 10000\" pattern=\"[0-9]\" [(ngModel)]=\"recharge_amount\" [disabled]=\"true\"  ngDefaultControl>\r\n              <input type=\"text\" *ngIf=\"!generic.rechargeamountDisable\" name=\"name\" id=\"rate-card--recharge__value\" class=\"rate-card--recharge__value\" maxlength=\"6\" value=\"\" placeholder=\"&#8377; 10000\" pattern=\"[0-9]\" [(ngModel)]=\"recharge_amount\" appAutofocus  [disabled]=\"false\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\"ngDefaultControl>\r\n            </div>\r\n            <div class=\"vehicles-rate-buttons\">\r\n              <div class=\"price-button-row\">\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('200',rate_button);\" value=\"200\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 200</button>\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('300',rate_button);\" value=\"300\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 300</button>\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('400',rate_button);\" value=\"400\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 400</button>\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('500',rate_button);\" value=\"500\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 500</button>\r\n              </div>\r\n              <div class=\"price-button-row\">\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('1500',rate_button);\" value=\"1500\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 1500</button>\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('2000',rate_button);\" value=\"2000\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 2000</button>\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('5000',rate_button);\" value=\"5000\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 5000</button>\r\n                <button type=\"button\" name=\"button\" id=\"rate-button1\" class=\"rate-button\" (click)=\"rateButtonClick('10000',rate_button);\" value=\"10000\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 10000</button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"rate-card rate-card--values\">\r\n            <button type=\"button\" name=\"button\" class=\"\r\n            number-button\" (click)=\"numberButtonClick('7',rate_button);\" value=\"7\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>7</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('8',rate_button);\" value=\"8\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>8</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('9',rate_button);\" value=\"9\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>9</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('4',rate_button);\" value=\"4\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>4</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('5',rate_button);\" value=\"5\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>5</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('6',rate_button);\" value=\"6\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>6</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('1',rate_button);\" value=\"1\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>1</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('2',rate_button);\" value=\"2\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>2</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('3',rate_button);\" value=\"3\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>3</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('0',rate_button);\" value=\"0\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>0</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('00',rate_button);\" value=\"00\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>00</button>\r\n            <button type=\"button\" name=\"button\" cursor [amount]=\"recharge_amount\" id=\"eraseNumber\" class=\"number-button\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" [(ngModel)]=\"eraseButton\" ngDefaultControl><img src=\"../src/images/delete.png\" /></button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- Modal -->\r\n  <ng-template #content let-c=\"close\" let-d=\"dismiss\">\r\n    <div class=\"total-content\">\r\n      <div class=\"modal-body\">\r\n        <img class=\"wrong-vehicle-design\" src=\"../src/images/car.png\" alt=\"\" />\r\n      </div>\r\n      <div class=\"wrong-vehicle-text\">\r\n        ARE YOU SURE ?\r\n      </div>\r\n      <div class=\"button-yes-no\">\r\n        <button class=\"no-confirmation\" (click)=\"c('Close click')\">NO</button>\r\n        <button class=\"yes-confirmation\">YES</button>\r\n      </div>\r\n    </div>\r\n    \r\n  </ng-template>\r\n  \r\n\r\n<!-- Modal for card action popup-->\r\n<ng-template role=\"dialog\"   let-c=\"close\" let-d=\"dismiss\" #cardactionResponse >\r\n   <div class=\"modal-content-card\">\r\n      <div class=\"close-modal \">\r\n        <button>\r\n          <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n        </button>\r\n      </div>\r\n      <div class=\"message-button\">\r\n        <span>Successful</span>\r\n        <button  (click)=\"c('Close click')\" autofocus>Done</button>\r\n      </div>\r\n   </div>\r\n</ng-template>\r\n\r\n<!-- Modal for card action popup ends-->\r\n<div>\r\n  <popup (setDefault)=\"definecardDefaults()\"></popup>\r\n</div>"

/***/ }),

/***/ "./src/app/dashboard/dashboard.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dashboard_component__ = __webpack_require__("./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_routing_module__ = __webpack_require__("./src/app/dashboard/dashboard.routing-module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directives_directive_module__ = __webpack_require__("./src/app/directives/directive.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pipes_pipes_module__ = __webpack_require__("./src/app/pipes/pipes.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__common_common_module__ = __webpack_require__("./src/app/common/common.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var DashboardModule = (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_8__common_common_module__["a" /* CommonBaseModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__dashboard_routing_module__["a" /* DashboardRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_5__directives_directive_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_7__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__["c" /* NgbModule */].forRoot()
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__dashboard_component__["a" /* DashboardComponent */]
            ],
            providers: [],
            exports: []
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.routing-module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_component__ = __webpack_require__("./src/app/dashboard/dashboard.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__dashboard_component__["a" /* DashboardComponent */] },
    { path: 'passes', loadChildren: 'app/dashboard/passes/passes.module#PassesModule' },
    { path: 'settlement', loadChildren: 'app/dashboard/settlement/settlement.module#SettlementModule' }
];
var DashboardRoutingModule = (function () {
    function DashboardRoutingModule() {
    }
    DashboardRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]]
        })
    ], DashboardRoutingModule);
    return DashboardRoutingModule;
}());



/***/ })

});
//# sourceMappingURL=dashboard.module.chunk.js.map