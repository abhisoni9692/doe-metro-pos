import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Subject } from "rxjs/Subject";
import { environment } from "../../environments/environment";
import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";

@Injectable()
export class CommonBaseService {
  initReader: number;
   storeinfodata = environment.storeInfo;
 
  constructor(private http: HttpClient) {
    console.log("services loaded")
    this.initReader = 1;
	}

  getEpocTimestamp(){
      var currentDateTime = new Date();
      var dataInEpoc = currentDateTime.getTime();
      return dataInEpoc;
  }
  getIsoStringTime(){
      var date = new Date();
      var tostring = date.toISOString();
      return tostring;
  }


  //sort array element based on alphabet
  sortArray(element) {
      element.sort(function(a, b){
          var nameA = a.info.numericCode, nameB = b.info.numericCode;
          if (nameA < nameB) //sort string ascending
              return -1 
          if (nameA > nameB)
              return 1
          return 0 //default return value (no sorting)
      })
  }
  //sort array for same vehicle cat label
  sortArrayOfObject(a,b){
      if(a.label<b.label)
          return -1;
      if(a.label>b.label)
          return 1;
      return 0;
  }



  createNewTransaction(reqObj) {
      let headerOptions = this.getHeaders();
      let url = environment.ds.activity;
      return this.http.post(url, reqObj, headerOptions)
        .map(function(response) {
          return response;
        }, function(error) {
          console.log(error);
        });
  }
  updateDoeStore(reqObj) {
      let headerOptions = this.getHeaders();
       let url = environment.ds.updateCard;
      return this.http.post(url, reqObj, headerOptions)
        .map(function(response) {
          return response;
        }, function(error) {
          console.log(error);
        });
		
  }
  //logout operator
  deleteOperationInfo() {
    localStorage.clear();
  }

  //set the accesstoken in local stroage
  setStoreData(data) {
    //let store = localStorage.getItem("store");
    if (data) {
      //let storeInfo = JSON.parse(store);
      //storeInfo = data ? data : this.storeinfodata.toll;
      localStorage.setItem("store", JSON.stringify(data));
    } else {
      let tolldata = data ? data : this.storeinfodata.toll;
      localStorage.setItem("store", JSON.stringify(this.storeinfodata));
    }  
  }
  //get the accesstoken in local stroage
  getStoreData() {
    let store = localStorage.getItem("store");
    console.log("local storage here =======>");
    console.log(store);
    if (store) {
      let storeInfo = JSON.parse(store);
      let tollstoreinfodata = storeInfo.toll
        ? storeInfo.toll
        : this.storeinfodata.toll;
      return storeInfo;
    }else {
      return this.storeinfodata.toll;
    }
  }

  getToken(){
    let store = localStorage.getItem("store");
    if (store) {
      let storeInfo = JSON.parse(store);
      let token = (storeInfo && storeInfo.accesstoken) ? storeInfo.accesstoken : "";
      return token;
    }else {
      let token = (this.storeinfodata.toll && this.storeinfodata.toll.accesstoken) ? this.storeinfodata.toll.accesstoken : "";
      return token;
    }
  }


  activateDoeStore(reqObj){
  	var url = environment.ds.activateCard;
  	return this.http.post(url, reqObj)
  	.map(function(response) {
  	  return response;
  	}, function(error) {
  	  console.log(error);
  	});
  }
  //login api call
  loginApi(reqobj){
    let url = environment.apiurls.loginApi + '/token';
    let txt = "username=" + reqobj.username + "&password=" + reqobj.password + "&grant_type=password";
    
    return this.http.post(url, txt)
    .map(function(response) {
      return response;
    }, function(error) {
      console.log(error);
    });
  }
  //logout operator api
  logoutApi(){
    let url = environment.apiurls.loginApi + '/revoke';
    let auth = this.getStoreData(); 
    let txt = "refresh_token=" + auth.refreshtoken;
    let headerOptions = this.getHeaders();
    return this.http.post(url,txt, headerOptions)
    .map(function(response) {
      return response;
    }, function(error) {
      console.log(error);
    });
  }
  //get operator info
  operatorInfo(reqObj){
    let url = environment.apiurls.userserv  + '/users/' + reqObj;
    let headerOptions = this.getHeaders();
    return this.http.get(url, headerOptions)
    .map(function(response) {
      return response;
    }, function(error) {
      console.log(error);
    });
  }  


  //get fee structure
  getFeeRevision(reqobj){
    let url = environment.apiurls.domainserv  + '/fee-revisions' + '?business_level_id=' + reqobj;
    let headerOptions = this.getHeaders();
    return this.http.get(url, headerOptions)
    .map(function(response) {
      console.log("fee revision in service",response)
      return response;
    }, function(error) {
      console.log(error);
    });
  }

  //get category codes
  getFeeCode(){
    let url = environment.apiurls.domainserv  + '/fee-codes';
     let headerOptions = this.getHeaders();
    return this.http.get(url, headerOptions)
    .map(function(response) {
      console.log("fee revision in service",response)
      return response;
    }, function(error) {
      console.log(error);
    });
  }

  //check amount before recharge activity
  getRechargeCheck(reqobj){
    let url = environment.apiurls.activityserv  + '/analyse';
    let headerOptions = this.getHeaders();
    return this.http.post(url, reqobj, headerOptions)
    .map(function(response) {
      console.log("recharge check api in service",response)
      return response;
    }, function(error) {
      console.log(error);
    });
  }

  //recharge Api
  recharge(reqobj){
    let url = environment.apiurls.userserv  + '/activities';
    let headerOptions = this.getHeaders();
    return this.http.post(url, reqobj, headerOptions)
    .map(function(response) {
      console.log("recharge api works",response)
      return response;
    }, function(error) {
      console.log(error);
    });
  }

  apigetDomaindata(reqobj){
    let url = environment.apiurls.domainserv  + '/tolls' + reqobj;
    let headerOptions = this.getHeaders();
    return this.http.get(url, headerOptions)
    .map((response) => {
      console.log("recharge api works",response)
      return response;
    }, (error) => {
      console.log(error);
    });
  }

  activateCardApi(reqobj) {
    let url = environment.apiurls.cardserv  + '/activate-card';
    let headerOptions = this.getHeaders();
    return this.http.post(url, reqobj, headerOptions)
    .map((response) => {
      console.log("activate card api works",response)
      return response;
    }, (error) => {
      console.log(error);
    });
  }
  updateCardApi(reqobj) {
    let url = environment.apiurls.cardserv  + '/update-card-info';
    let headerOptions = this.getHeaders();
    return this.http.post(url, reqobj, headerOptions)
    .map((response) => {
      console.log("update card api works",response)
      return response;
    }, (error) => {
      console.log(error);
    });
  }

//transaction deduction api
  transactionApi(reqobj) {
    let url = environment.apiurls.activityserv  + '/activities';
    let headerOptions = this.getHeaders();
    return this.http.post(url, reqobj, headerOptions)
    .map((response) => {
      console.log("deduction from card api works",response)
      return response;
    }, (error) => {
      console.log(error);
    });
  }
  //get company details
   getCompanyDetails(reqobj) {
    let url = environment.apiurls.businessserv  + '/business-levels/' + reqobj;
    let headerOptions = this.getHeaders();
    return this.http.get(url, headerOptions)
    .map((response) => {
      console.log("company details api works",response)
      return response;
    }, (error) => {
      console.log(error);
    });
  }
  //activate pass
  activatePassApi(reqobj) {
    let url = environment.apiurls.activityserv  + '/activities';
    let headerOptions = this.getHeaders();
    return this.http.post(url, reqobj, headerOptions)
    .map((response) => {
      console.log("activate pass api works",response)
      return response;
    }, (error) => {
      console.log(error);
    });
  }
  passTransactionApi(reqobj) {
    let url = environment.apiurls.activityserv  + '/activities';
    let headerOptions = this.getHeaders();
    return this.http.post(url, reqobj, headerOptions)
    .map((response) => {
      console.log("pass transaction api works",response)
      return response;
    }, (error) => {
      console.log(error);
    });
  }

  //get transaction log data
  transactionLogsApi(reqobj) {
    let url = environment.apiurls.activityserv  + '/activities' + reqobj;
    let headerOptions = this.getHeaders();
    return this.http.get(url, headerOptions)
    .map((response) => {
      console.log("transaction log api works",response)
      return response;
    }, (error) => {
      console.log(error);
    });
  }
  
  //offline mode
  private extractData(res: Response) {
    return res || {};
  }

  getOfflineAccessToken(){
    return this.http
     .get("./../src/assets/json/accessToken.json")
     .map(this.extractData);
  }
  getOfflineOperatorInfo(){
    return this.http
     .get("./../src/assets/json/operatorInfo.json")
     .map(this.extractData);
  }
  getOfflineCompanyDetails(){
    return this.http
     .get("./../src/assets/json/companyInfo.json")
     .map(this.extractData);
  }
  getOfflineFeeStructure(){
    return this.http
     .get("./../src/assets/json/feeStructure.json")
     .map(this.extractData);
  }

  getHeaders(){
    let token: string = this.getToken();
      let headers = new HttpHeaders({ 'Authorization': 'Bearer ' + token });
      let options = { headers: headers };
      return options;
  }
}



