import { Injectable } 				from '@angular/core';
import { HttpClient, HttpHeaders } 	from '@angular/common/http';

import { Observable } 				from 'rxjs/Observable';
import { of } 						from 'rxjs/observable/of';
import { environment } 				from  '../../environments/environment';

declare var require: any;
var dal_binding = require("C:/posreaderlib/build/Release/posappnode.node");

@Injectable()
export class DalService {

    constructor(private http: HttpClient) {
       
    }

    initializeSdr10(initializeVariable){
        dal_binding.initializeReader(initializeVariable, (response) => {
            console.log("sdr10 initilised in service",JSON.stringify(response));
        });
    }
    initializeBuaroh(initializeVariable){
        dal_binding.initializeReader(initializeVariable, (response) => {
            console.log("buaroh initilised in service",response);
        });
    }

    // stopDetectionSdr10(initializeVariable,callback){
    //     dal_binding.stopDetection(initializeVariable, (resultsdr) => {
    //         console.log("sdr10 stopDetection in beginning",resultsdr) 
    //         callback(resultsdr);

    //     });
    // }
    stopDetection(initializeVariable, callback){
        dal_binding.stopDetection(initializeVariable, (resultbuaroh) => {
            console.log("stopDetection for readers in beginning in services",resultbuaroh);
            callback(resultbuaroh);
        });
    }
    isCardRemoved(initializeVariable, callback){
        console.log("befor is card removed",initializeVariable);
        dal_binding.isCardRemoved(initializeVariable, (resp) => {
            console.log("card removed in service",resp)
            callback(resp)
        })
    }   
    getGenericData(getgenericParams, callback){
        dal_binding.getGenericData(getgenericParams, (resgetgenericRes)=>{
            console.log("GENERIC DATA in service",resgetgenericRes)
            callback(resgetgenericRes)
        })  
    }
    
    recharge(rechargeParams, callback){
        dal_binding.recharge(rechargeParams,(resRecharge)=>{
            console.log("recharge from reader",resRecharge)
            callback(resRecharge);
        })
    }
    updatePersonalData(updateParams, callback){
         dal_binding.updatePersonalData(updateParams, (resp)=> {
            console.log("update from reader",resp)
            callback(resp);

         })
    }
    activateCard(activateParams, callback){
        dal_binding.activateCard(activateParams, (res)=> {
            console.log("activate from reader",res)
            callback(res);
        })
    }
    usePass(usePassParams, callback){
         dal_binding.usePass(usePassParams,(resUsepass)=> {
             console.log("activate from reader",resUsepass)
             callback(resUsepass);
         })
    }
    pay(payparams, callback){
        dal_binding.pay(payparams,(resPay)=>{
            console.log("pay from reader",resPay)
            callback(resPay);
        })
    }
    activatePass(activatePassparams, callback){
        dal_binding.activatePass(activatePassparams, (resultsdr) => {
            console.log("activatePass from reader",resultsdr)
            callback(resultsdr);
        })
    } 
}