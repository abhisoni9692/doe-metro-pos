import { Component, ViewChild, OnInit, ChangeDetectorRef, Input, ElementRef, ViewChildren, Output} from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl } from "@angular/forms";
import { CommonBaseService } from "../services/common.service";
import { DalService } from "../services/dal.service";
import { environment } from "../../environments/environment";
import { Base64Service } from '../services/base64.service';
import { PopupComponent } from './../common/popup/popup.component';
import * as $ from 'jquery';
declare var $: any;
declare var require: any;

@Component({
    templateUrl: 'dashboard.html',
})
export class DashboardComponent {

    hide_First_Screen :boolean = false;
    hide_Second_Screen :boolean = true;
    vehicle_Cat_no : number = 1;
    totalScreen : number;
    closeResult: string;
    dal_path : any;
    cardInfo :any;
    holderInfo :any;
    tempCardInfo :any = {
        aadhaar: "",
        balance: "",
        cardNumber: "",
        name: "",
        mobile: "",
        vehicleNumber: "",
        vehicleType: "",
        passes : []
    };
    generic : any;
    ownername : string;
    lostCard_number : any;
    sdrInitializeVariable :any;
    buarohinitializeVariable : any;
    rechargeButton: any;
    activateButton: any;
    recharge_amount : any;
    backgroundBtn : any;
    digitBackgroundVar : any;
    payParams : any;
    rechargeParams : any;
   // usepassobj : any;
    //modal popup variable
    tempAction : any;
    popup : any;
    //modal popup variable ends
    businessLevelId : any;
   // checkRechargeparmas : any;
    //fee structure variable
    error : any;
    feesetup : any;
    companyid : any;
    branchId: any;
    apiTollService:any;
    apiGetBranchListResponseData:any;
    branchList:any;
    domaindataresponse:any;
    feecodesresponse:any;
    businessId : any;
    vehicleName : any;
    vehicleNameSecond : any;
    vehicleNameFirst : any;
    //fee structure var ends
    vehicleCount : any;
    //class for vehicle type
    vehicleClass : any;
    countVehicleName : any;
    nextVehicleName : any;
    //time vaiable
    currentDateTime : any;
    dataInEpoc: any;
    vehicl : {} = environment.vehicleType ;
    //activate card variable
    vehicleItem : any;
    getVehiType : any;
    getVehiCategory : any;
    activeVehicleClass : any;
    cardVehicleType : any;
    vehiCategoryExist : any;
    //aadhar blur
    editAadharFlag : any;
    //transaction var
    activityCodeApi : any; 
    userid : any;
    collectionVehicleTypeNumCode : any;
    collectionVehicleCatNumCode : any;
    vehicleTypeArray : any
    vehicleCatArray : any;
    selectVehicle : any;
    successFlag : any;
    readerMode : any;
    validClick : any;
    //ViewChild for Fine Popup
    @ViewChild(PopupComponent)
     finePopup : PopupComponent;

    constructor(
    private commonserve:CommonBaseService,
    private ref: ChangeDetectorRef,
    private base64 : Base64Service,
    private router : Router,
    private dal : DalService,  
    ){  
        this.vehicleName = [];
        this.vehicleNameSecond = [];
        this.vehicleNameFirst = []; 
        //call api for business id
        this.getOperatorDetails();
        //buaroh vaiable
        this.payParams = {
            "serviceType":"TOLL",
            "action":"PAY",
            "posTerminalId":"123456789",
            "amount": 0,
            "sourceBranchId" : "123456789",
            "destinationBranchId" : "12",
            "timeInMilliSeconds": '',
            "readerType": environment.buarohinitializeVariable.readerType,
            "readerIndex":0
        }  
        this.ownername = "";
        this.holderInfo = {
            "vehicleNo" : "",
            "cardNo" : "",
            "holderName" : "",
            "phoneNo" : "",
            "aadharNo" : "",
            "rfidNo" : "",
            "currentAmount" : "",
        };

        this.generic = {
            "vehiclenoActive" : true,
            "nameActive" : true,
            "mobilenoActive" : true,
            "cardnoActive" : true,
            "aadharnoActive" : true,
            "rfidActive" : true,
            "rechargeDisable" : true,
            "activateDisable" : true,
            "updateDisable" : true,
            "lostCardActive" : false,
            "clearallDisable" :true,
            "rechargeInitited" : false,
            "updateInitited" : false,
            "activateInitited" : false,
            "rechargeamountDisable" : true,
            "activeVehicleClass" : true,
          //  "nextVehicleName" : true
        };
        this.recharge_amount = "";
        this.lostCard_number = "";
        this.backgroundBtn = {
            "activatebackground" : false,
            "rechargebackground" : false,
            "updatebackground" :false,
        }
        this.nextVehicleName = true;
        //use pass variable

        //stop all readers and define defaults and then intialize all
        this.stopallReadersAndDefineDefaults();
        
        //get fee structure
        this.error = {};
        this.feesetup = {
          "tollInfo" : {},
          "structure" : {},
        };
        this.feesetup.vehicleTypes = [];
        this.feesetup.journeyTypes = [];
        this.feesetup.passType = [];
        this.feesetup.structure = {};
        this.feesetup.startediting = false;
        this.feesetup.feeCodes = [];
        this.feesetup.feeStructure = {};
        this.feesetup.vehiclecategories = [];
        this.feesetup.tollInfo = {};
        this.feesetup.vehicleTypes = {
            "label" : "",
            "class" : ""
        };
       //activate card variable initiate
       this.vehicleItem ="";
       this.vehiCategoryExist = "";
       this.totalScreen = 1;
       this.activityCodeApi = "";
       this.selectVehicle = {
           "vehicleType" : {
                "label" : "",
                "numericCode" : [],
                "class" : ""
            },
           "vehicleCat" : {
               "category" : "",
                "numericCode" : []
           },
           "numericCode" : 0
       }
       this.successFlag = 0;

    }

    stopallReadersAndDefineDefaults(){
        this.dal.stopDetection(environment.sdrInitializeVariable, (resultsdr) => {
            console.log("sdr10 stopDetection in beginning",resultsdr) 
            this.dal.stopDetection(environment.buarohinitializeVariable, (resultbuaroh) => {
                this.dal.stopDetection(environment.buarohinitializeVariable2, (resultbuaroh2) => {
                    this.definecardDefaults();
                    console.log(" buaroh2 stopDetection in beginning",resultbuaroh2);
                });
                console.log(" buaroh stopDetection in beginning",resultbuaroh);
            });
        });
    }

   
    //get vehicle category code and type
    getVehicleType(item){
       
        this.selectVehicle.vehicleType = item;
        console.log("type is",item)

        this.ref.detectChanges();
    }
    getVehiclecategory(item){
       this.selectVehicle.vehicleCat = item;
       console.log("cat is",item)
       this.ref.detectChanges();
    }
    //get vehicle category code and type ends

    getCategorycodewithDash(item){
        console.log("item is.....",item)
        var temp = item.replace("_","-");
        return temp;
    }
    
    //operator details test
    getOperatorDetails() {
        var auth = this.commonserve.getStoreData();
        console.log("token is",JSON.stringify(auth));
        var str = auth.accesstoken.split(".");
        var decodestr = str[1];
        var userinfo = this.base64.decode(decodestr);
        console.log("userinfo.....", userinfo)
        var s = userinfo.replace(/\\n/g, "\\n")
            .replace(/\\'/g, "\\'")
            .replace(/\\"/g, '\\"')
            .replace(/\\&/g, "\\&")
            .replace(/\\r/g, "\\r")
            .replace(/\\t/g, "\\t")
            .replace(/\\b/g, "\\b")
            .replace(/\\f/g, "\\f");
        s = s.replace(/[\u0000-\u0019]+/g, "");
        var obj = JSON.parse(s);
        this.userid = Object.keys(obj.businessProfile);
        environment.businessLevelId = obj;
        this.getOperatorInfo(obj.sub);
        this.gettoll(this.userid[0]); 

    };
    //get company details
    getTransactionDetails(parentid){
        if (window.navigator.onLine) {
            this.commonserve.getCompanyDetails(parentid['businessProfile']['businessLevel']['parentId']).subscribe(response=>{
                console.log("company  info",response);
                environment.companyInfo = response;
            },err=>{
                this.finePopup.message = err;
                this.finePopup.showPopup('WARNINGPOPUP');
                console.log("error in login ",err);
            });
        }else{
            this.finePopup.message = "Check Your Internet Connection";
            this.finePopup.showPopup('WARNINGPOPUP');
        }
    }
    //restrict character or number function ends
    onlyNumberKey(event) {
        this.editAadharFlag = true;
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }

    restrictNumeric(e) {
        return (e.charCode == 8 || e.charCode == 0 || e.charCode == 32) ? null : e.charCode >= 97 && e.charCode <= 122 || e.charCode >= 65 && e.charCode <= 90;
    }
    restrictSpecialcharachter(e) {
        return (e.charCode == 8 || e.charCode == 0) ? null : e.charCode >= 97 && e.charCode <= 122 || e.charCode >= 65 && e.charCode <= 90 || e.charCode >= 48 && e.charCode <= 57;
    }
    //restrict character or number function ends
    enterFromKeyboard(e, popup){
        if(e.charCode == 13)
        {
           this.doneButtonClick(popup);
           this.validClick = 1;
        }
    }
    numberButtonClick(rate, type) { 
        if(this.validClick !== 1){
            if (this.recharge_amount.toString().length < 6) {
                let temp = this.recharge_amount + rate;
                this.recharge_amount = parseInt(temp);
                this.digitBackgroundVar = true;
            }
        }
        this.validClick = 0;
    };
    rateButtonClick(rate, type) {
        this.recharge_amount = rate;
    };
    previousScreen(){
        this.hide_Second_Screen = true;
        this.hide_First_Screen = false;
        this.vehicle_Cat_no = 1;
        this.totalScreen = 1;
    }
    nextScreen(){
        this.hide_Second_Screen = false;
        this.hide_First_Screen = true;
        this.vehicle_Cat_no = 2;
        this.totalScreen = 2;
    }
    //seperate vehicle type from vehicle code
    returnVehicleCode(split) {
        if(!split)
        {
           //alert("reader stopped working")
           this.finePopup.message = "reader stopped working";
            this.finePopup.showPopup('WARNINGPOPUP');
        }else{
            let VehicleCode = [];
            let vehLabel;
            VehicleCode =  split.split('_');
            console.log("type of",this.feesetup.vehicleTypes.length)
            for(let i=0;i<this.feesetup.vehicleTypes.length;i++){
                if(this.feesetup.vehicleTypes[i].code ==  split){
                    console.log("array cat code in loop",this.feesetup.vehicleTypes[i].vehicleCode)
                    return this.feesetup.vehicleTypes[i].vehicleCode;
                }
            }
        }   
    }
    //seperate vehicle type from vehicle code
    returnVehicleCategorycode(split) {
        let VehicleCode = [];
        VehicleCode =  split.split('_');
        let tempVehicleCode = [];
        if(VehicleCode.length>2){
            for(let i = 0; i <=1 ; i++){
                tempVehicleCode.push( VehicleCode[i]);
                console.log("came code",tempVehicleCode)
            }
            var vehicleCodeTemp;
            vehicleCodeTemp = tempVehicleCode.join('-');
            console.log("came now.....",vehicleCodeTemp)
            return vehicleCodeTemp;
        }else{
            for(let i = 0; i <1 ; i++){
                tempVehicleCode.push( VehicleCode[i]);
                console.log("came code",tempVehicleCode)
            }
            return tempVehicleCode;   
        }
        
    }
    //changeaadhar mode
    changeAadharMode(flag) {
        this.editAadharFlag = flag;
        this.ref.detectChanges();
    }
    //navigate to pass type for activation
    passNavigate(passtype){
        if(this.cardInfo){
            switch(passtype){
                case "Single Journey": 
                   // this.router.navigate(['/dashboard/passes/singlepass']);.
                   break;
                case "COMMERCIAL_VEHICLE_WITHIN_DISTRICT": 
                    if(this.cardInfo.passes[0]){
                        this.finePopup.message = "Same Pass Already Exist";
                        this.finePopup.showPopup('WARNINGPOPUP');
                    }
                    else
                       this.router.navigate(['/dashboard/passes/localpass']); 
                       break;
                case "Return Journey": 
                  // this.router.navigate(['/dashboard/passes/returnpass']);   
                   break;
                case "MONTHLY_PASS":
                    let passPresent = 0;
                    console.log("environment.feeStructureLimits",environment.feeStructureLimits)
                    for(let i=0; i< environment.feeStructureLimits['baseFees'].length; i++){
                        if((environment.vehicleTypeFromCard == environment.feeStructureLimits['baseFees'][i].categoryCode ) && environment.feeStructureLimits['baseFees'][i].activityCode == "MONTHLY_PASS")
                        {  
                            passPresent = 1;
                        }
                    }  
                    if(passPresent == 1){
                            if(!this.cardInfo.passes[0]){
                                this.router.navigate(['/dashboard/passes/monthlypass']); 
                                break;
                            }else{
                                this.finePopup.message = "Pass already exist for this Toll";
                                this.finePopup.showPopup('WARNINGPOPUP');
                            }
                        } else{
                            this.finePopup.message = "Passes for this Vehicle Type not available";
                            this.finePopup.showPopup('WARNINGPOPUP');

                        }
                break;
            }
        }else{
            this.finePopup.message = "Please tap the card";
            this.finePopup.showPopup('WARNINGPOPUP');
        }
            
    }
    //clear all button action
    clearAll() {
        this.holderInfo = {
            "vehicleNo" : "",
            "cardNo" : "",
            "holderName" : "",
            "phoneNo" : "",
            "aadharNo" : "",
            "rfidNo" : "",
            "currentAmount" : "",
        };
        this.cardVehicleType = "";
        this.vehiCategoryExist = "";
        this.lostCard_number = "";
        this.ref.detectChanges();
        this.getGenericDataFromCard(); 
    }
      //Successfully done popup
    donePopup(message){
        this.finePopup.showPopup(message);
        setTimeout(()=> {
            this.finePopup.closePopup();

        },1000);
    }
    //successfully done popup ends

    //card function starts to call defualt card display
    definecardDefaults() {
        let currentUrl = this.router.url;
        if(currentUrl == '/dashboard'){
            this.payParams.amount = "";
            this.cardInfo = "";
            this.holderInfo = {
                "vehicleNo" : "",
                "cardNo" : "",
                "holderName" : "",
                "phoneNo" : "",
                "aadharNo" : "",
                "rfidNo" : "",
                "currentAmount" : "",
            };
            this.lostCard_number = "";
            this.generic = {
                "rechargeInitited" : false,
                "updateInitited" : false,
                "activateInitited" : false,
                "rechargeDisable" : true,
                "activateDisable" : true,
                "updateDisable" : true,
                "rechargeamountDisable" : true,
                "doneDisable" : true,
                "vehiclenoActive" : true,
                "nameActive" : true,
                "mobilenoActive" : true,
                "cardnoActive" : true,
                "aadharnoActive" : true,
                "rfidActive" : true,
                "lostCardActive" : false,
                "clearallDisable" :true,
            };
            this.recharge_amount = "";
            this.backgroundBtn = {
                "activatebackground" : false,
                "rechargebackground" : false,
                "updatebackground" :false,
            }
            this.cardVehicleType = "";
            this.vehiCategoryExist = "";
            this.activityCodeApi = "";
            this.selectVehicle = {
               "vehicleType" : {
                    "label" : "",
                    "numericCode" : [],
                    "class" : ""
                },
               "vehicleCat" : {
                   "category" : "",
                    "numericCode" : []
               },
               "numericCode" : 0
           }
            for(let i = 0; i<this.feesetup.journeyTypes.length;i++){  
                this.feesetup.journeyTypes[i].selected = false;              
            }
            this.successFlag = 0;
            this.ref.detectChanges();
            this.getGenericDataFromCard(); 
        }
    }
    //get pass type
    getPassType(type) {
        switch(type) {
            case "MONTHLY_PASS":
                return "MONTHLY";
            case "COMMERCIAL_VEHICLE_WITHIN_DISTRICT" :
                return "LOCAL";
            case "RETURN_JOURNEY" :
                return "RETURN_JOURNEY";
            case "SINGLE_JOURNEY" :
                return "SINGLE_JOURNEY";
        }
    }
    
    //sort array for same vehicle cat label ends
    //apply class in journey Type
    getJourneyTypeClass() {
        if(this.cardInfo && this.cardInfo.passes[0]){
            for(let i=0;i<this.feesetup.journeyTypes.length;i++){
                if(this.cardInfo.passes[0].passType == this.getPassType(this.feesetup.journeyTypes[i].code)){
                    this.feesetup.journeyTypes[i].selected = true;
                }

            }

        }
       
        else if(this.cardInfo && !this.cardInfo.passes[0]){
            for(let i=0;i<this.feesetup.journeyTypes.length;i++){
                if(this.activityCodeApi == this.getPassType(this.feesetup.journeyTypes[i].code)){
                    this.feesetup.journeyTypes[i].selected = true;
                }

            }
        }
    }
     //get vehiclwe type and vehicle code from card
    getVehicleDetailFromCard(){
        let vehicleCode = this.cardInfo.vehicleType;
        for(let i=0; i< this.feesetup.vehicleTypes.length; i++){
            let vehicleCodeFromDB
           if(this.feesetup.vehicleTypes[i].code[1] == '_'){
              vehicleCodeFromDB = this.feesetup.vehicleTypes[i].code.substr(2); 
           }else{
              vehicleCodeFromDB = this.feesetup.vehicleTypes[i].code;
           } 

            if(vehicleCode == vehicleCodeFromDB){
                let response = {
                    "label" : this.feesetup.vehicleTypes[i].label,
                    "numericCode" :  this.feesetup.vehicleTypes[i].info.numericCode,
                    "category" : this.feesetup.vehicleTypes[i].vehicleCategoryType
                }
                console.log("response1", response);
               // return response;
               let objVehicle = {
                    "label" : response.label,
                    "numericCode" : [response.numericCode],
                    "class" : ""
                }
                let objCat = {
                    "category" : response.category,
                    "numericCode" : [response.category]
                }
               this.getVehicleType(objVehicle);
               this.getVehiclecategory(objCat);
            }

        }
    }
    //get vehicle type and code from card ends

    //display generic data function
    genericAction() {
        this.editAadharFlag = false;
        this.holderInfo = {
            "vehicleNo" : this.cardInfo.vehicleNumber,
            "cardNo" : this.cardInfo.cardNumber,
            "holderName" : this.cardInfo.name,
            "phoneNo" : this.cardInfo.mobile,
            "aadharNo" : this.cardInfo.aadhaar,
            "rfidNo" : "",
            "currentAmount" : this.cardInfo.balance,
        };
        environment.vehicleTypeFromCard = this.cardInfo.vehicleType;
        this.getVehicleDetailFromCard();
        // this.cardVehicleType = this.returnVehicleCode(this.cardInfo.vehicleType);
        // this.vehiCategoryExist = this.returnVehicleCategorycode(this.cardInfo.vehicleType)
        this.backgroundBtn = {
            "activatebackground" : false,
            "rechargebackground" : false,
            "updatebackground" :false,
        }
        this.recharge_amount = "";
        this.generic = {
            "vehiclenoActive" : true,
            "nameActive" : true,
            "mobilenoActive" : true,
            "cardnoActive" : true,
            "aadharnoActive" : true,
            "rfidActive" : true,
            "rechargeDisable" : false,
            "activateDisable" : true,
            "updateDisable" : false,
            "lostCardActive" : true,
            "clearallDisable" :true,
            "rechargeInitited" : false,

            "updateInitited" : false,
            "activateInitited" : false,
            "rechargeamountDisable" : true,
            "doneDisable" : true,
            "activeVehicleClass" : true,
        };
        this.getJourneyTypeClass();
        if(this.readerMode == "Outer_Reader"){
            this.generic.rechargeDisable = true;
            this.generic.updateDisable = true;
        }
        environment.cardInfo = this.cardInfo;
        this.ref.detectChanges();
    }
    //is card removed dal function for sdr10
    sdrCardRemoved() {
        this.dal.isCardRemoved(environment.sdrInitializeVariable, (resp) => {
            console.log("sdr card removd..",resp)
            if(resp.cardRemoved == true){
                environment.cardInfo = {
                    passes : []
                }
                this.definecardDefaults();
            }
        })
    }
    //is card removed dal function for buaroh
    buarohCardRemoved() {
        this.dal.isCardRemoved(environment.buarohinitializeVariable, (resp) => {
            if(resp.cardRemoved == true){
                this.definecardDefaults();
            }
        })
    }

     //is card removed dal function for buaroh2
    buarohCardRemoved2() {
         this.dal.isCardRemoved(environment.buarohinitializeVariable2, (resp) => {
             if(resp.cardRemoved == true){
                 this.definecardDefaults();
             }
         })
        console.log("code for 2nd bauroh reader");
    }
    //test to get same vehicle type and category ina array
    getVehicleTypeNumCode(){
       this.collectionVehicleTypeNumCode = [];
       let j = 0;
       let i = 0;
        let vehicleClas = ["vehicle-buttons--bus","vehicle-buttons--car","vehicle-buttons--small_truck","vehicle-buttons--container","vehicle-buttons--bigger_truck","vehicle-buttons--Road_lorry","vehicle-buttons--lorry","vehicle-buttons--truck"];
        for(let j = vehicleClas.length -1; j < this.feesetup.vehicleTypes.length; j++){
            vehicleClas = vehicleClas.concat(vehicleClas);
        }
         do{
             if(i==0){
                  this.collectionVehicleTypeNumCode.push({
                     "label" : this.feesetup.vehicleTypes[i].label,
                     "numericCode" : [this.feesetup.vehicleTypes[i].info.numericCode],
                     "class" : vehicleClas
                 });
              }else{
                 let checklableResult = {
                        "available" : false,
                        "index" : -1
                    }
                  checklableResult = this.checkLabelinCollectionVehicleTypeArray(this.feesetup.vehicleTypes[i].label);
                  if(checklableResult.available){
                      this.collectionVehicleTypeNumCode[checklableResult.index].numericCode.push(this.feesetup.vehicleTypes[i].info.numericCode);
                  }else{
                      this.collectionVehicleTypeNumCode.push({
                     "label" : this.feesetup.vehicleTypes[i].label,
                     "numericCode" : [this.feesetup.vehicleTypes[i].info.numericCode],
                     "class" : vehicleClas
                     });

                  }
              }
            

             i++;
         }while(i < this.feesetup.vehicleTypes.length)
         console.log("label numbric code in array",this.collectionVehicleTypeNumCode);
    }  

    checkLabelinCollectionVehicleTypeArray(lbl){
        for(let i=0; i< this.collectionVehicleTypeNumCode.length; i++){
            if(this.collectionVehicleTypeNumCode[i].label == lbl)
                return {
                    "available" : true,
                    "index" : i
                };
        }
        return {
                    "available" : false,
                    "index" : -1
                };;
    }  
    //test to get same vehicle type and category ina array ends   
    //get vehicle cat from fee structrue
    getVehicleCatNumCode(){
       this.collectionVehicleCatNumCode = []; 
        let i = 0;
        do{
             if(i==0){
                  this.collectionVehicleCatNumCode.push({
                     "category" : this.feesetup.vehicleTypes[i].vehicleCategoryType,
                     "numericCode" : [this.feesetup.vehicleTypes[i].info.numericCode],
                     
                 });
              }else{
                 let checkCategoryResult = {
                        "available" : false,
                        "index" : -1
                    }
                  checkCategoryResult = this.checkCategoryinCollectionVehicleTypeArray(this.feesetup.vehicleTypes[i].vehicleCategoryType);
                  if(checkCategoryResult.available){
                      this.collectionVehicleCatNumCode[checkCategoryResult.index].numericCode.push(this.feesetup.vehicleTypes[i].info.numericCode);
                  }else{
                        this.collectionVehicleCatNumCode.push({
                         "category" : this.feesetup.vehicleTypes[i].vehicleCategoryType,
                         "numericCode" : [this.feesetup.vehicleTypes[i].info.numericCode],
                        });

                    }
                }
             i++;
         }while(i < this.feesetup.vehicleTypes.length)
         console.log("caregory numbric code in array",this.collectionVehicleCatNumCode);
    }  
    checkCategoryinCollectionVehicleTypeArray(lbl){
        for(let i=0; i< this.collectionVehicleCatNumCode.length; i++){
            if(this.collectionVehicleCatNumCode[i].category == lbl)
                return {
                    "available" : true,
                    "index" : i
                };
        }
        return {
            "available" : false,
            "index" : -1
        };;
    }  
    //get vehicle cat from fee structure ends
    //to get numeric code on vehicle type click
    getSeletcedNumericCode(vehicleType, vehicleCategory ){
        for(let i=0; i<vehicleType.length ; i++){
            for(let j=0; j< vehicleCategory.length; j++)

            if (vehicleType[i] == vehicleCategory[j]) {
                // code...
                return vehicleType[i];
            }
            return null;
        }
    }
    //to get numeric code on vehicle type click ends
    //recharge button action
    recharge() {  
        if(this.cardInfo.readerType == environment.sdrInitializeVariable.readerType){
            this.backgroundBtn = {
                "rechargebackground" : true,
            }
            this.generic ={
                "rechargeDisable" : false,
                "activateDisable" : true,
                "updateDisable" : true,
                "rechargeInitited" : true,
                "lostCardActive" : true,
                "rechargeamountDisable" : false,
                "vehiclenoActive" : true,
                "nameActive" : true,
                "mobilenoActive" : true,
                "cardnoActive" : true,
                "aadharnoActive" : true,
                "rfidActive" : true,
                "clearallDisable" :true,
            }
            
            this.ref.detectChanges(); 
        }
    }
    //update button
    update() {
      //  this.editAadharFlag = false;
        this.backgroundBtn = {
            "updatebackground" : true
        }
        this.generic = {
            "vehiclenoActive" : false,
            "nameActive" : true,
            "mobilenoActive" : true,
            "cardnoActive" : true,
            "aadharnoActive" : true,
            "rfidActive" : true,
            "rechargeDisable" : true,
            "activateDisable" : true,
            "updateDisable" : false,
            "lostCardActive": true,
            "updateInitited" : true,
            "rechargeamountDisable" : true,
            "clearallDisable" :true,
            "doneDisable" : false,
        };
        this.ref.detectChanges();
    }
    //activate button
    activate() {
        this.backgroundBtn = {
            "activatebackground" : true,
        }
        this.generic = {
            "activateDisable" : false,
            "rechargeDisable" : true,
            "updateDisable" : true,
            "rechargeamountDisable" : true,
            "lostCardActive": true,
            "activateInitited" : true,
            "doneDisable" : false,
        };
        this.ref.detectChanges();
    }
    //check before activation
    checkBeforeActiveCard(){
        if(this.holderInfo.holderName && this.holderInfo.phoneNo && this.holderInfo.vehicleNo && this.holderInfo.cardNo && this.holderInfo.aadharNo && this.selectVehicle.vehicleType.label && this.selectVehicle.vehicleCat.category)
            return true;    
    }
    //after recharge action
    afterRecharge(){
        this.tempAction = true;
        this.backgroundBtn = {
            "rechargebackground" : false,
        }
        this.generic = {
            "rechargeDisable" : false,
            "activateDisable" : true,
            "updateDisable" : false,
            "rechargeamountDisable" : true,
            "lostCardActive": false,
            "updateInitited" : false,
            "nameActive" : true,
            "mobilenoActive" : true,
            "cardnoActive" : true,
            "vehiclenoActive" : true,
            "aadharnoActive" : true,
            "rfidActive" : true,
            "clearallDisable" :true,
            "doneDisable" : true,
        };
        this.recharge_amount = '';
        this.donePopup('DONE');
       // this.cardActionResponse(this.popup);
        //this.ref.detectChanges();
    }

    //after update action
    afterUpdate(){
        this.backgroundBtn = {
            "updatebackground" :false
        }
        this.generic = {
            "rechargeDisable" : false,
            "activateDisable" : true,
            "updateDisable" : false,
            "updateInitited" : false,
            "rechargeamountDisable" : true,
            "nameActive" : true,
            "mobilenoActive" : true,
            "cardnoActive" : true,
            "vehiclenoActive" : true,
            "aadharnoActive" : true,
            "rfidActive" : true,
            "clearallDisable" :true,
            "doneDisable" : true,
        };
        //this.editAadharFlag = false;
        this.lostCard_number = "";
        //this.cardActionResponse(this.popup);
       // this.ref.detectChanges();
       this.donePopup('DONE');
    }

    //after activate action
    afterActivate(){
        this.backgroundBtn = {
            "activatebackground" : false,
        }
        this.generic = {
            "rechargeDisable" : false,
            "activateDisable" : true,
            "updateDisable" : false,
            "activateInitited" : false,
            "rechargeamountDisable" : true,
            "doneDisable" : true,
            "vehiclenoActive" : true,
            "nameActive" : true,
            "mobilenoActive" : true,
            "cardnoActive" : true,
            "aadharnoActive" : true,
            "rfidActive" : true,
            "lostCardActive" : false,
            "clearallDisable" :true,
        };
        this.lostCard_number = "";
      //  this.cardActionResponse(this.popup);
        this.donePopup('DONE');
        //this.ref.detectChanges();
    }
    //first tym activation action
    activateCardAction() {
        this.generic = {
            "rechargeDisable" : true,
            "activateDisable" : false,
            "updateDisable" : true,
        };
        this.finePopup.message = "PLEASE ACTIVATE THE CARD";
        this.finePopup.showPopup('WARNINGPOPUP');
        this.ref.detectChanges();    
    }
    //update action
    updateAction(){
        this.dal.stopDetection(environment.sdrInitializeVariable,(resultForUpdate)=> { 
            var updateParams = {
                "firstName": this.holderInfo.holderName,
                "name": this.holderInfo.holderName, 
                "middleName" : this.holderInfo.holderName,
                "lastName" : this.holderInfo.holderName,
                "gender": "MALE",
                "dateOfBirth": 1234567,
                "vehicleNumber" : this.holderInfo.vehicleNo,
                "mobile": parseInt(this.holderInfo.phoneNo),
                "aadhaar" : parseInt(this.holderInfo.aadharNo),
                "timeInMilliSeconds": this.commonserve.getEpocTimestamp(),
                "readerType" :environment.sdrInitializeVariable.readerType,
                "readerIndex": 0
            }
            var updateApiParams = {
                "cardNumber" : this.holderInfo.cardNo,
                "vehicleNumber" : this.holderInfo.vehicleNo,
                "vehicleType" : this.cardInfo.vehicleType
            }
            this.dal.updatePersonalData(updateParams, (resp)=> {
                console.log("update",resp);
                // this.doeStoreUpdateapi();
                this.afterUpdate();
                //api cal for update card info
                this.commonserve.updateCardApi(updateApiParams).subscribe(respActivate=>{
                    console.log("update done Api sucess....................",respActivate)
                },err=>{
                    alert(err.error.message)
                    console.log("error in update ",err);
                });
                this.ref.detectChanges();
                this.sdrCardRemoved();
            });
        });
    }
     //recharge action
    rechargeAction(){
        this.dal.stopDetection(environment.sdrInitializeVariable, (resultsdr) => {
            console.log("stop det in done......", resultsdr);
            if (this.recharge_amount) {
                let rechargeParams = {
                    "serviceType":"TOLL",
                    "action":"PAY",
                    "posTerminalId":"1234",
                    "amount": parseInt(this.recharge_amount),
                    "sourceBranchId":"231",
                    "destinationBranchId":"12",
                    "timeInMilliSeconds": this.commonserve.getEpocTimestamp(),
                    "readerType":environment.sdrInitializeVariable.readerType,
                    "readerIndex":0
                }
                //rechargeAPI params
               var rechargeParamsApi = {
                    "mode": "TOLL",
                    "amount": {
                        "value" : parseInt(this.recharge_amount),
                    },
                    "action": "CARD_TOP_UP",
                    "readerId": environment.readerId,
                    "cardNumber": this.holderInfo.cardNo,
                    "activityTime" : this.commonserve.getIsoStringTime(),
               } 
                this.commonserve.getRechargeCheck(rechargeParamsApi).subscribe(response=>{
                    console.log("response of rech limitssss",response);
                    
                        this.dal.recharge(rechargeParams,(resRecharge)=>{
                            console.log(":recharge dal params",rechargeParams)
                            console.log(":recharge from reader",resRecharge)
                           // this.checkRechargeparmas.amount.value = parseInt(this.recharge_amount);
                           
                            //rechrage api
                            this.commonserve.recharge(rechargeParamsApi).subscribe(response=>{
                              //  console.log("recharge params api...",this.checkRechargeparmas)
                                console.log("recharge done res",response);
                                var dt = new Date();
                                var date1 = dt.toISOString(); // current date nd time
                                this.holderInfo.currentAmount = resRecharge.amount;
                                this.afterRecharge();
                                this.ref.detectChanges();
                                this.sdrCardRemoved();
                            },err=>{
                                  console.log("error in recharge ",err);
                                  alert(err.error.message);
                                  this.sdrCardRemoved();
                            });
                            //recharge api end
                           // this.sdrCardRemoved();
                        })
                    
                },err=>{
                      console.log("u have crossed 20000 limit",err.error.message);
                      alert(err.error.message);
                      this.sdrCardRemoved();

                }); 
                //end
            }else{
               this.finePopup.message = "Please enter the amount";
               this.finePopup.showPopup('WARNINGPOPUP');
               this.sdrCardRemoved();
            }
        })
    }
    //get same numeric code in vehicle type and vehicle category
    getVehicleCode(numericCode){
        let vehicleCodeFromDB;
        for(let i=0;i<this.feesetup.vehicleTypes.length;i++){
            if(this.feesetup.vehicleTypes[i].info.numericCode == numericCode){
                if(this.feesetup.vehicleTypes[i].code[1] == '_'){
                    vehicleCodeFromDB = this.feesetup.vehicleTypes[i].code.substr(2); 
                    return vehicleCodeFromDB;
                }else{
                    return this.feesetup.vehicleTypes[i].code;
                }  
            }
        } 
    }
    //get same numeric code in vehicle type and vehicle category ends 
     //activate action
    activateAction(){
        this.selectVehicle.numericCode = this.getSeletcedNumericCode(this.selectVehicle.vehicleType.numericCode, this.selectVehicle.vehicleCat.numericCode);
        console.log("selected nummmmmmmm",this.selectVehicle.numericCode)
        this.dal.stopDetection(environment.sdrInitializeVariable, (resultForRecharge)=>{
            //params for dal activate card
            if(this.checkBeforeActiveCard() == true)
            {
                var activateparams = {
                    "serviceType" : "TOLL",
                    "action" : "PAY",
                    "posTerminalId" : "1234",
                    "amount" : 100,
                    "sourceBranchId" : "231",
                    "destinationBranchId" : "12",
                    "timeInMilliSeconds" : this.commonserve.getEpocTimestamp(),
                    "readerType" : environment.sdrInitializeVariable.readerType,
                    "readerIndex" : 0,
                    "cardNumber" : this.holderInfo.cardNo,
                    "name" : this.holderInfo.holderName,
                    "mobile" : parseInt(this.holderInfo.phoneNo),
                    "aadhaar" : parseInt(this.holderInfo.aadharNo),
                    "vehicleType": this.getVehicleCode(this.selectVehicle.numericCode),
                    "vehicleNumber" : this.holderInfo.vehicleNo,
                    "cardStatus" : "ACTIVATE"
                }
                //params for Activate Card Api
                let activateApiParams = {
                    "cardName": this.holderInfo.holderName,
                    "cardNumber": this.holderInfo.cardNo,
                    "aadhaar": this.holderInfo.aadharNo,
                    "allocation": {
                        "issuerBusinessLevelId": "51134012824156808",
                        "readerId": environment.readerId
                    },
                    "vehicleNumber": this.holderInfo.vehicleNo,
                    "vehicleType": this.getVehiCategory + '_' + this.getVehiType,
                    "deviceTransactionId": this.commonserve.getEpocTimestamp(),
                    "primaryCardUser": {
                        "mobileNumber": parseInt(this.holderInfo.phoneNo)
                    },
                    "initialBalance": {
                        "value": 100
                    }
                }
                console.log("activateparams",activateparams)
                this.commonserve.activateCardApi(activateApiParams).subscribe(respActivate=>{
                   console.log("activate done Api sucess....................",respActivate)                           
                    this.dal.activateCard(activateparams, (res)=> {
                        console.log("activate done....................",res)
                        this.afterActivate();
                        this.sdrCardRemoved();
                        this.ref.detectChanges();
                    });
                },err=>{  
                    alert(err.error.message)
                    console.log("error in Activate ",err);
                    this.sdrCardRemoved();
                });
            } else{
                // this.finePopup.message = "Enter all the field";
                // this.finePopup.showPopup('WARNINGPOPUP');
                let arr = [this.holderInfo.holderName, this.holderInfo.phoneNo , this.holderInfo.vehicleNo , this.holderInfo.cardNo , this.holderInfo.aadharNo , this.selectVehicle.vehicleType.label , this.selectVehicle.vehicleCat.category];
                console.log("empty arrrr", arr)
                let arrStrMsg = ["NAME"," MOBILE NUMBER"," VEHICLE NUMBER"," CARD NUMBER"," AADHAR NUMBER"," VEHICLE TYPE"," VEHICLE CATEGORY"];
                let msg = [];  
                let selectMsg = [];    
                // arr.forEach((ele, index) => {
                //     if(!ele){
                //         console.log("ele nt there,,,,,......",ele)
                //         console.log("index nt there,,,,,.....",index)
                        

                //     }
                // })
                 console.log("ele nt there,,,,,......",arr)
                // console.log("index nt there,,,,,.....",index)
                for(let i=0; i<arr.length; i++){
                    if(!arr[i])
                    {    
                       // msg.push(arrStrMsg[i]);
                       if(i < 5){
                          msg.push(arrStrMsg[i])
                       }else{
                          selectMsg.push(arrStrMsg[i])
                       }
                    }              
                }
                
                console.log("msgggg in array22222.....",msg)
                let emptyValueMessage = msg.join(",");
                let selectValuMessage = selectMsg.join(",");
                console.log("msg created",emptyValueMessage)
                if(msg.length){
                    this.finePopup.message = "PLEASE ENTER " + emptyValueMessage;
                }
                if(!msg.length && selectMsg.length){
                    this.finePopup.message = "PLEASE SELECT " + selectValuMessage;
                }

               if(msg.length && selectMsg.length){
                    this.finePopup.message = "PLEASE ENTER " + emptyValueMessage + " & SELECT " + selectValuMessage;
                }
                
                this.finePopup.showPopup('WARNINGPOPUP');
                this.sdrCardRemoved();
            }
        });  
    }
    //done button click action
    doneButtonClick(popup) {
        this.popup = popup;
        console.log("reader type in done",this.cardInfo.readerType)
        if(this.cardInfo.readerType == environment.sdrInitializeVariable.readerType){
             if (window.navigator.onLine) {
                if (this.generic.updateInitited == true) {
                    this.updateAction();
                }

                if (this.generic.activateInitited == true) {
                    this.activateAction();
                }
                if (this.generic.rechargeInitited == true) {
                    this.rechargeAction();
                } 
             }else{
                this.finePopup.message = "cannot recharge in offline mode";
                this.finePopup.showPopup('WARNINGPOPUP');
             }  
        }
    }
    //single journey && RETURN JOURNET
    noPassActivity() {
        let vehicleTypePayparams = this.cardInfo.vehicleType; 
         if(this.cardInfo.passes[0]){
            if(this.cardInfo.passes[0].passType == "LOCAL"){
                if(this.feesetup.structure[vehicleTypePayparams].COMMERCIAL_VEHICLE_WITHIN_DISTRICT.value)
                
                {
                    this.activityCodeApi = "COMMERCIAL_VEHICLE_WITHIN_DISTRICT";
                    this.payParams.amount = parseInt(this.feesetup.structure[vehicleTypePayparams].COMMERCIAL_VEHICLE_WITHIN_DISTRICT.value);  
                }else{
                    this.successFlag = 1;
                }
            }   
         }else{
            if(this.cardInfo.tollLogs.length == 0){
                if(this.feesetup.structure[vehicleTypePayparams])
                {
                    this.activityCodeApi = "SINGLE_JOURNEY";
                
                    this.payParams.amount = parseInt(this.feesetup.structure[vehicleTypePayparams].SINGLE_JOURNEY.value);  
                 
                }else{
                    this.successFlag = 1;
                }
                
            }else{
                for(let i=0; i < this.cardInfo.tollLogs.length; i++){
                    var next24Hours = this.cardInfo.tollLogs[0].inDateTime*1000 + environment.timeDuration.day;
                console.log("epoccc tymmmmm",this.commonserve.getEpocTimestamp())
                    if(this.commonserve.getEpocTimestamp() < next24Hours){
                        
                        if(!this.feesetup.structure[vehicleTypePayparams].RETURN_JOURNEY){
                            this.activityCodeApi = "SINGLE_JOURNEY";
                            this.payParams.amount = parseInt(this.feesetup.structure[vehicleTypePayparams].SINGLE_JOURNEY.value);  
                        }else{
                             
                            this.activityCodeApi = "RETURN_JOURNEY";
                            this.payParams.amount = parseInt(this.feesetup.structure[vehicleTypePayparams].RETURN_JOURNEY.value) - parseInt(this.feesetup.structure[vehicleTypePayparams].SINGLE_JOURNEY.value);  
                           
                        }
                    }else{
                        this.activityCodeApi = "SINGLE_JOURNEY";
                        this.payParams.amount = parseInt(this.feesetup.structure[vehicleTypePayparams].SINGLE_JOURNEY.value);  
                     
                    }
                }
            }
        }
    }
    PassActivityDal(activityParams){
        this.dal.usePass(activityParams,(resPass)=> {  
            console.log("dal pass deduction activity",resPass) 
            this.genericAction();
            this.holderInfo.currentAmount = resPass.balance; 
            if(activityParams.readerIndex == 1){
                this.buarohCardRemoved2();
            }else{
                this.buarohCardRemoved();
            }            
            
        })
    }
    //get getenric data from sdr10
    scsproGetGenericDataFromCard(){
        this.dal.getGenericData(environment.sdrInitializeVariable, (response)=>{
            console.log("getg of sdr10.....",response);
            if(response.error){
                console.log("SDR10 ERR",response)
                if(response.error.name == "APP_ERROR") {
                   console.log("app error sdr10")
                }else{ 
                    this.stopallReadersAndDefineDefaults();
                 }
            }else{
                //STOP DETECTION FOR BUAROH
                this.dal.stopDetection(environment.buarohinitializeVariable, (buarohstop) => {
                    console.log("buaroh stop detection IN sdr10",buarohstop);
                    this.dal.stopDetection(environment.buarohinitializeVariable2, (buarohstop2) => {
                        console.log("buaroh2 stop detection IN sdr10",buarohstop2);
                    });
                });
                
                this.cardInfo = response;
                this.tempCardInfo = response;
                console.log("sdr10 generic data", this.cardInfo);
                if (this.lostCard_number) { 
                    //this.generic.activateDisable = false;
                    if (this.cardInfo.phoneno) {
                        alert('card is already activated');
                    } else {
                        this.holderInfo.cardNo = this.cardInfo.cardno;
                    }
                } else {
                    this.readerMode = "Inner_Reader";
                    this.genericAction(); 
                    if(response.cardStatus != 'ACTIVATE') {
                        this.activateCardAction();            
                    } 
                    this.sdrCardRemoved(); 
                }
                
            }

       });
    }
    //get getenric data from buaroh
    buarohGetGenericDataFromCard(){
        this.dal.getGenericData( environment.buarohinitializeVariable,(res)=>{
            this.payParams.timeInMilliSeconds = this.commonserve.getEpocTimestamp();
            if(res.error){
                console.log("buarohError",res)
                if(res.error.name == "APP_ERROR") {

                   console.log("app error buaroh")
                }else{
                    this.stopallReadersAndDefineDefaults();
                }
            }else{
                console.log("buaroh generic data",res)

                // STOP DETECTION FOR SDR10
                this.dal.stopDetection(environment.sdrInitializeVariable, (resultForRecharge) => {
                    console.log("sdr10 stop detection IN BUAROH",resultForRecharge);
                    this.dal.stopDetection(environment.buarohinitializeVariable2, (resultForRecharge2) => {
                        console.log("BUAROH2 stop detection IN BUAROH",resultForRecharge2);
                    });
                
                });
                
                this.cardInfo = res;
                if(res.cardStatus != 'ACTIVATE') {
                    this.activateCardAction();
                    this.buarohCardRemoved();          
                }else{
                    this.noPassActivity();
                    if(this.payParams.amount < 0){
                        this.payParams.amount = 0;
                    }else{
                        this.payParams.amount = this.payParams.amount;
                    }
                    let payApiParams = {
                        "cardNumber": this.cardInfo.cardNumber,
                        "readerId": environment.readerId,
                        "activityTime": this.commonserve.getIsoStringTime(),
                        "amount": {
                            "value": this.payParams.amount
                        },
                        "mode": "TOLL",
                        "action": "TOLL_ENTRY",
                        "vehicleNumber": this.cardInfo.vehicleNumber,
                        "categoryCode": this.cardInfo.vehicleType,
                        "activityCode": this.activityCodeApi,
                    }
                    if(res.passes[0]==null||this.cardInfo.passes[0].passType == "LOCAL"){
                        //CAL SINGLE JOURNEY AND RETURN JOURNEY
                        //offline mode for demo
                        if (window.navigator.onLine) {
                            if(this.successFlag == 0){
                                if(this.cardInfo.balance >= this.payParams.amount) {
                                    this.commonserve.transactionApi(payApiParams)
                                    .subscribe(respTransaction =>{       
                                        console.log("transaction api success",respTransaction);

                                        this.payParams.readerIndex = environment.buarohinitializeVariable.readerIndex;
                                            this.dal.pay(this.payParams,(resPay)=>{
                                                if(!resPay.error){
                                                    this.readerMode = "Outer_Reader";
                                                    this.genericAction();
                                                    this.holderInfo.currentAmount = resPay.amount;
                                                    this.ref.detectChanges();
                                                    console.log("payment is deducted with no pass",resPay); 
                                                                   
                                                }else{
                                                    this.finePopup.tap_again_message = "Tap Card Again";
                                                    this.donePopup('TAPAGAIN');   
                                                }
                                                this.buarohCardRemoved(); 
                                            }); 
                                        
                                    },err=>{
                                        console.log("error in transaction ",err);
                                        alert(err.error.message);
                                        this.buarohCardRemoved();
                                    });
                                }else{
                                    this.finePopup.message = "Insufficient Amount";
                                    this.finePopup.showPopup('WARNINGPOPUP');
                                    this.buarohCardRemoved();
                                }
                            }else{
                                this.finePopup.tap_again_message = "No Such Vehicle Type Exist For this Toll";
                                this.donePopup('TAPAGAIN');
                                this.buarohCardRemoved(); 
                            }
                            
                        }else{
                            this.finePopup.tap_again_message = "No Internet Connection";
                            this.donePopup('TAPAGAIN');
                            this.buarohCardRemoved();   
                        }
                        
                    }else{
                    //use pass test
                        this.payParams.amount = 0;
                        var maxlimit = this.cardInfo.passes[0].maxTrips-1;
                        var usepassobj = {
                            'readerType': environment.buarohinitializeVariable.readerType,
                            'readerIndex':0,
                            'amount' : 0,
                            'action' : 'pay',
                            'serviceType' : "TOLL",
                            'posTerminalId' : '123456789',
                            'sourceBranchId' : '123456789',
                            'destinationBranchId' : '123456789',
                            'periodicity':[{
                                'limitPeriodicity': "MONTHLY",
                                'limitMaxCount':15
                            },
                            {
                                'limitPeriodicity' : "DAILY" ,
                                'limitMaxCount':2
                            }],
                            'expiryDate': 1534748898,
                            'timeInMilliSeconds' : this.commonserve.getEpocTimestamp(),//TODAY
                            'passType': this.cardInfo.passes[0].passType,
                            'maxTrips' : maxlimit,
                            'partnerTxnId': 9876543210,
                            'isRenewal':false
                        };
                        var passActivityApi = {
                            "cardNumber": this.cardInfo.cardNumber,
                            "readerId": environment.readerId,
                            "activityTime": this.commonserve.getIsoStringTime(),
                            "mode": "TOLL",
                            "action": this.cardInfo.passes[0].passType + "_PASS",
                            "vehicleNumber": this.cardInfo.vehicleNumber,
                            "categoryCode": this.cardInfo.vehicleType,
                            "activityCode": this.cardInfo.passes[0].passType + "_PASS",
                            "pass": {
                                "tripsPending": maxlimit,
                                "passType": this.cardInfo.passes[0].passType + "_PASS",
                                "periodicityLimits": {
                                    "DAILY": {
                                        "tripsPending": 0
                                    },
                                    "MONTHLY": {
                                        "tripsPending": 0
                                    }
                                }
                            }
                        }
                        if (window.navigator.onLine) {
                             //transaction api for pass transaction
                            this.commonserve.passTransactionApi(passActivityApi).subscribe(respPassActivity=>{
                                console.log("pass transaction Api sucess....................",respPassActivity)
                                this.genericAction();
                                this.holderInfo.currentAmount = res.balance;
                                //pass api for transaction
                                //api pass transaction
                                console.log("activity pass params", passActivityApi)
                             //    this.payParams.readerIndex = environment.buarohinitializeVariable.readerIndex;
                                this.PassActivityDal(usepassobj);
                             //   this.buarohCardRemoved();
                                this.ref.detectChanges();
                            },err=>{
                                alert(err.error.message)
                                console.log("error in pass Activate ",err);
                                this.buarohCardRemoved();
                            });
                    //use pass test end
                        }else{
                            this.finePopup.tap_again_message = "No Internet Connection";
                            this.donePopup('TAPAGAIN');
                            this.buarohCardRemoved();   
                        }
                    }
                }
            }
        });
    }

        //get getenric data from buaroh2
    buarohGetGenericDataFromCard2(){
        console.log("entered new buaroh function");
        this.dal.getGenericData( environment.buarohinitializeVariable2,(res)=>{
            this.payParams.timeInMilliSeconds = this.commonserve.getEpocTimestamp();
            if(res.error){
                console.log("buarohError",res)
                if(res.error.name == "APP_ERROR") {
                   console.log("app error buaroh")
                }else{
                    this.stopallReadersAndDefineDefaults();
                }
            }else{
                console.log("buaroh generic data",res)

                // STOP DETECTION FOR SDR10
                this.dal.stopDetection(environment.sdrInitializeVariable, (resultForRecharge) => {
                    console.log("sdr10 stop detection IN BUAROH",resultForRecharge);
                    this.dal.stopDetection(environment.buarohinitializeVariable, (resultbuaroh)=> {
                        console.log("Buaroh stop Detection in BUAROH");
                    });
                });

                

                
                this.cardInfo = res;
                if(res.cardStatus != 'ACTIVATE') {
                    this.activateCardAction();
                    this.buarohCardRemoved2();          
                }else{
                    this.noPassActivity();
                    if(this.payParams.amount < 0){
                        this.payParams.amount = 0;
                    }else{
                        this.payParams.amount = this.payParams.amount;
                    }
                    let payApiParams = {
                        "cardNumber": this.cardInfo.cardNumber,
                        "readerId": environment.readerId,
                        "activityTime": this.commonserve.getIsoStringTime(),
                        "amount": {
                            "value": this.payParams.amount
                        },
                        "mode": "TOLL",
                        "action": "TOLL_ENTRY",
                        "vehicleNumber": this.cardInfo.vehicleNumber,
                        "categoryCode": this.cardInfo.vehicleType,
                        "activityCode": this.activityCodeApi,
                    }
                    if(res.passes[0]==null||this.cardInfo.passes[0].passType == "LOCAL"){
                        //CAL SINGLE JOURNEY AND RETURN JOURNEY
                        //offline mode for demo
                        if (window.navigator.onLine) {
                            if(this.successFlag == 0){
                                if(this.cardInfo.balance >= this.payParams.amount) {
                                    this.commonserve.transactionApi(payApiParams)
                                    .subscribe(respTransaction =>{       
                                        console.log("transaction api success",respTransaction);
                                        this.payParams.readerIndex = environment.buarohinitializeVariable2.readerIndex;
                                            this.dal.pay(this.payParams,(resPay)=>{
                                                if(!resPay.error){
                                                    this.readerMode = "Outer_Reader";
                                                    this.genericAction();
                                                    this.holderInfo.currentAmount = resPay.amount;
                                                    this.ref.detectChanges();
                                                    console.log("payment is deducted with no pass",resPay); 
                                                                   
                                                }else{
                                                    this.finePopup.tap_again_message = "Tap Card Again";
                                                    this.donePopup('TAPAGAIN');   
                                                }
                                                this.buarohCardRemoved2(); 
                                            }); 
                                        
                                    },err=>{
                                        console.log("error in transaction ",err);
                                        alert(err.error.message);
                                        this.buarohCardRemoved2();
                                    });
                                }else{
                                    this.finePopup.message = "Insufficient Amount";
                                    this.finePopup.showPopup('WARNINGPOPUP');
                                    this.buarohCardRemoved2();
                                }
                            }else{
                                this.finePopup.tap_again_message = "No Such Vehicle Type Exist For this Toll";
                                this.donePopup('TAPAGAIN');
                                this.buarohCardRemoved2(); 
                            }
                            
                        }else{
                            this.finePopup.tap_again_message = "No Internet Connection";
                            this.donePopup('TAPAGAIN');
                            this.buarohCardRemoved2();   
                        }
                        
                    }else{
                    //use pass test
                        this.payParams.amount = 0;
                        var maxlimit = this.cardInfo.passes[0].maxTrips-1;
                        var usepassobj = {
                            'readerType': environment.buarohinitializeVariable2.readerType,
                            'readerIndex':  1,
                            'amount' : 0,
                            'action' : 'pay',
                            'serviceType' : "TOLL",
                            'posTerminalId' : '123456789',
                            'sourceBranchId' : '123456789',
                            'destinationBranchId' : '123456789',
                            'periodicity':[{
                                'limitPeriodicity': "MONTHLY",
                                'limitMaxCount':15
                            },
                            {
                                'limitPeriodicity' : "DAILY",
                                'limitMaxCount':2
                            }],
                            'expiryDate': 1534748898,
                            'timeInMilliSeconds' : this.commonserve.getEpocTimestamp(),//TODAY
                            'passType': this.cardInfo.passes[0].passType,
                            'maxTrips' : maxlimit,
                            'partnerTxnId': 9876543210,
                            'isRenewal':false
                        };
                        var passActivityApi = {
                            "cardNumber": this.cardInfo.cardNumber,
                            "readerId": environment.readerId,
                            "activityTime": this.commonserve.getIsoStringTime(),
                            "mode": "TOLL",
                            "action": this.cardInfo.passes[0].passType + "_PASS",
                            "vehicleNumber": this.cardInfo.vehicleNumber,
                            "categoryCode": this.cardInfo.vehicleType,
                            "activityCode": this.cardInfo.passes[0].passType + "_PASS",
                            "pass": {
                                "tripsPending": maxlimit,
                                "passType": this.cardInfo.passes[0].passType + "_PASS",
                                "periodicityLimits": {
                                    "DAILY": {
                                        "tripsPending": 0
                                    },
                                    "MONTHLY": {
                                        "tripsPending": 0
                                    }
                                }
                            }
                        }
                        if (window.navigator.onLine) {
                             //transaction api for pass transaction
                            this.commonserve.passTransactionApi(passActivityApi).subscribe(respPassActivity=>{
                                console.log("pass transaction Api sucess....................",respPassActivity)
                                this.genericAction();
                                this.holderInfo.currentAmount = res.balance;
                                //pass api for transaction
                                //api pass transaction
                                console.log("activity pass params", passActivityApi)
                                //this.payParams.readerIndex = environment.buarohinitializeVariable2.readerIndex;
                                this.PassActivityDal(usepassobj);
                                //this.buarohCardRemoved2();
                                this.ref.detectChanges();
                            },err=>{
                                alert(err.error.message)
                                console.log("error in pass Activate ",err);
                                this.buarohCardRemoved2();
                            });
                    //use pass test end
                        }else{
                            this.finePopup.tap_again_message = "No Internet Connection";
                            this.donePopup('TAPAGAIN');
                            this.buarohCardRemoved2();   
                        }
                    }
                }
            }
        });
    }


    //call getGenericDataFromCard from card
    getGenericDataFromCard() { 
        this.scsproGetGenericDataFromCard();
        this.buarohGetGenericDataFromCard();
        this.buarohGetGenericDataFromCard2();    
    }
    //aadhar card popup call
    otpFingerscan() {
        this.finePopup.otpFingerscan();
    }
    otpFingerConfirmed() {
        this.finePopup.otpFingerConfirmed();
    }
    otpFingerNotRecognized() {
        this.finePopup.otpFingerNotRecognized();
    }
    otpConfirmedFinger() {
        this.finePopup.otpFingerConfirmed();
    }
    optInputFinger() {
       this.finePopup.otpFingerConfirmed();
    }
    otpOrFinger() {
        this.finePopup.otporFingerPopup();
    }
    //get operator info
    getOperatorInfo(operatorid){
        if (window.navigator.onLine) {
            this.commonserve.operatorInfo(operatorid)
            .subscribe(response =>{
                environment.operatorDetails = response;
                console.log("opeatr info",environment.operatorDetails)
                 this.getTransactionDetails(environment.operatorDetails);

            })
        }else{
            this.finePopup.message = "Check Your Internet Connection";
            this.finePopup.showPopup('WARNINGPOPUP');
        }
      
    }
    //get fee structure fuctn
    gettoll(bussiness_id){
        let queryparam = "?business_level_id=" + bussiness_id;
            if (window.navigator.onLine) {
                this.commonserve.apigetDomaindata(queryparam)
                    .subscribe(response =>{
                        console.log("toll info=======================>>");
                        this.domaindataresponse = response;
                        this.feesetup.tollInfo = this.domaindataresponse && this.domaindataresponse.items && this.domaindataresponse.items[0] ? this.domaindataresponse.items[0] : {};
                        environment.tollInfo = response;
                        console.log(response);
                        for(var i =0; i < this.feesetup.tollInfo.feeRevisions.length; i++){
                            var effectiveDate = new Date(this.feesetup.tollInfo.feeRevisions[i].effectiveTill).getTime();
                            var todayDate = new Date().getTime();

                            if(todayDate < effectiveDate){
                               this.feesetup.feeStructure = this.feesetup.tollInfo.feeRevisions[i];
                               console.log("fee structure ===========>>");
                               console.log(this.feesetup.feeStructure);
                               this.updateFeeStructure();
                            }
                        }
                    })
            }else{
                this.finePopup.message = "Check Your Internet Connection";
                this.finePopup.showPopup('WARNINGPOPUP');
        }   
    }
    updateFeeStructure(){
        
        let vehicleCategory =[];
        this.feesetup.vehicleTypes = [];
        this.feesetup.journeyTypes = [];
        this.feesetup.passType = [];
        this.feesetup.structure = {};
        
        if(this.feesetup.feeStructure.baseFees){
            this.feesetup.feeStructure.baseFees.forEach((item, index) => {    
            if(this.feesetup.structure[item.categoryCode]){
                this.feesetup.structure[item.categoryCode][item.activityCode] = {
                  "id" : item.id,
                  "data" : item,
                  "value": item.amount.value
                };
            }else{
                this.feesetup.structure[item.categoryCode] = {};
                this.feesetup.structure[item.categoryCode][item.activityCode] = {
                  "id" : item.id,
                  "data" : item,
                  "value": item.amount.value
                };
            }

            let jindex = this.feesetup.journeyTypes.findIndex( x=> x.code == item.activityCode);
            if( !(jindex > -1) ){
                this.feesetup.journeyTypes.push({
                  "code" : item.activityCode,
                  "label" : item.activityCodeInfo.label.replace("_",""),
                  "info": item.activityCodeInfo,
                  "selected" : false
                });
              }
            this.commonserve.sortArray(this.feesetup.journeyTypes);
            console.log("cattt type is",this.feesetup.journeyTypes)
            let vindex = this.feesetup.vehicleTypes.findIndex( y => y.code == item.categoryCode);
            if( !(vindex > -1) ){
                let label = item.categoryCodeInfo.label.split('_');
                let VehicleCode = [];
                VehicleCode = item.categoryCodeInfo.code.split('_');
                //let vehicleCode = code1.indexOf('_',code1.indexOf('_') + 1);
                console.log("vehicle code=============================>", VehicleCode);
                console.log("lable=============================>", label);
                //take vehicle code
                let tempVehicleCode = [];
                for(let i = 0; i < VehicleCode.length; i++){
                    tempVehicleCode.push( VehicleCode[i]);
                    console.log("came code.....",tempVehicleCode)
                }
               var vehicleCodeTemp;
               vehicleCodeTemp = tempVehicleCode.join('_');
                console.log("came now.....",tempVehicleCode)
                console.log("chck the vehi code",vehicleCodeTemp)
                for ( let i=0;i<this.feesetup.vehicleTypes.length;i++){
                  let categoryTemp = label[0];
                    vehicleCategory.push(categoryTemp);
                    console.log("array for vehicle category in loop",vehicleCategory)
                }

                //sort the array vehicleCategory and remove same category code
                console.log("vehicleCategory to debug",vehicleCategory)
                vehicleCategory.sort();
                for ( let i=0;i<vehicleCategory.length;i++){
                    if(vehicleCategory[i] == vehicleCategory[i+1]){
                        vehicleCategory.splice(i,1);
                        i--;
                    }
                }
                console.log("demo cat",vehicleCategory )
                console.log("demo cat ll",vehicleCategory.length)
                //sort journey types and remove same code
                this.feesetup.vehiclecategories = vehicleCategory;

                this.feesetup.vehicleTypes.push({
                  "code" : item.categoryCode,
                  "label" : label[1],
                  "info" : item.categoryCodeInfo,
                  "vehicleCode" : vehicleCodeTemp,
                  "vehicleCategoryType" : label[0]
                });
              }
            });
          }
        console.log("this is it..............",this.feesetup.vehicleTypes)
        console.log("fee setup is",this.feesetup.structure)
        environment.feeStructure = this.feesetup.structure ;
        environment.feeStructureLimits = this.feesetup.feeStructure;
        this.vehicleName = this.feesetup.vehicleTypes;

          //test end
        console.log("envv",environment.feeStructureLimits);
        console.log(this.feesetup.feeStructure);
        this.getVehicleTypeNumCode();
        this.getVehicleCatNumCode();
        //step to show vehicle category for first pagination page (vehicle type)
        for(let i=0; i<8; i++){
            if(this.collectionVehicleTypeNumCode[i]){
                this.vehicleNameFirst.push(this.collectionVehicleTypeNumCode[i]);
            }else{
                break;
            }

        }
        //fee structure ends
    }
}