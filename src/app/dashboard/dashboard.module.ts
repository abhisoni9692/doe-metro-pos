import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard.routing-module';

import { DirectivesModule } from '../directives/directive.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PipesModule } from '../pipes/pipes.module';
import { CommonBaseModule } from '../common/common.module';

@NgModule({
    imports: [
        CommonBaseModule,
        FormsModule,
        CommonModule,
        DashboardRoutingModule,
        DirectivesModule,
        PipesModule,
        NgbModule.forRoot()

    ],
    declarations: [
        DashboardComponent
    ],
    providers: [],
    exports: []
})
export class DashboardModule {
   

}
