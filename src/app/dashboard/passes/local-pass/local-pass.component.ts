 import { HttpModule } from '@angular/http';

import { Component, ViewChild, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl } from "@angular/forms";

import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CommonBaseService } from "./../../../services/common.service";
import { environment } from "./../../../../environments/environment";
import { DalService } from "./../../../services/dal.service";

@Component({
    templateUrl: 'local-pass.component.html',
})
export class LocalPassComponent {
    passParams : any;
    closeResult: string;
    currentDateTime : any;
    dataInEpoc : any;
    //modal variable
    popup : any;
    //modal variable ends
    MaxLImit : any;
    constructor(
        private commonserve:CommonBaseService,
        private modalService: NgbModal,
        private ref: ChangeDetectorRef,
        private dal : DalService,
        private router : Router
    ){
	}

    //get time in epoc
    getEpocTimestamp(){
        var currentDateTime = new Date();
        var dataInEpoc = currentDateTime.getTime();
        return dataInEpoc;
    }
    getISOString(epocTime){
       var ISOString = new Date(epocTime).toISOString();
       return ISOString;
    }
    //is card removed dal function for sdr10
    sdrCardRemoved() {
        this.dal.isCardRemoved(environment.sdrInitializeVariable, (resp) => {
            if(resp.cardRemoved == true){
                environment.cardInfo = {
                    passes : []
                }
            }
        })
    }
	confirmPassActivation(popup) {
            var timeInSeconds = this.getEpocTimestamp()+environment.timeDuration.monthly;
            timeInSeconds = timeInSeconds/1000;
            timeInSeconds = parseInt(timeInSeconds.toString());
            var passParams = {
                    "readerType" : "SCSPRO_READER",
                    "readerIndex" : 0,
                    "serviceType" : "TOLL",
                    "sourceBranchId" : "123456789",
                    "destinationBranchId" : "123456789",
                    "periodicity" : [{
                        "limitPeriodicity" : "MONTHLY",
                        "limitMaxCount": 10
                    },
                    {
                        "limitPeriodicity" : "DAILY" ,
                        "limitMaxCount" : 10
                    }],
                    "expiryDate" : timeInSeconds,
                    "timeInMilliSeconds" : this.getEpocTimestamp(),
                    "passType": "LOCAL",
                    "maxTrips" : 0,
                    "isRenewal" : false,
                    "amount" : 0,
                }
    		this.dal.stopDetection(environment.sdrInitializeVariable, (resultForRecharge) => {
                console.log("sdr10 stop detection IN passes",resultForRecharge);
                this.dal.activatePass(passParams, (resultsdr) => {
                    console.log("pass result",resultsdr)
                    if(resultsdr.error){
                        alert(resultsdr.error.message)
                    }else{
                        alert("pass activated")
                        this.sdrCardRemoved();

                    }
                })
            });	
	}
    cardActionResponse(cardactionResponse) {
        this.modalService.open(cardactionResponse).result.then((result) => {
            this.closeResult = `Closed with: ${result}`; 
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }
    
    
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
}