import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { environment } from "../../../../environments/environment";
import { CommonBaseService } from "../../../services/common.service";
import { DalService } from "../../../services/dal.service";
import { Base64Service } from '../../../services/base64.service';


@Component({
  templateUrl: 'profile.component.html',
})
export class ProfileComponent {
	operatorDetails : any;
	constructor(private commonserve : CommonBaseService, private activateroute: ActivatedRoute, private router : Router,  private base64 : Base64Service){
		this.getOperatorDetails();
	}
	getOperatorDetails(){
		this.commonserve.operatorInfo(environment.businessLevelId.sub).subscribe(response=>{
            console.log("operator info",response);
            this.operatorDetails = response;
        },err=>{
            console.log("error in login ",err);
        });
	}
	logout() {
        this.commonserve.logoutApi().subscribe(response=>{
            console.log("logout operator",response);
            this.commonserve.deleteOperationInfo();
            this.router.navigate(['/home']); 
        },err=>{
            console.log("error in logout ",err);
        });
    };
}
