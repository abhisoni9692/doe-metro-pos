import { HttpModule } from '@angular/http';
import { Component, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl } from "@angular/forms";

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { environment } from "../../../../environments/environment";

import { CommonBaseService } from "../../../services/common.service";
import { DalService } from "../../../services/dal.service";
import { Base64Service } from '../../../services/base64.service';


@Component({
  templateUrl: 'rate-card-settlement.component.html',
})
export class RateCardSettlementComponent {
  activatedroute : any;
  error : any;
  feesetup : any;
  companyid : any;
  branchId: any;
  apiTollService:any;
  apiGetBranchListResponseData:any;
  branchList:any;
  domaindataresponse:any;
  feecodesresponse:any;
  businessId : any;
  categoryType : string;
  effectiveFrom :  any;
  effectiveTill : any;
  companyDetails : any;
  tollInfo : any;
  constructor(private commonserve : CommonBaseService, private activateroute: ActivatedRoute, private route : Router,  private base64 : Base64Service, private dal : DalService){

    this.categoryType = "NH";
    this.activatedroute = this.activateroute;
    this.branchId = this.activatedroute.parent.params._value.id;
    this.error = {};
    this.error.showmeassge = false;
    this.error.message = "Kindly atleast one from each Vehicle type and service tupe";
    this.feesetup = { };
    this.effectiveFrom = "";
    this.effectiveTill = "";
    this.companyid = this.activatedroute.parent.parent.params._value.id;
    this.tollInfo = environment.tollInfo;
    this.companyDetails = environment.companyInfo;
    this.getOperatorDetails();
    this.gettoll();
  }

  gettoll(){
    let queryparam = "?business_level_id=" + Object.keys(environment.businessLevelId['businessProfile'])[0];
    this.commonserve.apigetDomaindata(queryparam)
    .subscribe(response =>{
      console.log("toll info=======================>>");
      this.domaindataresponse = response;
      this.feesetup.tollInfo = this.domaindataresponse && this.domaindataresponse.items && this.domaindataresponse.items[0] ? this.domaindataresponse.items[0] : {};
      console.log(response);
        for(var i =0; i < this.feesetup.tollInfo.feeRevisions.length; i++){
         var effectiveDate = new Date(this.feesetup.tollInfo.feeRevisions[i].effectiveTill).getTime();
         var todayDate = new Date().getTime();

         if(todayDate < effectiveDate){
           this.feesetup.feeStructure = this.feesetup.tollInfo.feeRevisions[i];
           console.log("fee structure ===========>>");
           console.log(this.feesetup.feeStructure);
           this.updateFeeStructure();
         }
       }
    })
  }


  getOperatorDetails() {
       // var businessLevelId;
        var auth = this.commonserve.getStoreData();
        console.log("token is",JSON.stringify(auth));
        var str = auth.accesstoken.split(".");
        var decodestr = str[1];
        var userinfo = this.base64.decode(decodestr);
        console.log("userinfo.....", userinfo)
        var s = userinfo.replace(/\\n/g, "\\n")
            .replace(/\\'/g, "\\'")
            .replace(/\\"/g, '\\"')
            .replace(/\\&/g, "\\&")
            .replace(/\\r/g, "\\r")
            .replace(/\\t/g, "\\t")
            .replace(/\\b/g, "\\b")
            .replace(/\\f/g, "\\f");
        s = s.replace(/[\u0000-\u0019]+/g, "");
        var obj = JSON.parse(s);
        var useriD = obj.sub;
        console.log("user id",useriD)
        this.commonserve.operatorInfo(useriD).subscribe(response=>{
            console.log("operator info",response);
        },err=>{
            console.log("error in login ",err);
        });
    };




  updateFeeStructure(){
      this.feesetup.vehicleTypes = [];
      this.feesetup.journeyTypes = [];
      this.feesetup.structure = {};
      if(this.feesetup.feeStructure.baseFees){
        this.feesetup.feeStructure.baseFees.forEach((item, index) => {
          if(this.feesetup.structure[item.categoryCode]){
            this.feesetup.structure[item.categoryCode][item.activityCode] = {
              "id" : item.id,
              "data" : item,
              "value": item.amount.value
            };
          }else{
            this.feesetup.structure[item.categoryCode] = {};
            this.feesetup.structure[item.categoryCode][item.activityCode] = {
              "id" : item.id,
              "data" : item,
              "value": item.amount.value
            };
          }

          let jindex = this.feesetup.journeyTypes.findIndex( x=> x.code == item.activityCode);
          if( !(jindex > -1) ){
            this.feesetup.journeyTypes.push({
              "code" : item.activityCode,
              "label" : item.activityCodeInfo.label.replace("_",""),
              "info": item.activityCodeInfo
            });
          }

          let vindex = this.feesetup.vehicleTypes.findIndex( y => y.code == item.categoryCode);
          if( !(vindex > -1) ){
            let label = item.categoryCodeInfo.label.split('_');
            this.feesetup.vehicleTypes.push({
              "code" : item.categoryCode,
              "label" : label[1],
              "info" : item.categoryCodeInfo
            });
            console.log("search ing",this.feesetup.vehicleTypes)
          }
        });
      }
      console.log("feesetup is",this.feesetup);
      //convert ISO Date format to date 
      this.effectiveFrom = this.feesetup.feeStructure.effectiveFrom.split("T")[0];
      this.effectiveTill = this.feesetup.feeStructure.effectiveTill.split("T")[0];
  }
}
