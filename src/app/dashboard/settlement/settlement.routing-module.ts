import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';

import { CardSettlementComponent } from './card-settlement/card-settlement.component';
import { ProfileComponent } from './profile/profile.component';
import { RateCardSettlementComponent } from './rate-card-settlement/rate-card-settlement.component';
import { TransactionSettlementComponent } from './transaction-settlement/transaction-settlement.component';


import { PassSettlementComponent } from './pass-settlement/pass-settlement.component';
import { RechargeSettlementComponent } from './recharge-settlement/recharge-settlement.component';
import { SummarySettlementComponent } from './summary-settlement/summary-settlement.component';

const routes: Routes = [
  { path: '', component: CardSettlementComponent},
  { path: 'cardsettlement', component: CardSettlementComponent},
  { path: 'profile', component: ProfileComponent},
  { path: 'rate-card', component: RateCardSettlementComponent},
  { path: 'transaction', component: TransactionSettlementComponent},
  { path: 'pass', component: PassSettlementComponent},
  { path: 'recharge', component: RechargeSettlementComponent},
  { path: 'summary', component: SummarySettlementComponent},
]

@NgModule({
  imports: [ RouterModule.forChild(routes)],
  exports: [ RouterModule ]
})
export class SettlementRoutingModule {}
