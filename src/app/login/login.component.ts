import { HttpModule } from '@angular/http';

import { Component, ViewChild, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl } from "@angular/forms";
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonBaseService } from "../services/common.service";
import { DalService } from "../services/dal.service";
import { environment } from "../../environments/environment";
import { PopupComponent } from './../common/popup/popup.component';
import 'rxjs/add/operator/retry';


@Component({
  templateUrl: 'login.html',
})
export class LoginComponent {
	loginParams : any;

   //ViewChild for Fine Popup
    @ViewChild(PopupComponent)
     finePopup : PopupComponent;
    //ViewChild for Fine Popup --end --

	constructor(
        private commonserve:CommonBaseService,
        private modalService: NgbModal,
        private ref: ChangeDetectorRef,
        private router : Router,
        private dal : DalService

    ){
    if(this.commonserve.initReader){
        this.dal.initializeSdr10(environment.sdrInitializeVariable);
        this.dal.initializeBuaroh(environment.buarohinitializeVariable);
        this.dal.initializeBuaroh(environment.buarohinitializeVariable2);

        this.commonserve.initReader = 0; 
    }
    this.loginParams = {
      "username" : "operator1r1",
      "password" : "12345678"
    }
	}
	login(){
    let parmas = {
      "username" : this.loginParams.username,
      "password" : this.loginParams.password
    }
    if (window.navigator.onLine) {
      if(this.loginParams.username && this.loginParams.password)
      {
        this.commonserve.loginApi(parmas).retry(5).subscribe(response=>{
          console.log("response in login",response);
          this.updateStore(response);
          this.router.navigate(['/dashboard']);
        },err=>{
          console.log("error in login ",err);
          if(err.error && err.error.message){
            this.finePopup.message = err.error.message;
            this.finePopup.showPopup('WARNINGPOPUP'); 
          } 
          else{
            this.finePopup.message = "check internet connection";
            this.finePopup.showPopup('WARNINGPOPUP');
          }  
        });
      } else{
        this.finePopup.message = "Please enter username and password";
        this.finePopup.showPopup('WARNINGPOPUP');
      }
    }else{
      this.finePopup.message = "Check Your Internet Connection";
      this.finePopup.showPopup('WARNINGPOPUP');
      
    }
	}
   updateStore (auth){
    let data = {
      "accesstoken" : auth.accessToken,
      "refreshtoken" : auth.refreshToken,
      "cartid" : ""
    };
    this.commonserve.setStoreData(data);
  }

}
