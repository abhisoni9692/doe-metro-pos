import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';


import { DirectivesModule } from '../directives/directive.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PipesModule } from '../pipes/pipes.module';
import { PopupComponent } from './popup/popup.component';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        DirectivesModule,
        PipesModule,
        NgbModule.forRoot()

    ],
    declarations: [
        PopupComponent
    ],
    providers: [],
    exports: [PopupComponent]
})
export class CommonBaseModule {
   

}
