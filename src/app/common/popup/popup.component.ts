import { Component, ChangeDetectorRef, Output, EventEmitter} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { environment } from "../../../environments/environment";
import { CommonBaseService } from "../../services/common.service";
import { DalService } from "../../services/dal.service";
import { Base64Service } from '../../services/base64.service';

@Component({
  templateUrl: 'popup.component.html',
  selector : 'popup'
})
export class PopupComponent {
    @Output() setDefault: EventEmitter<any> = new EventEmitter(); 

    finePopupFlag : any;
    inSufficentBalance : any;
    tapCardPopup : any;
    successPopup : any;
    cancelFinePopup : any;
    cardInfo : any;
    cardBalance : any;
    travelTime : any;

    currentDateTime : any;
    cardEntryDateTime : any;
    donePopupFlag : any;
    warningPopupDashboard : any;
    tap_again_message
    message : any;
    tap_again_warningPopup
    //aadhar card popup
    otpFingerPopup : any;
    otpFingerconfirmedPopup : any;
    otpFingernotRecognizedPopup : any;
    otpconfirmedFingerPopup : any;
    optinputFingerPopup : any;
    otporFingerPopup : any;
    //aadhar card popupends

	constructor(private commonserve : CommonBaseService, private activateroute: ActivatedRoute, private router : Router,  private base64 : Base64Service, private dal : DalService, private ref: ChangeDetectorRef){
        this.cardInfo = {
            tollLogs : []
        }
        this.otpFingerPopup = false;
        this.otpFingerconfirmedPopup = false;
        this.otpFingernotRecognizedPopup = false;
        this.otpconfirmedFingerPopup = false;
        this.optinputFingerPopup = false;
        this.otporFingerPopup = false;
    }
   
	showPopup (type){
        switch(type) {
            case "INSUFFICIENTBAL" : 
                this.finePopupFlag = false;
                this.inSufficentBalance = true;
                this.tapCardPopup = false;
                this.successPopup = false;
                this.cancelFinePopup = false;
                this.donePopupFlag = false;
                this.warningPopupDashboard = false;
                this.tap_again_warningPopup = false;
            break;
            case "TAPCARD" : 
                this.finePopupFlag = false;
                this.inSufficentBalance = false;
                this.tapCardPopup = true;
                this.successPopup = false;
                this.cancelFinePopup = false;
                this.donePopupFlag = false;
                this.warningPopupDashboard = false;
                this.tap_again_warningPopup = false;
            break;
            case 'CANCEL' : 
                this.finePopupFlag = false;
                this.inSufficentBalance = false;
                this.tapCardPopup = false;
                this.successPopup = false;
                this.cancelFinePopup = true;
                this.donePopupFlag = false;
                this.warningPopupDashboard = false;
                this.tap_again_warningPopup = false;
            break;
            case "SUCCESS" : 
                this.finePopupFlag = false;
                this.inSufficentBalance = false;
                this.tapCardPopup = false;
                this.successPopup = true;
                this.cancelFinePopup = false;
                this.donePopupFlag = false;
                this.warningPopupDashboard = false;
                this.tap_again_warningPopup = false;
            break;
             case "DONE" : 
                this.finePopupFlag = false;
                this.inSufficentBalance = false;
                this.tapCardPopup = false;
                this.successPopup = false;
                this.cancelFinePopup = false;
                this.donePopupFlag = true;
                this.warningPopupDashboard = false;
                this.tap_again_warningPopup = false;
            break;
            case "ACTIVATECARD" : 
                this.finePopupFlag = false;
                this.inSufficentBalance = false;
                this.tapCardPopup = false;
                this.successPopup = false;
                this.cancelFinePopup = false;
                this.donePopupFlag = false;
                this.warningPopupDashboard = false;
                this.tap_again_warningPopup = false;
            break;
            case "WARNINGPOPUP" : 
                this.finePopupFlag = false;
                this.inSufficentBalance = false;
                this.tapCardPopup = false;
                this.successPopup = false;
                this.cancelFinePopup = false;
                this.donePopupFlag = false;
                this.warningPopupDashboard = true;
                this.tap_again_warningPopup = false;
            break;
            case "TAPAGAIN" : 
                this.finePopupFlag = false;
                this.inSufficentBalance = false;
                this.tapCardPopup = false;
                this.successPopup = false;
                this.cancelFinePopup = false;
                this.donePopupFlag = false;
                this.warningPopupDashboard = false;
                this.tap_again_warningPopup = true;
            break;
        }
         this.ref.detectChanges();
    }
    closePopup(){
        this.finePopupFlag = false;
        this.inSufficentBalance = false;
        this.tapCardPopup = false;
        this.successPopup = false;
        this.cancelFinePopup = false;
        this.donePopupFlag = false;
        this.warningPopupDashboard = false;
        this.tap_again_warningPopup = false;
        this.ref.detectChanges();
    }

    cancelPopupYesBtn(){
        this.setDefault.emit(null);
        this.closePopup();
    }
    cancelPopupForActivateCard(){
        this.closePopup();
    }

    otpFingerscan() {
        this.otpFingerPopup = true;
        this.otpFingerconfirmedPopup = false;
        this.otpFingernotRecognizedPopup = false;
        this.otpconfirmedFingerPopup = false;
        this.optinputFingerPopup = false;
        this.otporFingerPopup = false;

        this.otpconfirmedFingerPopup = false;
        this.ref.detectChanges(); 
    }
    otpFingerConfirmed() {
        this.otpFingerconfirmedPopup = false;
        this.otpFingerPopup = false;
        this.otpFingernotRecognizedPopup = false;
        this.otpconfirmedFingerPopup = false;
        this.optinputFingerPopup = false;
        this.otporFingerPopup = true;
        this.ref.detectChanges(); 
    }
    otpFingerNotRecognized() {
        this.otpFingernotRecognizedPopup = false;
        this.otpFingerPopup = false;
        this.otpFingerconfirmedPopup = false;
        this.otpconfirmedFingerPopup = false;
        this.optinputFingerPopup = false;
        this.otporFingerPopup = false;
        this.ref.detectChanges(); 
    }
    otpConfirmedFinger() {
        this.otpconfirmedFingerPopup = true;
        this.otpFingerPopup = false;
        this.otpFingerconfirmedPopup = false;
        this.otpFingernotRecognizedPopup = false;
        this.optinputFingerPopup = false;
        this.otporFingerPopup = false;
        this.ref.detectChanges(); 
    }
    optInputFinger() {
       this.optinputFingerPopup = true;
       this.otpconfirmedFingerPopup = false;
        this.otpFingerPopup = false;
        this.otpFingerconfirmedPopup = false;
        this.otpFingernotRecognizedPopup = false;
        this.optinputFingerPopup = false;
        this.otporFingerPopup = false;
       this.ref.detectChanges(); 
    }
    otpOrFinger() {
        this.otporFingerPopup = true;
        this.otpconfirmedFingerPopup = false;
        this.otpFingerPopup = false;
        this.otpFingerconfirmedPopup = false;
        this.otpFingernotRecognizedPopup = false;
        this.optinputFingerPopup = false;
        this.otporFingerPopup = false;
        this.ref.detectChanges(); 
    }
}
