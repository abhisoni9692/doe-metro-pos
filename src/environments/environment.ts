// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
let baseUrl = "http://139.59.35.232";

export const environment = {
    production: true,
    apiurls : {   
    "isLiveApi" : true,
      //login Api
      "loginApi" : baseUrl + "/v1/auth",
      //get operator
      "userserv" : baseUrl + "/v1/account",
      //get fee revision
      "domainserv" : baseUrl + "/v1/domain",
      //activity apis
      "activityserv" : baseUrl + "/v1/account",
      //card related activities
      "cardserv" : baseUrl + "/v1/device",
      "businessserv": baseUrl + "/v1/business",
      "entityserv" : "http://139.59.5.154/v1/entity",
      "authserv" : "http://139.59.5.154/v1/auth",
      "contentserv" : "http://139.59.5.154/v1/content",
      "swmgmtserv" : "http://139.59.5.154/",
      "deviceserv" : "http://139.59.5.154/v1/asset",
      "checkoutserv" : "http://139.59.5.154/v1/checkout",
      "supportserv" : "http://139.59.5.154/v1/support",
      "trackingServ" : "http://139.59.5.154/v1/tracking",
      "mmscheckoutserv" : "http://139.59.5.154/v1/checkout",
      "mmsdeviceserv" : "http://139.59.5.154/v1/asset",
      "mmsauthserv" : "http://139.59.5.154/v1/business/auth",
      "mmssupportserv" : "http://139.59.5.154/v1/support",
      "mmsuserserv" : "http://139.59.5.154/v1/account",
      "mmsbusinessserv": "http://139.59.5.154/v1/business/business",
      "mmsdomainserv" : "http://139.59.5.154/v1/domain"
      },
      "ds": {
        "activity": "http://localhost:9200/activities",
        "updateCard": "http://localhost:9200/cards/updateCard",
        "activateCard": "http://localhost:9200/cards/activateCard",
        "recentTransaction": "http://localhost:9200/activities/recent",
        "feeActivities": "http://localhost:9200/toll",

      },
      "readers": {
        'sdr10': {
          "path": "C:/felica_readers/SDR-10/build/Release/sdr-10_felica",

        },
        'duali': {
          "path": "D:/Duali/SampleApp/build/Release/duali_felica"
        },
        'mquest': {
          "path": "D:/UsbChatPortwithC/UsbChatPortwithC/build/Release/mquest_felica",
        },
        'sony': {
          "path": "D:/Sony/SampleApp/build/Release/sony_felica"
        },
        'buaroh' : {
        //  "path" : "C:/BuarohSerialPort/BuarohSerialPort/build/Release/br_felica",
         "path" : "C:/felica_readers/Buaroh_8210_lib/BuarohSerialPort/build/Release/br_felica",
        },
        'dal_binding' : {
         "path": "C:/posreaderlib/build/Release/posappnode"
        }
      },
      //local storage object 
      "storeInfo":  {
        "location":{},
        "toll":{
          "accesstoken" : "",
          "refreshtoken" : "",
          "cartid" : "",
          "usertype" : "TOLL-COMPANY",
          "companyaccesstoken" : "",
          "branchaccesstoken" : ""
        }      
      },
      "vehicleType" : {
        "bus/truck" : {
          "image" : "../src/images/truckblack@2x.png",
          "label" : "",
        },
        "4 to 6 Axle Vehicle" : {
          "image" : "../src/images/axelblack-1@2x.png",
          "label" : "",
        },
        "Car/Jeep/Van" : {
          "image" : "../src/images/carblack@2x.png",
          "label" : "",
        },
        "Upto 3 Axle Vehicle":{
          "image" : "../src/images/axelblack-1@2x.png",
          "label" : "",
        },
        "LCV" : {
          "image" : "../src/images/vanblack@2x.png",
          "label" : "",
        },  
        "7 or more Axle Vehicle" : {
          "image" : "../src/images/axelblack-3@2x.png",
          "label" : "",
        },
        "HCM/EME" : {
          "image" : "../src/images/axelblack-2@2x.png",
          "label" : "",
        },
        "others" : {
          "image" : "../src/images/axelbiggerblack-3@2x.png",
          "label" : ""
        },
      },
    "vehicleTypeFromCard" : "",
    "feeStructure" : {},
    "sdrInitializeVariable": {
      "readerType":"SCSPRO_READER",
      "readerIndex":0,
      "serviceType": "TOLL",
      "branchId": "123456789"
    },
    "buarohinitializeVariable" : {
      "readerType":"BUAROH_READER",
      "readerIndex":0,
      "serviceType": "TOLL",
      "branchId": "123456789"
    },
      "buarohinitializeVariable2" : {
        "readerType":"BUAROH_READER",
        "readerIndex":1,
        "serviceType": "TOLL",
        "branchId": "123456789"
      },
    "feeStructureLimits" : [],
    "cardInfo" : {
      "passes" : [],
    },
    "businessLevelId" : {
      "sub" : "",
    },
    "operatorDetails" : {},
    "tollInfo" : {},
    "companyInfo" : {},
    "readerId" : "1157610283554512716",
    "timeDuration":{
      "day" : 86400000,
      "monthly" : 2592000000,
      "weekly" : 604800000
    },
  };
  
  