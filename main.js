const {app, BrowserWindow} = require('electron')
const path = require('path')
const url = require('url')

// const dal = require("../../posreaderlib/build/Release/posappnode");

//var dallib = require('path').join(__dirname, '/..','/..','/posreaderlib','/build','/Release','posappnode');
    
       
let win

function createWindow () {
  win = new BrowserWindow({
      // name: "Doe-toll",
      webPreferences:{
         webviewTag:true,
         webSecurity: false,
         nodeIntegration:true
       },
      height: 850,
      width: 1300,
      'max-width':1300,
      'max-height':850,
      fullscreen:true,
      kiosk:false
    })

  // load the dist folder from Angular
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'dist/index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Open the DevTools optionally:
  // win.webContents.openDevTools()

  win.on('closed', () => {
    win = null
  })
}

app.on('ready', createWindow)


app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})
//module.exports = dal;